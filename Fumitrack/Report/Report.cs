﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace Fumitrack.Report
{
    public partial class Report : MetroForm
    {
        public Report()
        {
            InitializeComponent();
        }

        private void toolStripMenuIPrint_Click(object sender, EventArgs e)
        {
            try
            {
                panelAll.AutoScroll = false;
                var size = panelAll.PreferredSize;
                var height = size.Height;
                bitmap = new Bitmap(panelAll.Width, height);

                //panelAll.DrawToBitmap(bitmap, new Rectangle(0, 0, panelAll.Width, height));
                foreach (Control control in panelAll.Controls)
                {
                    foreach (Control childControl in control.Controls)
                    {
                        control.DrawToBitmap(bitmap, control.Bounds);
                    }
                }

                printPreviewDialog1.Document = printDocument1;
                printPreviewDialog1.PrintPreviewControl.Zoom = 1;
                ((Form)printPreviewDialog1).WindowState = FormWindowState.Maximized;
                printPreviewDialog1.ShowDialog();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "toolStripMenuIPrint_Click: " + hata.Message);
            }
        }
        Bitmap bitmap;
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            try
            {
                Tuple<int, int> tuple = null;
                tuple = GUI.FumitrackMainForm.getNewWidthandHeight(bitmap, e);

                e.Graphics.DrawImage(bitmap, 40, 150, tuple.Item1 + 400, tuple.Item2-170);
                //e.Graphics.DrawImage(bitmap, e.MarginBounds.Left + (e.MarginBounds.Width / 2) - (bitmap.Width / 2), 150);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "printDocument1_PrintPage: " + hata.Message);
            }
        }

        private void printDocument1_EndPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            panelAll.AutoScroll = true;
        }

        private void Report_Load(object sender, EventArgs e)
        {

        }
    }
}
