﻿namespace Fumitrack.Report
{
    partial class Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Report));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuIPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.panelAll = new System.Windows.Forms.Panel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label27 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label22 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label50 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lblFumiYpnAdSoyad1 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lblFumiYpnAdSoyad2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblYptrnFirma = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblYpldYer = new System.Windows.Forms.Label();
            this.lblFumiSekli = new System.Windows.Forms.Label();
            this.lblKonteynerNo = new System.Windows.Forms.Label();
            this.lblUrunCinsi = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblZararliAd = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lblNeMaksatlaY = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblAclsGunSaat = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblKapanGunSaat = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblFumiMktr = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblFumiCins = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblFumiDoz = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblFumiScklk = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblIlaclananHcm = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblAmbljSekli = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblUrunMiktar = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblAmbljAdet = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblKontrolNo = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lblEmtia = new System.Windows.Forms.Label();
            this.lblBasincTipi = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label45 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblKayitNo = new System.Windows.Forms.Label();
            this.lblTarih = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            this.panelAll.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuIPrint});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(106, 26);
            // 
            // toolStripMenuIPrint
            // 
            this.toolStripMenuIPrint.Name = "toolStripMenuIPrint";
            this.toolStripMenuIPrint.Size = new System.Drawing.Size(105, 22);
            this.toolStripMenuIPrint.Text = "Yazdır";
            this.toolStripMenuIPrint.Click += new System.EventHandler(this.toolStripMenuIPrint_Click);
            // 
            // printDocument1
            // 
            this.printDocument1.EndPrint += new System.Drawing.Printing.PrintEventHandler(this.printDocument1_EndPrint);
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Text = "Baskı önizleme";
            this.printPreviewDialog1.Visible = false;
            // 
            // panelAll
            // 
            this.panelAll.AutoScroll = true;
            this.panelAll.Controls.Add(this.tableLayoutPanel4);
            this.panelAll.Controls.Add(this.tableLayoutPanel3);
            this.panelAll.Controls.Add(this.tableLayoutPanel2);
            this.panelAll.Controls.Add(this.tableLayoutPanel1);
            this.panelAll.Controls.Add(this.panel2);
            this.panelAll.Controls.Add(this.panel1);
            this.panelAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAll.Location = new System.Drawing.Point(23, 60);
            this.panelAll.Name = "panelAll";
            this.panelAll.Size = new System.Drawing.Size(731, 703);
            this.panelAll.TabIndex = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.02007F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.97993F));
            this.tableLayoutPanel4.Controls.Add(this.label27, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 758);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 79F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 79F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(714, 79);
            this.tableLayoutPanel4.TabIndex = 26;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(402, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(241, 64);
            this.label27.TabIndex = 0;
            this.label27.Text = "Firma Sorumlusu/Company Responsible\r\nAdı Soyadı/Name and Surname\r\n\r\nİmzası/Signat" +
    "ure";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.22073F));
            this.tableLayoutPanel3.Controls.Add(this.label22, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 658);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 124F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 124F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(714, 100);
            this.tableLayoutPanel3.TabIndex = 25;
            // 
            // label22
            // 
            this.label22.Dock = System.Windows.Forms.DockStyle.Left;
            this.label22.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label22.Location = new System.Drawing.Point(3, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(677, 124);
            this.label22.TabIndex = 0;
            this.label22.Text = resources.GetString("label22.Text");
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.2437F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.7563F));
            this.tableLayoutPanel2.Controls.Add(this.label50, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label25, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblFumiYpnAdSoyad1, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label18, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label20, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblFumiYpnAdSoyad2, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label12, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 532);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(714, 126);
            this.tableLayoutPanel2.TabIndex = 24;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(3, 26);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(281, 16);
            this.label50.TabIndex = 0;
            this.label50.Text = "Adı Soyadı-Lisans/Name and Surname-License";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label25.Location = new System.Drawing.Point(3, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(417, 16);
            this.label25.TabIndex = 0;
            this.label25.Text = "Fumigasyonu Yapan Elamanın/Fumigation Personnel";
            // 
            // lblFumiYpnAdSoyad1
            // 
            this.lblFumiYpnAdSoyad1.AutoSize = true;
            this.lblFumiYpnAdSoyad1.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblFumiYpnAdSoyad1.Location = new System.Drawing.Point(3, 47);
            this.lblFumiYpnAdSoyad1.Name = "lblFumiYpnAdSoyad1";
            this.lblFumiYpnAdSoyad1.Size = new System.Drawing.Size(12, 16);
            this.lblFumiYpnAdSoyad1.TabIndex = 1;
            this.lblFumiYpnAdSoyad1.Text = "-";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 74);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(105, 16);
            this.label18.TabIndex = 0;
            this.label18.Text = "İmzası/Signature";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(426, 74);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(105, 16);
            this.label20.TabIndex = 0;
            this.label20.Text = "İmzası/Signature";
            // 
            // lblFumiYpnAdSoyad2
            // 
            this.lblFumiYpnAdSoyad2.AutoSize = true;
            this.lblFumiYpnAdSoyad2.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblFumiYpnAdSoyad2.Location = new System.Drawing.Point(426, 47);
            this.lblFumiYpnAdSoyad2.Name = "lblFumiYpnAdSoyad2";
            this.lblFumiYpnAdSoyad2.Size = new System.Drawing.Size(12, 16);
            this.lblFumiYpnAdSoyad2.TabIndex = 1;
            this.lblFumiYpnAdSoyad2.Text = "-";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(426, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(285, 16);
            this.label12.TabIndex = 0;
            this.label12.Text = "Adı Soyadı-Lisans/Name and Surname-License";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.60028F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55.39972F));
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblYptrnFirma, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label26, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblYpldYer, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblFumiSekli, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblKonteynerNo, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblUrunCinsi, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label24, 0, 19);
            this.tableLayoutPanel1.Controls.Add(this.lblZararliAd, 1, 19);
            this.tableLayoutPanel1.Controls.Add(this.label21, 0, 18);
            this.tableLayoutPanel1.Controls.Add(this.lblNeMaksatlaY, 1, 18);
            this.tableLayoutPanel1.Controls.Add(this.label19, 0, 17);
            this.tableLayoutPanel1.Controls.Add(this.label17, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.lblFumiMktr, 1, 15);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.lblFumiCins, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.lblFumiDoz, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.lblFumiScklk, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.lblIlaclananHcm, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.lblAmbljSekli, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.lblUrunMiktar, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblAmbljAdet, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblKontrolNo, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label23, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label28, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblEmtia, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblBasincTipi, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblKapanGunSaat, 1, 17);
            this.tableLayoutPanel1.Controls.Add(this.lblAclsGunSaat, 1, 16);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 89);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 20;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(714, 443);
            this.tableLayoutPanel1.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(301, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Fumigasyonu Yaptıran Firma/Fumigation Company";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(273, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Fumigasyonun Yapıldığı Yer/Fumigation Place";
            // 
            // lblYptrnFirma
            // 
            this.lblYptrnFirma.AutoSize = true;
            this.lblYptrnFirma.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblYptrnFirma.Location = new System.Drawing.Point(322, 1);
            this.lblYptrnFirma.Name = "lblYptrnFirma";
            this.lblYptrnFirma.Size = new System.Drawing.Size(12, 16);
            this.lblYptrnFirma.TabIndex = 1;
            this.lblYptrnFirma.Text = "-";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(211, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Fumigasyon Şekli/Fumigation Type";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(4, 64);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(87, 16);
            this.label26.TabIndex = 0;
            this.label26.Text = "Konteyner No";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(162, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "Ürünün Cinsi/Product Type";
            // 
            // lblYpldYer
            // 
            this.lblYpldYer.AutoSize = true;
            this.lblYpldYer.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblYpldYer.Location = new System.Drawing.Point(322, 22);
            this.lblYpldYer.Name = "lblYpldYer";
            this.lblYpldYer.Size = new System.Drawing.Size(12, 16);
            this.lblYpldYer.TabIndex = 1;
            this.lblYpldYer.Text = "-";
            // 
            // lblFumiSekli
            // 
            this.lblFumiSekli.AutoSize = true;
            this.lblFumiSekli.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblFumiSekli.Location = new System.Drawing.Point(322, 44);
            this.lblFumiSekli.Name = "lblFumiSekli";
            this.lblFumiSekli.Size = new System.Drawing.Size(12, 16);
            this.lblFumiSekli.TabIndex = 1;
            this.lblFumiSekli.Text = "-";
            // 
            // lblKonteynerNo
            // 
            this.lblKonteynerNo.AutoSize = true;
            this.lblKonteynerNo.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblKonteynerNo.Location = new System.Drawing.Point(322, 64);
            this.lblKonteynerNo.Name = "lblKonteynerNo";
            this.lblKonteynerNo.Size = new System.Drawing.Size(12, 16);
            this.lblKonteynerNo.TabIndex = 1;
            this.lblKonteynerNo.Text = "-";
            // 
            // lblUrunCinsi
            // 
            this.lblUrunCinsi.AutoSize = true;
            this.lblUrunCinsi.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblUrunCinsi.Location = new System.Drawing.Point(322, 83);
            this.lblUrunCinsi.Name = "lblUrunCinsi";
            this.lblUrunCinsi.Size = new System.Drawing.Size(12, 16);
            this.lblUrunCinsi.TabIndex = 1;
            this.lblUrunCinsi.Text = "-";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(4, 400);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(138, 16);
            this.label24.TabIndex = 0;
            this.label24.Text = "Zararlı Adı/ Pest Name";
            // 
            // lblZararliAd
            // 
            this.lblZararliAd.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblZararliAd.Location = new System.Drawing.Point(322, 400);
            this.lblZararliAd.Name = "lblZararliAd";
            this.lblZararliAd.Size = new System.Drawing.Size(385, 40);
            this.lblZararliAd.TabIndex = 1;
            this.lblZararliAd.Text = "-";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(4, 379);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(270, 16);
            this.label21.TabIndex = 0;
            this.label21.Text = "Ne Maksatla Yapıldığı/Purpose Of Fumigation";
            // 
            // lblNeMaksatlaY
            // 
            this.lblNeMaksatlaY.AutoSize = true;
            this.lblNeMaksatlaY.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblNeMaksatlaY.Location = new System.Drawing.Point(322, 379);
            this.lblNeMaksatlaY.Name = "lblNeMaksatlaY";
            this.lblNeMaksatlaY.Size = new System.Drawing.Size(12, 16);
            this.lblNeMaksatlaY.TabIndex = 1;
            this.lblNeMaksatlaY.Text = "-";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(4, 358);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(272, 16);
            this.label19.TabIndex = 0;
            this.label19.Text = "Fum. Bit. Günü /Fum. Comp. Day–Saati/Hour";
            // 
            // lblAclsGunSaat
            // 
            this.lblAclsGunSaat.AutoSize = true;
            this.lblAclsGunSaat.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblAclsGunSaat.Location = new System.Drawing.Point(322, 337);
            this.lblAclsGunSaat.Name = "lblAclsGunSaat";
            this.lblAclsGunSaat.Size = new System.Drawing.Size(12, 16);
            this.lblAclsGunSaat.TabIndex = 1;
            this.lblAclsGunSaat.Text = "-";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(4, 337);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(283, 16);
            this.label17.TabIndex = 0;
            this.label17.Text = "Fum. Baş. Günü /Fum. Comm. Day–Saati/Hour";
            // 
            // lblKapanGunSaat
            // 
            this.lblKapanGunSaat.AutoSize = true;
            this.lblKapanGunSaat.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblKapanGunSaat.Location = new System.Drawing.Point(322, 358);
            this.lblKapanGunSaat.Name = "lblKapanGunSaat";
            this.lblKapanGunSaat.Size = new System.Drawing.Size(12, 16);
            this.lblKapanGunSaat.TabIndex = 1;
            this.lblKapanGunSaat.Text = "-";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 316);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(264, 16);
            this.label16.TabIndex = 0;
            this.label16.Text = "Fumigantın Miktarı/Fumigant Of Amount (Gr)";
            // 
            // lblFumiMktr
            // 
            this.lblFumiMktr.AutoSize = true;
            this.lblFumiMktr.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblFumiMktr.Location = new System.Drawing.Point(322, 316);
            this.lblFumiMktr.Name = "lblFumiMktr";
            this.lblFumiMktr.Size = new System.Drawing.Size(12, 16);
            this.lblFumiMktr.TabIndex = 1;
            this.lblFumiMktr.Text = "-";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 295);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(302, 16);
            this.label15.TabIndex = 0;
            this.label15.Text = "Kullanılan Fumigantın Cinsi/Type of Fumigant Used";
            // 
            // lblFumiCins
            // 
            this.lblFumiCins.AutoSize = true;
            this.lblFumiCins.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblFumiCins.Location = new System.Drawing.Point(322, 295);
            this.lblFumiCins.Name = "lblFumiCins";
            this.lblFumiCins.Size = new System.Drawing.Size(101, 16);
            this.lblFumiCins.TabIndex = 1;
            this.lblFumiCins.Text = "Sulfuryl Fluoride";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 274);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(260, 16);
            this.label14.TabIndex = 0;
            this.label14.Text = "Fumigasyon Dozu/Fumigation Dose (Gr/m³)";
            // 
            // lblFumiDoz
            // 
            this.lblFumiDoz.AutoSize = true;
            this.lblFumiDoz.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblFumiDoz.Location = new System.Drawing.Point(322, 274);
            this.lblFumiDoz.Name = "lblFumiDoz";
            this.lblFumiDoz.Size = new System.Drawing.Size(12, 16);
            this.lblFumiDoz.TabIndex = 1;
            this.lblFumiDoz.Text = "-";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 253);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(299, 16);
            this.label13.TabIndex = 0;
            this.label13.Text = "Fumigasyon sıcaklığı/Fumigation Temperature (C°)";
            // 
            // lblFumiScklk
            // 
            this.lblFumiScklk.AutoSize = true;
            this.lblFumiScklk.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblFumiScklk.Location = new System.Drawing.Point(322, 253);
            this.lblFumiScklk.Name = "lblFumiScklk";
            this.lblFumiScklk.Size = new System.Drawing.Size(12, 16);
            this.lblFumiScklk.TabIndex = 1;
            this.lblFumiScklk.Text = "-";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 232);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(242, 16);
            this.label11.TabIndex = 0;
            this.label11.Text = "İlaçlanan Hacim/Fumigation Volume (m³)";
            // 
            // lblIlaclananHcm
            // 
            this.lblIlaclananHcm.AutoSize = true;
            this.lblIlaclananHcm.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblIlaclananHcm.Location = new System.Drawing.Point(322, 232);
            this.lblIlaclananHcm.Name = "lblIlaclananHcm";
            this.lblIlaclananHcm.Size = new System.Drawing.Size(12, 16);
            this.lblIlaclananHcm.TabIndex = 1;
            this.lblIlaclananHcm.Text = "-";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 211);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(303, 16);
            this.label10.TabIndex = 0;
            this.label10.Text = "Ambalaj Şekli ve Markası /Packing Type and Brand";
            // 
            // lblAmbljSekli
            // 
            this.lblAmbljSekli.AutoSize = true;
            this.lblAmbljSekli.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblAmbljSekli.Location = new System.Drawing.Point(322, 211);
            this.lblAmbljSekli.Name = "lblAmbljSekli";
            this.lblAmbljSekli.Size = new System.Drawing.Size(12, 16);
            this.lblAmbljSekli.TabIndex = 1;
            this.lblAmbljSekli.Text = "-";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 190);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(244, 16);
            this.label9.TabIndex = 0;
            this.label9.Text = "Ürünün Miktarı (Kg.)/ Quantity of Product";
            // 
            // lblUrunMiktar
            // 
            this.lblUrunMiktar.AutoSize = true;
            this.lblUrunMiktar.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblUrunMiktar.Location = new System.Drawing.Point(322, 190);
            this.lblUrunMiktar.Name = "lblUrunMiktar";
            this.lblUrunMiktar.Size = new System.Drawing.Size(12, 16);
            this.lblUrunMiktar.TabIndex = 1;
            this.lblUrunMiktar.Text = "-";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 169);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(192, 16);
            this.label8.TabIndex = 0;
            this.label8.Text = "Ambalaj Adeti/Packing Quantity";
            // 
            // lblAmbljAdet
            // 
            this.lblAmbljAdet.AutoSize = true;
            this.lblAmbljAdet.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblAmbljAdet.Location = new System.Drawing.Point(322, 169);
            this.lblAmbljAdet.Name = "lblAmbljAdet";
            this.lblAmbljAdet.Size = new System.Drawing.Size(12, 16);
            this.lblAmbljAdet.TabIndex = 1;
            this.lblAmbljAdet.Text = "-";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "Parti No/Lot Number";
            // 
            // lblKontrolNo
            // 
            this.lblKontrolNo.AutoSize = true;
            this.lblKontrolNo.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblKontrolNo.Location = new System.Drawing.Point(322, 148);
            this.lblKontrolNo.Name = "lblKontrolNo";
            this.lblKontrolNo.Size = new System.Drawing.Size(12, 16);
            this.lblKontrolNo.TabIndex = 1;
            this.lblKontrolNo.Text = "-";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(4, 106);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(112, 16);
            this.label23.TabIndex = 2;
            this.label23.Text = "Emtia/Commodity";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(4, 127);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(159, 16);
            this.label28.TabIndex = 2;
            this.label28.Text = "Basınç Tipi/Pressure Type";
            // 
            // lblEmtia
            // 
            this.lblEmtia.AutoSize = true;
            this.lblEmtia.Location = new System.Drawing.Point(322, 106);
            this.lblEmtia.Name = "lblEmtia";
            this.lblEmtia.Size = new System.Drawing.Size(12, 16);
            this.lblEmtia.TabIndex = 3;
            this.lblEmtia.Text = "-";
            // 
            // lblBasincTipi
            // 
            this.lblBasincTipi.AutoSize = true;
            this.lblBasincTipi.Location = new System.Drawing.Point(322, 127);
            this.lblBasincTipi.Name = "lblBasincTipi";
            this.lblBasincTipi.Size = new System.Drawing.Size(12, 16);
            this.lblBasincTipi.TabIndex = 3;
            this.lblBasincTipi.Text = "-";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label45);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.lblKayitNo);
            this.panel2.Controls.Add(this.lblTarih);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 48);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(714, 41);
            this.panel2.TabIndex = 21;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label45.Location = new System.Drawing.Point(490, 21);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(109, 16);
            this.label45.TabIndex = 0;
            this.label45.Text = "Kayıt No/Reg.No:";
            this.label45.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(490, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tarih / Date :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblKayitNo
            // 
            this.lblKayitNo.AutoSize = true;
            this.lblKayitNo.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblKayitNo.Location = new System.Drawing.Point(596, 21);
            this.lblKayitNo.Name = "lblKayitNo";
            this.lblKayitNo.Size = new System.Drawing.Size(12, 16);
            this.lblKayitNo.TabIndex = 1;
            this.lblKayitNo.Text = "-";
            // 
            // lblTarih
            // 
            this.lblTarih.AutoSize = true;
            this.lblTarih.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTarih.Location = new System.Drawing.Point(573, 3);
            this.lblTarih.Name = "lblTarih";
            this.lblTarih.Size = new System.Drawing.Size(12, 16);
            this.lblTarih.TabIndex = 1;
            this.lblTarih.Text = "-";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(714, 48);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(714, 48);
            this.label1.TabIndex = 0;
            this.label1.Text = " FUMİGASYON TUTANAĞI\r\nFUMIGATION CERTIFICATE";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(777, 788);
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.Controls.Add(this.panelAll);
            this.Font = new System.Drawing.Font("Arial", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.MaximizeBox = false;
            this.Name = "Report";
            this.Padding = new System.Windows.Forms.Padding(23, 60, 23, 25);
            this.Resizable = false;
            this.ShowIcon = false;
            this.Style = MetroFramework.MetroColorStyle.White;
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.Report_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panelAll.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuIPrint;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Panel panelAll;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.Label lblFumiYpnAdSoyad1;
        public System.Windows.Forms.Label lblFumiYpnAdSoyad2;
        public System.Windows.Forms.Label lblYptrnFirma;
        public System.Windows.Forms.Label lblYpldYer;
        public System.Windows.Forms.Label lblFumiSekli;
        public System.Windows.Forms.Label lblKonteynerNo;
        public System.Windows.Forms.Label lblUrunCinsi;
        public System.Windows.Forms.Label lblKontrolNo;
        public System.Windows.Forms.Label lblAmbljAdet;
        public System.Windows.Forms.Label lblUrunMiktar;
        public System.Windows.Forms.Label lblAmbljSekli;
        public System.Windows.Forms.Label lblIlaclananHcm;
        public System.Windows.Forms.Label lblFumiScklk;
        public System.Windows.Forms.Label lblFumiDoz;
        public System.Windows.Forms.Label lblFumiCins;
        public System.Windows.Forms.Label lblFumiMktr;
        public System.Windows.Forms.Label lblKapanGunSaat;
        public System.Windows.Forms.Label lblAclsGunSaat;
        public System.Windows.Forms.Label lblNeMaksatlaY;
        public System.Windows.Forms.Label lblZararliAd;
        public System.Windows.Forms.Label lblKayitNo;
        public System.Windows.Forms.Label lblTarih;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label28;
        public System.Windows.Forms.Label lblEmtia;
        public System.Windows.Forms.Label lblBasincTipi;
    }
}