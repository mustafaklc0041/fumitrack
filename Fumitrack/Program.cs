﻿using Fumitrack.GUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fumitrack
{
    static class Program
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static bool isFirstInstance = false;
        /// <summary>
        /// Uygulamanın ana girdi noktası.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                using (var mutex = new Mutex(true, "FumiTrack2019", out isFirstInstance))
                {
                    if (isFirstInstance)
                    {
                        log4net.Config.BasicConfigurator.Configure();
                        System.IO.Directory.CreateDirectory(@Path.Combine(Properties.Settings.Default.sistemDizini, "log"));

                        
                        Application.EnableVisualStyles();
                        Application.SetCompatibleTextRenderingDefault(false);
                        Application.Run(new LoginPage());
                        //Application.Run(new AdminPanelForm());
                        //Application.Run(new FumitrackMainForm());

                        GC.KeepAlive(mutex);
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception hata)
            {
                log.Error("Program- Main: " + hata.Message);
            }
        }
    }
}
