USE [Fumitrack]
GO
ALTER DATABASE [Fumitrack]collate Turkish_CI_AS
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[AlanBilgileri]    Script Date: 9.10.2019 22:45:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[AlanBilgileri]    Script Date: 22.11.2019 02:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AlanBilgileri](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DozajPlanID] [bigint] NULL,
	[AlanAdi] [nvarchar](100) NULL,
	[Sicaklik] [nvarchar](50) NULL,
	[YarilamaSuresi] [decimal](18, 2) NULL,
	[MaruzKalmaSuresi] [int] NULL,
	[AlanHacmi] [decimal](18, 2) NULL,
	[FumiMiktar] [decimal](18, 2) NULL,
	[SilindirSayisi] [decimal](18, 2) NULL,
	[HedefCT] [decimal](18, 2) NULL,
 CONSTRAINT [PK_AlanBilgileri] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DozajPlan]    Script Date: 22.11.2019 02:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DozajPlan](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FirmaID] [bigint] NULL,
	[IlaclamaTarih] [nvarchar](30) NULL,
	[FumiLisans1] [nvarchar](50) NULL,
	[FumiAdSoyad1] [nvarchar](150) NULL,
	[FumiLisans2] [nvarchar](50) NULL,
	[FumiAdSoyad2] [nvarchar](150) NULL,
	[YapildigiYer] [nvarchar](150) NULL,
	[KayitNo] [nvarchar](50) NULL,
	[UrunCinsi] [nvarchar](150) NULL,
	[UrunMiktari] [nvarchar](50) NULL,
	[KontrolNo] [nvarchar](50) NULL,
	[AmbalajAdedi] [nvarchar](50) NULL,
	[AmbalajSekli] [nvarchar](150) NULL,
	[NeMaksatlaY] [nvarchar](150) NULL,
	[KonteynerNo] [nvarchar](50) NULL,
	[HedefZararli] [nvarchar](250) NULL,
	[Dozaj] [nvarchar](10) NULL,
	[Emtia] [nvarchar](150) NULL,
	[FumiSekli] [nvarchar](150) NULL,
	[BasincTip] [nvarchar](50) NULL,
	[AcilisTarih] [nvarchar](30) NULL,
	[AcilisSaat] [nvarchar](30) NULL,
	[KapanisTarih] [nvarchar](30) NULL,
	[KapanisSaat] [nvarchar](30) NULL,
 CONSTRAINT [PK_DozajPlan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IzlemePlan]    Script Date: 22.11.2019 02:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IzlemePlan](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[AlanBilgileriID] [bigint] NULL,
	[IzlemeNoktasiAdi] [nvarchar](150) NULL,
	[OlcumNoktasi] [nvarchar](150) NULL,
	[HortumMesafesi] [nvarchar](50) NULL,
 CONSTRAINT [PK_IzlemePlan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IzlemeVeriGirdileri]    Script Date: 22.11.2019 02:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IzlemeVeriGirdileri](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DozajPlanID] [bigint] NULL,
	[AlanBilgileriID] [bigint] NULL,
	[Tarih] [nvarchar](15) NULL,
	[Saat] [nvarchar](10) NULL,
	[Sicaklik] [nvarchar](10) NULL,
	[IzlemeNokBilgID] [bigint] NULL,
	[OrtKonsantrasyon] [decimal](18, 1) NULL,
	[HatOrtKonsantrasyon] [nvarchar](10) NULL,
	[HLT] [decimal](18, 1) NULL,
	[Aciklama] [nvarchar](150) NULL,
 CONSTRAINT [PK_IzlemeVeriGirdileri] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Language]    Script Date: 22.11.2019 02:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Language](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FormName] [nvarchar](50) NULL,
	[ObjectName] [nvarchar](100) NULL,
	[Turkish] [nvarchar](150) NULL,
	[English] [nvarchar](150) NULL,
 CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UygulamaPlan]    Script Date: 22.11.2019 02:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UygulamaPlan](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[AlanBilgileriID] [bigint] NULL,
	[GirisHatti] [nvarchar](150) NULL,
	[MalzemeCinsi] [nvarchar](150) NULL,
	[SisCapi] [nvarchar](20) NULL,
	[Uzunluk] [nvarchar](20) NULL,
	[FanKapasitesi] [nvarchar](20) NULL,
	[FanSayisi] [nvarchar](20) NULL,
	[SilindirSayisi] [nvarchar](20) NULL,
 CONSTRAINT [PK_UygulamaPlan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UygulamaYapilanFirmalar]    Script Date: 22.11.2019 02:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UygulamaYapilanFirmalar](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FirmaAdi] [nvarchar](150) NULL,
	[Telefon] [nvarchar](20) NULL,
	[Eposta] [nvarchar](50) NULL,
	[Sehir] [nvarchar](50) NULL,
	[Adres] [nvarchar](500) NULL,
	[Aciklama] [nvarchar](500) NULL,
	[IslemYapanID] [bigint] NULL,
 CONSTRAINT [PK_UygulamaYapilanFirmalar] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Language] ON 
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (1, N'FumitrackMainForm', N'tabControl1', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (2, N'FumitrackMainForm', N'tabPageHistory', N'Geçmiş Uygulamalarım', N'Past Transactions')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (3, N'FumitrackMainForm', N'groupBox7', N'Geçmiş Uygulamalarım', N'Past Transactions')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (4, N'FumitrackMainForm', N'FirmaAdi', N'Firma Adı', N'Company Name')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (5, N'FumitrackMainForm', N'IlaclamaTarihi', N'İlaçlama Tarihi', N'Fumigation Date')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (6, N'FumitrackMainForm', N'DozajPlanIDGecmis', N'DozajPlanIDGecmis', N'Past Dosage Plan ID')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (7, N'FumitrackMainForm', N'dtgrdGecmisUyglm', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (8, N'FumitrackMainForm', N'panel4', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (9, N'FumitrackMainForm', N'tabPageCustomerInfo', N'Uygulama Yapılan Firma Bilgileri', N'Application Company Informations')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (10, N'FumitrackMainForm', N'panel19', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (11, N'FumitrackMainForm', N'btnFirmaKydt', N'Kaydet ve İlerle >>', N'Save and Next >>')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (12, N'FumitrackMainForm', N'groupBox1', N'Uygulama Yapılan Firma Bilgileri', N'Application Company Informations')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (13, N'FumitrackMainForm', N'cmbFirmaAdi', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (14, N'FumitrackMainForm', N'txtTelefon', N'(   )    -', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (15, N'FumitrackMainForm', N'txtAdres', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (16, N'FumitrackMainForm', N'txtAciklama', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (17, N'FumitrackMainForm', N'txtEposta', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (18, N'FumitrackMainForm', N'txtSehir', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (19, N'FumitrackMainForm', N'label5', N'Açıklama', N'Notes')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (20, N'FumitrackMainForm', N'label4', N'E-Posta Adresi', N'E-mail')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (21, N'FumitrackMainForm', N'label3', N'Telefon', N'Phone')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (22, N'FumitrackMainForm', N'label6', N'Şehir', N'City')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (23, N'FumitrackMainForm', N'label2', N'Adres', N'Adress')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (24, N'FumitrackMainForm', N'label1', N'*Firma İsmi', N'*Company Name')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (25, N'FumitrackMainForm', N'panel1', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (26, N'FumitrackMainForm', N'tabPageFumigationDosagePlan', N'Fumigasyon Dozaj Planı', N'Fumigation Dosage Plan')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (27, N'FumitrackMainForm', N'panelHedefZararli', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (28, N'FumitrackMainForm', N'btnHdfZararlIptal', N'İPTAL', N'CANCEL')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (29, N'FumitrackMainForm', N'btnHdfZararlTamam', N'TAMAM', N'OK')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (30, N'FumitrackMainForm', N'Secim', N'Seçim', N'Selection')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (31, N'FumitrackMainForm', N'HedefZararlilar', N'Hedef Zararlılar', N'Target Pests')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (32, N'FumitrackMainForm', N'dtgrdHedefZararli', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (33, N'FumitrackMainForm', N'panel24', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (34, N'FumitrackMainForm', N'groupBox4', N'Sonuçlar', N'Results')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (35, N'FumitrackMainForm', N'txtSunuc', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (36, N'FumitrackMainForm', N'panel13', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (37, N'FumitrackMainForm', N'btnDzjPlanKydt', N'Kaydet ve İlerle >>', N'Save and Next >>')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (38, N'FumitrackMainForm', N'panel25', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (39, N'FumitrackMainForm', N'btnDzjPlanYazdr', N' YAZDIR', N'PRINT')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (40, N'FumitrackMainForm', N'groupBox8', N'Yapı/Alan Bilgileri (Bir yapı birden fazla alana sahip olabilir)', N'Structure/Area Info(A structure may have more than one Area)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (41, N'FumitrackMainForm', N'panel12', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (42, N'FumitrackMainForm', N'AlanAdi', N'Alan Adı', N'Area Name')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (43, N'FumitrackMainForm', N'Sicaklik', N'Sıcaklık', N'Temperature')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (44, N'FumitrackMainForm', N'EstHLT', N'Yarılama Süresi', N'Half Time')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (45, N'FumitrackMainForm', N'MaruzKalmaSuresi', N'Maruz Kalma Süresi', N'Exposure Time')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (46, N'FumitrackMainForm', N'AlanHacmi', N'*Alan Hacmi (m3)', N'*Area Volume(m3)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (47, N'FumitrackMainForm', N'GerekliIlac', N'Kullanılacak Fumigant Miktarı (Gr)', N'Fumigant Required (Gr)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (48, N'FumitrackMainForm', N'KullanilacakSilindirSayisi', N'Kullanılacak Silindir Sayısı', N'Cylinder Required')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (49, N'FumitrackMainForm', N'HedefCT', N'Hedef CT (Gr)', N'Target CT(Gr)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (50, N'FumitrackMainForm', N'dtgrdYapiAlanBlg', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (51, N'FumitrackMainForm', N'panel8', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (52, N'FumitrackMainForm', N'btnDzjPlaniHesapla', N'Hesapla', N'Calculate')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (53, N'FumitrackMainForm', N'panel23', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (54, N'FumitrackMainForm', N'pictureBox1', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (55, N'FumitrackMainForm', N'txtAmbalajSekli', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (56, N'FumitrackMainForm', N'label25', N'Ambalaj Şekli ve Markası', N'Packing Type and Brand')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (57, N'FumitrackMainForm', N'txtAmbalajAdedi', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (58, N'FumitrackMainForm', N'label26', N'Ambalaj Adedi', N'Packing Quantity')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (59, N'FumitrackMainForm', N'txtPartiNo', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (60, N'FumitrackMainForm', N'label23', N'Parti No', N'Lot Number')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (61, N'FumitrackMainForm', N'txtUrunMiktari', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (62, N'FumitrackMainForm', N'label22', N'Ürünün Miktarı', N'Quantity of Product')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (63, N'FumitrackMainForm', N'txtUrunCinsi', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (64, N'FumitrackMainForm', N'label18', N'Ürünün Cinsi', N'Product Type')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (65, N'FumitrackMainForm', N'panel7', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (66, N'FumitrackMainForm', N'groupBox3', N'Hedef Bilgileri', N'Target Informations')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (67, N'FumitrackMainForm', N'btnHedefZararli', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (68, N'FumitrackMainForm', N'txtNeMaksatlaY', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (69, N'FumitrackMainForm', N'label24', N'Ne Maksatla Yapıldığı', N'Purpose Of Fumigation')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (70, N'FumitrackMainForm', N'txtKonteynerNo', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (71, N'FumitrackMainForm', N'label28', N'*Konteyner No', N'*Container Number')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (72, N'FumitrackMainForm', N'cmbEmtia', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (73, N'FumitrackMainForm', N'cmbEmtia', N'YOK', N'ABSENT')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (74, N'FumitrackMainForm', N'cmbEmtia', N'HUBUBAT', N'Grain')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (75, N'FumitrackMainForm', N'cmbEmtia', N'BAKLİYAT', N'Legume')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (76, N'FumitrackMainForm', N'cmbEmtia', N'KURU MEYVE', N'Dried Fruit')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (77, N'FumitrackMainForm', N'cmbEmtia', N'AHŞAP VE TÜREVLERİ', N'Wood products')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (78, N'FumitrackMainForm', N'cmbEmtia', N'KAĞIT-MUKAVVA', N'Paper-Paperboard')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (79, N'FumitrackMainForm', N'cmbEmtia', N'AVUST. WEB SAYFASINDAKİ İLGİLİ ÜRÜNLER', N'Related products on the Australian website')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (80, N'FumitrackMainForm', N'cmbBsncTip', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (81, N'FumitrackMainForm', N'cmbBsncTip', N'ATMOSFERİK FUMİGASYON', N'Atmospheric Fumigation')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (82, N'FumitrackMainForm', N'cmbBsncTip', N'VAKUM FUMİGASYON', N'Vacuum Fumigation')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (83, N'FumitrackMainForm', N'cmbFumiSekli', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (84, N'FumitrackMainForm', N'cmbFumiSekli', N'GEMİ FUMİGASYONU', N'Ship Fumigation')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (85, N'FumitrackMainForm', N'cmbFumiSekli', N'BİNA FUMİGASYONU', N'Building  Fumigation')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (86, N'FumitrackMainForm', N'cmbFumiSekli', N'ÖRTÜALTI FUMİGASYONU', N'Sheeted Fumigation')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (87, N'FumitrackMainForm', N'cmbFumiSekli', N'KONTEYNER FUMİGASYONU', N'Container Fumigation')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (88, N'FumitrackMainForm', N'cmbFumiSekli', N'SİLO FUMİGASYONU', N'Silo Fumigation')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (89, N'FumitrackMainForm', N'cmbFumiSekli', N'VAKUM FUMİGASYONU', N'Vacuum Fumigation')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (90, N'FumitrackMainForm', N'cmbDozaj', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (91, N'FumitrackMainForm', N'cmbDozaj', N'15', N'15')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (92, N'FumitrackMainForm', N'cmbDozaj', N'16', N'16')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (93, N'FumitrackMainForm', N'cmbDozaj', N'20', N'20')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (94, N'FumitrackMainForm', N'cmbDozaj', N'24', N'24')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (95, N'FumitrackMainForm', N'cmbDozaj', N'50', N'50')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (96, N'FumitrackMainForm', N'cmbDozaj', N'100', N'100')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (99, N'FumitrackMainForm', N'label7', N'Basınç Tipi', N'Pressure Type')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (100, N'FumitrackMainForm', N'label14', N'Fumigasyon Şekli', N'Fumigation Type')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (101, N'FumitrackMainForm', N'label13', N'*Emtia', N'*Goods')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (102, N'FumitrackMainForm', N'label12', N'*Dozaj', N'*Dosage')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (103, N'FumitrackMainForm', N'label11', N'*Hedef Zararlıları', N'*Target Pests')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (104, N'FumitrackMainForm', N'groupBox2', N'Genel Bilgiler', N'General Informations')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (105, N'FumitrackMainForm', N'txtFumLisans2', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (106, N'FumitrackMainForm', N'label27', N'Fumigasyonu Yapan Lisansı2', N'Fumigation Personnel2 License')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (107, N'FumitrackMainForm', N'txtFumAdSoyad2', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (108, N'FumitrackMainForm', N'label17', N'Fumigasyonu Yapan Ad Soyad2', N'Fumigation Personnel2 Name Surname')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (109, N'FumitrackMainForm', N'txtFumAdSoyad1', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (110, N'FumitrackMainForm', N'label16', N'*Fumigasyonu Yapan Ad Soyad', N'*Fumigation Personnel Name Surname')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (111, N'FumitrackMainForm', N'txtYapildigiYer', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (112, N'FumitrackMainForm', N'txtKayitNo', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (113, N'FumitrackMainForm', N'label8', N'Kayıt No', N'Registration Number')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (114, N'FumitrackMainForm', N'label15', N'Yapıldığı Yer', N'Fumigation Place')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (115, N'FumitrackMainForm', N'txtFumLisans1', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (116, N'FumitrackMainForm', N'dtIlaclamaTrh', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (117, N'FumitrackMainForm', N'label10', N'*Fumigasyonu Yapan Lisansı', N'*Fumigation Personnel License')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (119, N'FumitrackMainForm', N'cmbHedefZararli', N'KIRMA BİTİ (Tribolium confusem)', N'Confused Flour Beetle(Tribolium confusem)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (120, N'FumitrackMainForm', N'cmbHedefZararli', N'PİRİNÇ BİTİ (Sitophilus oryzae)', N'Rice Weewill (Sitophilus oryzae)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (121, N'FumitrackMainForm', N'cmbHedefZararli', N'UN BİTİ (Tribolium castaneum)', N'Red Flour Beetle(Tribolium castaneum)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (122, N'FumitrackMainForm', N'cmbHedefZararli', N'BUĞDAY BİTİ (Sitophilus granarius)', N'Grain Weevil (Sitophilus granarius)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (123, N'FumitrackMainForm', N'cmbHedefZararli', N'TESTERELİ BÖCEK (Oryzephilus surinamensis)', N'Saw-toothed Grain beetle (Oryzephilus surinamensis)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (124, N'FumitrackMainForm', N'cmbHedefZararli', N'TÜRK HUBUBAT ZARARLISI (Cryptolestes turcicus)', N'Turkish Grain Turcicus (Cryptolestes turcicus)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (125, N'FumitrackMainForm', N'cmbHedefZararli', N'EKİN KANBUR BİTİ (Rhizopertha dominica)', N'Lasser Grain Borer (Rhizopertha dominica)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (126, N'FumitrackMainForm', N'cmbHedefZararli', N'EKİN KARA BÖCEĞİ (Tenebroides mauritanicus)', N'Cadella (Tenebroides mauritanicus)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (127, N'FumitrackMainForm', N'cmbHedefZararli', N'KHAPRA BÖCEĞİ (Trogoderma granarium)', N'Khapra Beetle (Trogoderma granarium)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (128, N'FumitrackMainForm', N'cmbHedefZararli', N'ECZANE BÖCEĞİ (Stegobium paniceum)', N'Drugtore Beetle (Stegobium paniceum)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (129, N'FumitrackMainForm', N'cmbHedefZararli', N'KİLER BÖCEĞİ (Dermestes lardarius)', N'Larder Beetle (Dermestes lardarius)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (130, N'FumitrackMainForm', N'cmbHedefZararli', N'KURUTULMUŞ ÜRÜN ZARARLISI (Necrobia rufipes)', N'Copra Bettle (Necrobia rufipes)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (131, N'FumitrackMainForm', N'cmbHedefZararli', N'KAHVE ÇEKİRDEĞİ BİTİ (Aracerus fasciculatus)', N'Coffe Bean Weevil (Aracerus fasciculatus)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (132, N'FumitrackMainForm', N'cmbHedefZararli', N'YER FISTIĞI BÖCEĞİ (Caryedon serratus)', N'Groundnut Borer (Caryedon serratus)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (133, N'FumitrackMainForm', N'cmbHedefZararli', N'FASULYE TOHUMU BÖCEĞİ (Acanthoscelides obtectus)', N'Bean Weevil  (Acanthoscelides obtectus)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (134, N'FumitrackMainForm', N'cmbHedefZararli', N'TATLI KURT (Lasioderma serricorne)', N'Tobacco or cigarette Beetle (Lasioderma serricorne)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (135, N'FumitrackMainForm', N'cmbHedefZararli', N'ARPA GÜVESİ (Sitotroga cerealella)', N'Angoumois Grain Moth (Sitotroga cerealella)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (136, N'FumitrackMainForm', N'cmbHedefZararli', N'DEĞİRMEN GÜVESİ (Ephestia kuehniella)', N'Mediterranean Flour Moth (Ephestia kuehniella)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (137, N'FumitrackMainForm', N'cmbHedefZararli', N'TÜTÜN GÜVESİ (Ephestia elutella)', N'Cocoa-bean or Tobacco Moth (Ephestia elutella)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (138, N'FumitrackMainForm', N'cmbHedefZararli', N'KURU MEYVE GÜVESİ (Plodia interpunctella)', N'Indian Meal Moth (Plodia interpunctella)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (139, N'FumitrackMainForm', N'cmbHedefZararli', N'AKARLAR (Acarus siro)', N'Flour Mite (Acarus siro)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (140, N'FumitrackMainForm', N'cmbHedefZararli', N'MISIR BİTİ (Sitophilus zeamais)', N'Maize Weevil (Sitophilus zeamais)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (141, N'FumitrackMainForm', N'cmbHedefZararli', N'GENİŞ-BOYNUZLU BÖCEK (Gnathocerus cornutus)', N'Coleotera, Tenebrionidae (Gnathocerus cornutus)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (142, N'FumitrackMainForm', N'cmbHedefZararli', N'KARA UN BİTİ (Tribolium destructor)', N'Coleotera, Tenebrionidae (Tribolium destructor)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (143, N'FumitrackMainForm', N'cmbHedefZararli', N'KAKAO GÜVESİ (Ephestia cautella)', N'Chocolate Moth (Ephestia cautella)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (144, N'FumitrackMainForm', N'cmbHedefZararli', N'EKŞİLİK BÖCEKLERİ (Carpophilus dimidiatus)', N'Coleptera, Nitidulidea (Carpophilus dimidiatus)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (145, N'FumitrackMainForm', N'cmbHedefZararli', N'PATATES GÜVESİ (Phthorimoea operculellla)', N'Potato Moth (Phthorimoea operculellla)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (146, N'FumitrackMainForm', N'cmbHedefZararli', N'BEZELYE TOHUM BÖCEĞİ (Bruchus pisorum)', N'Pea Beetle (Bruchus pisorum)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (147, N'FumitrackMainForm', N'cmbHedefZararli', N'BATI ÇİÇEK THRİPSİ (Frankliniella occidentalis)', N'Pergande (Frankliniella occidentalis)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (148, N'FumitrackMainForm', N'cmbHedefZararli', N'TAHTA KURUSU (Cimex lectularius)', N'Bedbugs (Cimex lectularius)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (149, N'FumitrackMainForm', N'cmbHedefZararli', N'ELBİSE GÜVESİ (Order lepidoptera)', N'Clothes Math (Order lepidoptera)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (150, N'FumitrackMainForm', N'cmbHedefZararli', N'HALI BÖCEĞİ (Anthrenus verbasci)', N'Carpet Beetles (Anthrenus verbasci)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (151, N'FumitrackMainForm', N'cmbHedefZararli', N'HAMAM BÖCEĞİ (Blatta orientalis)', N'Cockroaches (Blatta orientalis)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (152, N'FumitrackMainForm', N'cmbHedefZararli', N'KURU ODUN TERMİTLERİ (Kalotermes flavicollis)', N'Drywood Termites (Kalotermes flavicollis)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (153, N'FumitrackMainForm', N'cmbHedefZararli', N'MOBİLYA BİTİ (Anobium punctatum)', N'Furniture Beetle (Anobium punctatum)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (154, N'FumitrackMainForm', N'cmbHedefZararli', N'AHŞAP DELİCİ BÖCEKLER (Xestobilum rufovillosum)', N'The Deathwatch Beetle (Xestobilum rufovillosum)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (155, N'FumitrackMainForm', N'cmbHedefZararli', N'KAHVERENGİ KOKMUŞ BÖCEK-BMSB (Halyomorpha halys)', N'Brown Mormorated Stink Bugs (Halyomorpha halys)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (156, N'FumitrackMainForm', N'label9', N'İlaçlama Tarihi', N'Fumigation Date')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (157, N'FumitrackMainForm', N'panel2', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (158, N'FumitrackMainForm', N'tabPageIntroductionPlan', N'Uygulama Planı', N'Transaction Plan')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (159, N'FumitrackMainForm', N'panel21', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (160, N'FumitrackMainForm', N'btnUyglmPlnKydt', N'Kaydet ve İlerle >>', N'Save and Next >>')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (161, N'FumitrackMainForm', N'panel5', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (162, N'FumitrackMainForm', N'btnUyglmPlanYzdr', N' YAZDIR', N'PRINT')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (163, N'FumitrackMainForm', N'groupBox6', N'Giriş Hatlarını ve Fanları Girin', N'INPUT Introduction Line and Fan Capacity')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (164, N'FumitrackMainForm', N'AlanAdi2', N'*Alan Adı', N'*Area Name')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (165, N'FumitrackMainForm', N'GirisHatti', N'Giriş Hattı', N'Introduction Line')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (166, N'FumitrackMainForm', N'RH', N'Dağıtımda Kullanılack Malzeme Cinsi', N'Type of Material to be used in Distribution')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (167, N'FumitrackMainForm', N'Silindir', N'Dağıtım Sis Çapı', N'Diameter of Distribution Material')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (168, N'FumitrackMainForm', N'Uzunluk', N'Uzunluk', N'Length')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (169, N'FumitrackMainForm', N'FanKapasitesi', N'Fan Kapasitesi', N'Fan Capacity')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (170, N'FumitrackMainForm', N'FanSayisi', N'Fan Sayısı', N'Number of Fun')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (171, N'FumitrackMainForm', N'SilindirSayisi', N'Silindir Sayısı', N'Number of Cylinder')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (172, N'FumitrackMainForm', N'dtgrdUyglmPlan', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (173, N'FumitrackMainForm', N'panel3', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (174, N'FumitrackMainForm', N'tabPageMonitoringPlan', N'İzleme Planı', N'Monitoring Plan')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (175, N'FumitrackMainForm', N'label29', N'Satırı silmek isterseniz satırı seçtikten sonra "Delete" tuşuna basınız...', N'If you want to delete the line, Select the line and than Push "Delete" button')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (176, N'FumitrackMainForm', N'panel22', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (177, N'FumitrackMainForm', N'btnIzlmPlnKydt', N'Kaydet ve İlerle >>', N'Save and Next >>')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (178, N'FumitrackMainForm', N'panel14', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (179, N'FumitrackMainForm', N'btnIzlemePlanYazdr', N' YAZDIR', N'PRINT')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (180, N'FumitrackMainForm', N'groupBox9', N'İlaçlama İzleme Bilgilerini Girin', N'Enter Fumigant Monitoring Information Here')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (181, N'FumitrackMainForm', N'AlanAdi4', N'Alan Adı', N'Area Name')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (182, N'FumitrackMainForm', N'IzlemeNoktasiAdi2', N'*İzleme Noktası Adı', N'*Monitor Point Name')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (183, N'FumitrackMainForm', N'OlcumNoktasi', N'Ölçüm Noktası', N'Location')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (184, N'FumitrackMainForm', N'HortumMesafesi', N'Hortum Mesafesi', N'Hose Distance')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (185, N'FumitrackMainForm', N'dtgrdIzlemePlan', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (186, N'FumitrackMainForm', N'panel6', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (187, N'FumitrackMainForm', N'tabPageMonitoringDataInput', N'İzleme Veri Girdileri', N'Monitoring Data Input')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (188, N'FumitrackMainForm', N'groupBox5', N'Sonuçlar', N'Results')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (189, N'FumitrackMainForm', N'txtSonucIzlemeVeriGirdi', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (190, N'FumitrackMainForm', N'panel20', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (191, N'FumitrackMainForm', N'btnIzlemeVeriGrafk', N' GRAFİK', N'Graph')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (192, N'FumitrackMainForm', N'panel26', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (193, N'FumitrackMainForm', N'btnIzlemeVeriGirdiYazdr', N' YAZDIR', N'PRINT')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (194, N'FumitrackMainForm', N'groupBox12', N'Veri Girişini İzleme', N'Monitoring Status')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (195, N'FumitrackMainForm', N'panel17', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (196, N'FumitrackMainForm', N'AlanAdi3', N'Alan Adı', N'Area Name')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (197, N'FumitrackMainForm', N'Tarih', N'Tarih', N'Date')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (198, N'FumitrackMainForm', N'Zaman', N'*Saat Verisi (Ör:09:35)', N'*Time Data(Ex:09:35)')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (199, N'FumitrackMainForm', N'IzlemeNoktasiAdi', N'İzleme Noktası Adı', N'Monitor Point Name')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (200, N'FumitrackMainForm', N'IzlemeNoktasiID', N'IzlemeNoktasiID', N'Monitor Point ID')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (201, N'FumitrackMainForm', N'Konsantrasyon', N'*Ölçülen Konsantrasyon', N'*Measured Concentration')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (202, N'FumitrackMainForm', N'HatOrtKonsantrasyon', N'Ortalama Konsantrasyon', N'Average Concentration')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (203, N'FumitrackMainForm', N'HLT', N'HLT', N'HLT')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (204, N'FumitrackMainForm', N'Aciklama2', N'Açıklama', N'Notes')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (205, N'FumitrackMainForm', N'dtgrdIzlemeVeriGrdi', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (206, N'FumitrackMainForm', N'panel11', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (207, N'FumitrackMainForm', N'btnVeriHesapla', N'HESAPLA', N'Calculate')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (208, N'FumitrackMainForm', N'groupBox13', N'Veri Filtresi', N'Data Filter')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (209, N'FumitrackMainForm', N'listBoxVeriFiltre', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (210, N'FumitrackMainForm', N'panel10', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (211, N'FumitrackMainForm', N'pictureBox2', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (212, N'FumitrackMainForm', N'dtBasSaat', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (213, N'FumitrackMainForm', N'dtBasTarih', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (214, N'FumitrackMainForm', N'label21', N'Saat', N'Time')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (215, N'FumitrackMainForm', N'label20', N'Tarih', N'Date')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (216, N'FumitrackMainForm', N'label19', N'Başlangıç Zamanı', N'Start Time')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (217, N'FumitrackMainForm', N'panel9', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (218, N'FumitrackMainForm', N'tabPageGraph', N'Fumigasyon Konsantrasyon Zaman Grafiği', N'Fumigation Concentration Time Graph')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (219, N'FumitrackMainForm', N'panel18', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (220, N'FumitrackMainForm', N'Alt Sınır', N'Alt Sınır', N'Sub-Limit')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (221, N'FumitrackMainForm', N'Gerçekleşen', N'Gerçekleşen', N'Achieved')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (224, N'FumitrackMainForm', N'chartZamanGrafk', N'chart1', N'chart1')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (225, N'FumitrackMainForm', N'panel16', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (226, N'FumitrackMainForm', N'lblGrfkIlaveMesaj', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (227, N'FumitrackMainForm', N'lblGrfkBaslik', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (228, N'FumitrackMainForm', N'btnZamanGrafikYazdr', N' YAZDIR', N'PRINT')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (229, N'FumitrackMainForm', N'groupBox14', N'Veri Filtresi', N'Data Filter')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (230, N'FumitrackMainForm', N'listBoxGrafkFiltre', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (231, N'FumitrackMainForm', N'panel15', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (232, N'FumitrackMainForm', N'fileToolStripMenuItem', N'Dosya', N'File')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (233, N'FumitrackMainForm', N'NewtoolStripMenuItem', N'Yeni Proje', N'New Project')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (234, N'FumitrackMainForm', N'ReporttoolStripMenuItem', N'Rapor', N'Report')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (235, N'FumitrackMainForm', N'normalRaporToolStripMenuItem', N'Fumigasyon Raporu', N'Fumigation Report')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (236, N'FumitrackMainForm', N'avustralyaRaporToolStripMenuItem', N'BMSB-Sulfuryl-Fluoride-Treatment-Certificate', N'BMSB-Sulfuryl-Fluoride-Treatment-Certificate')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (237, N'FumitrackMainForm', N'turkceToolStripMenuItem', N'Türkçe', N'Turkish')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (238, N'FumitrackMainForm', N'ingilizceToolStripMenuItem', N'İngilizce', N'English')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (239, N'FumitrackMainForm', N'avustralyaRapor2ToolStripMenuItem', N'BMSB-Sulfuryl-Metric', N'BMSB-Sulfuryl-Metric')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (240, N'FumitrackMainForm', N'turkce2ToolStripMenuItem', N'Türkçe', N'Turkish')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (241, N'FumitrackMainForm', N'ingilizce2ToolStripMenuItem1', N'İngilizce', N'English')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (242, N'FumitrackMainForm', N'avustralyaRapor3ToolStripMenuItem', N'BMSB-Sulfuryl-Imperial', N'BMSB-Sulfuryl-Imperial')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (243, N'FumitrackMainForm', N'turkce3ToolStripMenuItem1', N'Türkçe', N'Turkish')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (244, N'FumitrackMainForm', N'ingilizce3ToolStripMenuItem2', N'İngilizce', N'English')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (245, N'FumitrackMainForm', N'avustralyaRapor4ToolStripMenuItem', N'BMSB-Sulfuryl-3rd-Party', N'BMSB-Sulfuryl-3rd-Party')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (246, N'FumitrackMainForm', N'turkce4ToolStripMenuItem2', N'Türkçe', N'Turkish')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (247, N'FumitrackMainForm', N'ingilizce4ToolStripMenuItem3', N'İngilizce', N'English')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (248, N'FumitrackMainForm', N'LogoutToolStripMenuItem', N'Oturumu Kapat', N'Log off')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (249, N'FumitrackMainForm', N'helpToolStripMenuItem', N'Yardım', N'Help')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (250, N'FumitrackMainForm', N'aboutToolStripMenuItem', N'Hakkında', N'About')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (251, N'FumitrackMainForm', N'dilToolStripMenuItem', N'Dil', N'Language')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (252, N'FumitrackMainForm', N'türkçeToolStripMenuItem', N'Türkçe', N'Turkish')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (253, N'FumitrackMainForm', N'ingilizceToolStripMenuItem1', N'İngilizce', N'English')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (254, N'FumitrackMainForm', N'menuStrip1', N'menuStrip1', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (255, N'AdminPanelForm', N'tabControl1', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (256, N'AdminPanelForm', N'tabPageUserCreate', N'Uygulayıcı Firma Ekleme', N'ADD Implementing Company')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (257, N'AdminPanelForm', N'btnTemizle_YP', N'TEMİZLE', N'CLEAR')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (258, N'AdminPanelForm', N'btnKaydet_YP', N'KAYDET', N'SAVE')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (259, N'AdminPanelForm', N'groupBox1', N'Uygulayıcı Firma Bilgileri', N'Implementing Company Informations')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (260, N'AdminPanelForm', N'txtTelefon_YP', N'(   )    -', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (261, N'AdminPanelForm', N'txtAdres_YP', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (262, N'AdminPanelForm', N'txtAciklama_YP', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (263, N'AdminPanelForm', N'txtEposta_YP', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (264, N'AdminPanelForm', N'txtSehir_YP', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (265, N'AdminPanelForm', N'txtSifre_YP', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (266, N'AdminPanelForm', N'txtKullAdi_YP', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (267, N'AdminPanelForm', N'txtFirmaAd_YP', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (268, N'AdminPanelForm', N'label8', N'*Şifre', N'*Password')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (269, N'AdminPanelForm', N'label7', N'*Kullanıcı Adı', N'*User Name')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (270, N'AdminPanelForm', N'label5', N'Açıklama', N'Notes')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (271, N'AdminPanelForm', N'label4', N'E-Posta Adresi', N'E-mail')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (272, N'AdminPanelForm', N'label3', N'Telefon', N'Phone')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (273, N'AdminPanelForm', N'label6', N'Şehir', N'City')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (274, N'AdminPanelForm', N'label2', N'Adres', N'Adress')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (275, N'AdminPanelForm', N'label1', N'*Firma İsmi', N'*Company Name')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (276, N'AdminPanelForm', N'tabPageUsers', N'Uygulayıcı Firma Listesi', N'List of Implementing Companies')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (277, N'AdminPanelForm', N'groupBox4', N'Uygulayıcı Firma Listesi', N'List of Implementing Companies')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (278, N'AdminPanelForm', N'panel1', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (279, N'AdminPanelForm', N'dtgrdFirmaListesi', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (280, N'AdminPanelForm', N'label9', N'İşlem yapmak için satıra sağ tıklayınız...', N'Right click on the line to make an action…')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (281, N'LoginPage', N'label5', N'FumiTrack', N'FumiTrack')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (282, N'LoginPage', N'pictureBox2', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (283, N'LoginPage', N'lblLoginHata', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (284, N'LoginPage', N'label3', N'Hoş Geldiniz', N'Welcome')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (285, N'LoginPage', N'pictureBox1', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (286, N'LoginPage', N'checkBoxRememberMe', N'Beni Hatırla', N'Remember Me')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (287, N'LoginPage', N'btnLogin', N'GİRİŞ', N'LOGIN')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (288, N'LoginPage', N'txtPasswd', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (289, N'LoginPage', N'txtUserName', N'', N'')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (290, N'LoginPage', N'label2', N'Parola', N'Password')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (291, N'LoginPage', N'label1', N'Kullanıcı Adı', N'User Name')
INSERT [dbo].[Language] ([ID], [FormName], [ObjectName], [Turkish], [English]) VALUES (292, N'FumitrackMainForm', N'Sicaklik2', N'Sıcaklık', N'Temperature')
SET IDENTITY_INSERT [dbo].[Language] OFF
ALTER TABLE [dbo].[DozajPlan] ADD  CONSTRAINT [DF_DozajPlan_AcilisTarih]  DEFAULT ('') FOR [AcilisTarih]
GO
ALTER TABLE [dbo].[DozajPlan] ADD  CONSTRAINT [DF_DozajPlan_AcilisSaat]  DEFAULT ('') FOR [AcilisSaat]
GO
ALTER TABLE [dbo].[DozajPlan] ADD  CONSTRAINT [DF_DozajPlan_KapanisTarih]  DEFAULT ('') FOR [KapanisTarih]
GO
ALTER TABLE [dbo].[DozajPlan] ADD  CONSTRAINT [DF_DozajPlan_KapanisSaat]  DEFAULT ('') FOR [KapanisSaat]
GO
ALTER TABLE [dbo].[Language] ADD  CONSTRAINT [DF_Language_FormName]  DEFAULT ('') FOR [FormName]
GO
ALTER TABLE [dbo].[Language] ADD  CONSTRAINT [DF_Language_ObjectName]  DEFAULT ('') FOR [ObjectName]
GO
ALTER TABLE [dbo].[Language] ADD  CONSTRAINT [DF_Language_Turkish]  DEFAULT ('') FOR [Turkish]
GO
ALTER TABLE [dbo].[Language] ADD  CONSTRAINT [DF_Language_English]  DEFAULT ('') FOR [English]
GO
ALTER TABLE [dbo].[AlanBilgileri]  WITH CHECK ADD  CONSTRAINT [FK_AlanBilgileri_DozajPlan] FOREIGN KEY([DozajPlanID])
REFERENCES [dbo].[DozajPlan] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AlanBilgileri] CHECK CONSTRAINT [FK_AlanBilgileri_DozajPlan]
GO
ALTER TABLE [dbo].[IzlemePlan]  WITH CHECK ADD  CONSTRAINT [FK_IzlemePlan_AlanBilgileri] FOREIGN KEY([AlanBilgileriID])
REFERENCES [dbo].[AlanBilgileri] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[IzlemePlan] CHECK CONSTRAINT [FK_IzlemePlan_AlanBilgileri]
GO
ALTER TABLE [dbo].[IzlemeVeriGirdileri]  WITH CHECK ADD  CONSTRAINT [FK_IzlemeVeriGirdileri_DozajPlan] FOREIGN KEY([DozajPlanID])
REFERENCES [dbo].[DozajPlan] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[IzlemeVeriGirdileri] CHECK CONSTRAINT [FK_IzlemeVeriGirdileri_DozajPlan]
GO
ALTER TABLE [dbo].[UygulamaPlan]  WITH CHECK ADD  CONSTRAINT [FK_UygulamaPlan_AlanBilgileri] FOREIGN KEY([AlanBilgileriID])
REFERENCES [dbo].[AlanBilgileri] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UygulamaPlan] CHECK CONSTRAINT [FK_UygulamaPlan_AlanBilgileri]
GO
USE [master]
GO
ALTER DATABASE [Fumitrack] SET READ_WRITE