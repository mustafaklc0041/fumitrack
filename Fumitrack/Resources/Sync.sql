USE [FumiTrack];
GO
ALTER TABLE [dbo].[AlanBilgileri]  WITH CHECK ADD  CONSTRAINT [FK_AlanBilgileri_DozajPlan] FOREIGN KEY([DozajPlanID])
REFERENCES [dbo].[DozajPlan] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AlanBilgileri] CHECK CONSTRAINT [FK_AlanBilgileri_DozajPlan]
GO
ALTER TABLE [dbo].[IzlemePlan]  WITH CHECK ADD  CONSTRAINT [FK_IzlemePlan_AlanBilgileri] FOREIGN KEY([AlanBilgileriID])
REFERENCES [dbo].[AlanBilgileri] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[IzlemePlan] CHECK CONSTRAINT [FK_IzlemePlan_AlanBilgileri]
GO
ALTER TABLE [dbo].[IzlemeVeriGirdileri]  WITH CHECK ADD  CONSTRAINT [FK_IzlemeVeriGirdileri_DozajPlan] FOREIGN KEY([DozajPlanID])
REFERENCES [dbo].[DozajPlan] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[IzlemeVeriGirdileri] CHECK CONSTRAINT [FK_IzlemeVeriGirdileri_DozajPlan]
GO
ALTER TABLE [dbo].[UygulamaPlan]  WITH CHECK ADD  CONSTRAINT [FK_UygulamaPlan_AlanBilgileri] FOREIGN KEY([AlanBilgileriID])
REFERENCES [dbo].[AlanBilgileri] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UygulamaPlan] CHECK CONSTRAINT [FK_UygulamaPlan_AlanBilgileri]
GO
DELETE FROM [dbo].[Language] WHERE ID IN ('222','223')
GO
ALTER TABLE [dbo].[IzlemeVeriGirdileri] ADD [Sicaklik] NVARCHAR (10) NULL;
GO
INSERT [dbo].[Language] ([FormName],[ObjectName],[Turkish],[English]) VALUES (N'FumitrackMainForm', N'Sicaklik2', N'Sıcaklık', N'Temperature')