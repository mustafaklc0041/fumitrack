﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Globalization;

namespace Fumitrack
{
    class MAC
    {
        private static string fingerPrint = string.Empty;
        public static string Value()
        {
            try
            {
                if (string.IsNullOrEmpty(fingerPrint))
                {
                    fingerPrint = GetHash("CPU >> " + cpuId() + "\nBIOS >> " + biosId() + "\nBASE >> " + baseId()
                                         + "\nDISK >> " + diskId() + "\nVIDEO >> " + videoId() + "\nMAC >> " + "SDF.1dl0S"
                                         //+ "\nDISK >> " + diskId() + "\nVIDEO >> " + videoId() + "\nMAC >> " + macId()
                                         );
                }

                if (dd) asd(fingerPrint);

                str2 = fingerPrint;
            }
            catch (Exception hata)
            {
               loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,"MAC.cs- Value: " + hata.Message);
            }

            return fingerPrint;
        }
        public static string str1 = "A5EE-1CBC-88F3-CDED-2CB8-62AD-DF3E-A23E"; //47BB-C757-132A-A0C7-C129-9C78-E9D5-40D3
        public static string str2 = "";

        private static byte[] Keys = null;
        private static bool dd = false;

        public static bool isOK(string str, bool ww)
        {
            bool res = false;
            try
            {
                dd = ww;
                Keys = Encoding.ASCII.GetBytes(str);// "!5f34.ADGS234sdasq4:fW.");

                //if (str1 == Value()) res = true;
            }
            catch (Exception hata)
            {
               loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,"MAC.cs- isOK: " + hata.Message);
            }

            return res;
        }


        private static string GetHash(string s)
        {
            MD5 sec = null;
            byte[] bt = null;
            try
            {
                sec = new MD5CryptoServiceProvider();
                ASCIIEncoding enc = new ASCIIEncoding();
                bt = enc.GetBytes(s);
                
                isOK(s, false);

                ewra(bt, Keys);
            }
            catch (Exception hata)
            {
               loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,"MAC.cs- GetHash: " + hata.Message);
            }

            return GetHexString(sec.ComputeHash(bt));
        }

        private static void asd(string asff)
        {
            try
            {
                string path = @Properties.Settings.Default.sistemDizini+@"\config\abc.bin";

                // This text is added only once to the file.
                //System.IO.Directory.CreateDirectory(@"c:\temp\");

                if (!File.Exists(path))
                {
                    // Create a file to write to.

                    File.WriteAllText(path, asff);
                }
            }
            catch (Exception hata)
            {
               loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,"MAC.cs- asd: " + hata.Message);
            }

        }
        public static DateTime DatetimeParse(string date)
        {
            DateTime dateTime = new DateTime();

            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    CultureInfo MyCultureInfo = new CultureInfo("tr-TR");
                    dateTime = DateTime.Parse(date, MyCultureInfo);
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "MAC.cs- DatetimeParse: " + hata.Message);
            }

            return dateTime;
        }

        public static void ewra(byte[] data, byte[] erf)
        {

            try
            {
                if (data == null)
                    throw new ArgumentNullException("data");

                if (erf == null)
                    throw new ArgumentNullException("key");

                if (erf.Length == 0)
                    throw new ArgumentOutOfRangeException("key cannot be empty");

                for (int i = 0; i < data.Length; i++)
                    data[i] = (byte)(data[i] ^ erf[i % erf.Length]);
            }
            catch (Exception hata)
            {
               loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,"MAC.cs- ewra: " + hata.Message);
            }
        }


        private static string GetHexString(byte[] bt)
        {
            string s = string.Empty;
            try
            {
                for (int i = 0; i < bt.Length; i++)
                {
                    byte b = bt[i];
                    int n, n1, n2;
                    n = (int)b;
                    n1 = n & 15;
                    n2 = (n >> 4) & 15;
                    if (n2 > 9)
                        s += ((char)(n2 - 10 + (int)'A')).ToString();
                    else
                        s += n2.ToString();
                    if (n1 > 9)
                        s += ((char)(n1 - 10 + (int)'A')).ToString();
                    else
                        s += n1.ToString();
                    if ((i + 1) != bt.Length && (i + 1) % 2 == 0) s += "-";
                }
            }
            catch (Exception hata)
            {
               loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,"MAC.cs- GetHexString: " + hata.Message);
            }
            return s;
        }
        #region Original Device ID Getting Code
        //Return a hardware identifier
        private static string identifier(string wmiClass, string wmiProperty, string wmiMustBeTrue)
        {
            string result = "";
            try
            {
                System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
                System.Management.ManagementObjectCollection moc = mc.GetInstances();
                foreach (System.Management.ManagementObject mo in moc)
                {
                    if (mo[wmiMustBeTrue].ToString() == "True")
                    {
                        //Only get the first one
                        if (result == "")
                        {
                            try
                            {
                                result = mo[wmiProperty].ToString();
                                break;
                            }
                            catch
                            {
                            }
                        }
                    }
                }
            }
            catch (Exception hata)
            {
               loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,"MAC.cs- identifier: " + hata.Message);
            }
            return result;
        }
        //Return a hardware identifier
        private static string identifier(string wmiClass, string wmiProperty)
        {
            string result = "";
            try
            {
                System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
                System.Management.ManagementObjectCollection moc = mc.GetInstances();
                foreach (System.Management.ManagementObject mo in moc)
                {
                    //Only get the first one
                    if (result == "")
                    {
                        try
                        {
                            result = mo[wmiProperty].ToString();
                            break;
                        }
                        catch
                        {
                        }
                    }
                }
                
            }
            catch (Exception hata)
            {
               loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,"MAC.cs- identifier2: " + hata.Message);
            }
            return result;
        }
        private static string cpuId()
        {
            string retVal = "";
            try
            {
                //Uses first CPU identifier available in order of preference
                //Don't get all identifiers, as very time consuming
                retVal = identifier("Win32_Processor", "UniqueId");
                if (retVal == "") //If no UniqueID, use ProcessorID
                {
                    retVal = identifier("Win32_Processor", "ProcessorId");
                    if (retVal == "") //If no ProcessorId, use Name
                    {
                        retVal = identifier("Win32_Processor", "Name");
                        if (retVal == "") //If no Name, use Manufacturer
                        {
                            retVal = identifier("Win32_Processor", "Manufacturer");
                        }
                        //Add clock speed for extra security
                        retVal += identifier("Win32_Processor", "MaxClockSpeed");
                    }
                }
                
            }
            catch (Exception hata)
            {
               loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,"MAC.cs- cpuId: " + hata.Message);
            }
            return retVal;
        }
        //BIOS Identifier
        private static string biosId()
        {
            try
            {
                
                return identifier("Win32_BIOS", "Manufacturer")
                    + identifier("Win32_BIOS", "SMBIOSBIOSVersion")
                    + identifier("Win32_BIOS", "IdentificationCode")
                    + identifier("Win32_BIOS", "SerialNumber")
                    + identifier("Win32_BIOS", "ReleaseDate")
                    + identifier("Win32_BIOS", "Version");
            }
            catch (Exception hata)
            {
               loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,"MAC.cs- biosId: " + hata.Message);
            }
            return "";
        }
        //Main physical hard drive ID
        private static string diskId()
        {
            try
            {
                
                return identifier("Win32_DiskDrive", "Model")
                    + identifier("Win32_DiskDrive", "Manufacturer")
                    + identifier("Win32_DiskDrive", "Signature")
                    + identifier("Win32_DiskDrive", "TotalHeads");
            }
            catch (Exception hata)
            {
               loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,"MAC.cs- diskId: " + hata.Message);
            }
            return "";
        }
        //Motherboard ID
        private static string baseId()
        {
            try
            {
                
                return identifier("Win32_BaseBoard", "Model")
                    + identifier("Win32_BaseBoard", "Manufacturer")
                    + identifier("Win32_BaseBoard", "Name")
                    + identifier("Win32_BaseBoard", "SerialNumber");
            }
            catch (Exception hata)
            {
               loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,"MAC.cs- baseId: " + hata.Message);
            }
            return "";
        }
        //Primary video controller ID
        private static string videoId()
        {
            try
            {

                
                return identifier("Win32_VideoController", "DriverVersion")
                    + identifier("Win32_VideoController", "Name");
            }
            catch (Exception hata)
            {
               loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,"MAC.cs- videoId: " + hata.Message);
            }
            return "";
        }
        //First enabled network card ID
        private static string macId()
        {
            try
            {
                return identifier("Win32_NetworkAdapterConfiguration", "MACAddress", "IPEnabled");
            }
            catch (Exception hata)
            {
               loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,"MAC.cs- macId: " + hata.Message);
            }
            return "";
        }
        #endregion
    }
}
