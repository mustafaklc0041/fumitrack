﻿using Fumitrack.Language;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fumitrack.Classes
{
    class DatabaseCommands
    {
        public static string ConnectionLocal
        {
            get
            {
                return Fumitrack.Properties.Settings.Default.LocalConnectionString;
            }
            set
            {
                Fumitrack.Properties.Settings.Default.LocalConnectionString = value;
            }
        }

        public static string ConnectionServer= Fumitrack.Properties.Settings.Default.FumitrackConnectionString;
       
        public static bool CheckLogin(string username, string passwd, string mac)
        {
            bool check = false;

            try
            {
                string sql = "Select MacID from Users where KullaniciAdi='" + username + "' and Sifre='"+passwd+"' ";
                DataTable dt = DataAccess.GetDataTable(sql, ConnectionServer);
                if (dt.Rows.Count > 0)
                {
                    string macID = dt.Rows[0][0].ToString();
                    if(string.IsNullOrEmpty(macID))
                    {
                        string sqlMac = " update Users set MacID= '" + mac + "' where KullaniciAdi='" + username + "'";
                        int durum = DataAccess.ExecuteSQL(sqlMac, ConnectionServer);
                        if (durum > 0)
                        {
                            check = true;
                        }
                    }
                    else
                    {
                        if (macID == mac)
                            check = true;
                        else
                            check = false;
                    }
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "CheckLogin: " + hata.Message);
            }
            return check;
        }

        public static int GetLoginUserYetki(string username)
        {
            int yetki = 0;

            try
            {
                string sql = "Select Yetki from Users where KullaniciAdi='" + username + "' ";
                DataTable dt = DataAccess.GetDataTable(sql, ConnectionServer);
                if (dt.Rows.Count > 0)
                {
                    yetki = Convert.ToInt16(dt.Rows[0][0].ToString());
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetLoginUserYetki: " + hata.Message);
            }
            return yetki;
        }

        public static bool InsertNewLogin(string firmaAdi, string telefon, string eposta, string sehir, 
            string adres, string aciklama, string username, string passwd, string Olusturan)
        {
            bool check = false;

            try
            {
                string sql = " insert into Users (KullaniciAdi,Sifre,FirmaAdi,Telefon,Eposta,Sehir,Adres,Aciklama,Olusturan) " +
                                     " values ('" + username + "', '" + passwd + "', '" + firmaAdi + "', '" + telefon + "'," +
                                     "          '" + eposta + "', '" + sehir + "', '" + adres + "', '" + aciklama + "','"+ Olusturan + "')";
                int durum= DataAccess.ExecuteSQL(sql, ConnectionServer);
                if (durum > 0)
                {
                    check = true;
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "InsertNewLogin: " + hata.Message);

            }
            return check;
        }

        public static bool UpdateLoginUser(string ID, string firmaAdi, string telefon, string eposta, string sehir, string adres, string aciklama, string username, string passwd)
        {
            bool check = false;

            try
            {
                string sql = " update Users set KullaniciAdi='" + username + "',Sifre='" + passwd + "',FirmaAdi='" + firmaAdi + "'" +
                    ",Telefon='" + telefon + "',Eposta= '" + eposta + "',Sehir='" + sehir + "',Adres='" + adres + "',Aciklama='" + aciklama + "'" +
                    "where ID='" + ID + "' ";
                int durum = DataAccess.ExecuteSQL(sql, ConnectionServer);
                if (durum > 0)
                {
                    check = true;
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "UpdateLoginUser: " + hata.Message);
            }
            return check;
        }

        public static bool DeleteLoginUser(string ID)
        {
            bool check = false;

            try
            {
                string sql = "Delete from Users where ID='"+ID+"'";
                int durum = DataAccess.ExecuteSQL(sql, ConnectionServer);
                if (durum > 0)
                {
                    check = true;
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "DeleteLoginUser: " + hata.Message);
            }
            return check;
        }

        public static string MD5Sifrele(string metin)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

                byte[] dizi = Encoding.UTF8.GetBytes(metin);
                dizi = md5.ComputeHash(dizi);

                foreach (byte ba in dizi)
                {
                    sb.Append(ba.ToString("x2").ToLower());
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "MD5Sifrele: " + hata.Message);

            }
            return sb.ToString();
        }

        public static DataTable GetAllLoginUsers()
        {
            DataTable users = null;
            
            try
            {
                string sql = "Select * from Users ";
                users = DataAccess.GetDataTable(sql, ConnectionServer);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetAllLoginUsers: " + hata.Message);
            }
            return users;
        }

        public static DataTable GetAllLoginUsersFromID(string ID)
        {
            DataTable users = null;

            try
            {
                string sql = "Select * from Users where ID='"+ID+"' ";
                users = DataAccess.GetDataTable(sql, ConnectionServer);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetAllLoginUsersFromID: " + hata.Message);
            }
            return users;
        }

        public static int CreateDatabase(string databaseName)
        {
            int check = 0;

            try
            {
                string sql = " IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'" + databaseName + "') " +
                       " CREATE DATABASE   " + databaseName;
                check = DataAccess.ExecuteSQL(sql, ConnectionLocal);

                if (check!=0)
                {
                    try
                    {
                        if (!ConnectionLocal.Contains("Initial Catalog"))
                        {
                            string sqlConnectionString = "";
                            try
                            {
                                sqlConnectionString = ConnectionLocal.Split(';')[0] + ";" + "Initial Catalog=" + databaseName + ";"
                                                        + ConnectionLocal.Split(';')[1] + ";" + ConnectionLocal.Split(';')[2];
                            }
                            catch
                            {
                                sqlConnectionString = ConnectionLocal.Split(';')[0] + ";" + "Initial Catalog=" + databaseName + ";"
                                                        + ConnectionLocal.Split(';')[1];
                            }
                            ConnectionLocal = sqlConnectionString;
                            Properties.Settings.Default.LocalConnectionString = ConnectionLocal;
                            Properties.Settings.Default.Save();
                            Properties.Settings.Default.Upgrade();
                            Properties.Settings.Default.Reload();
                        }

                        string script = File.ReadAllText(Application.StartupPath + @"\Resources\Fumitrack.sql");
                        string[] ScriptSplitter = script.Split(new string[] { "GO" }, StringSplitOptions.None);
                        foreach (string str in ScriptSplitter)
                        {
                            System.Threading.Thread.Sleep(300);
                            try
                            {
                                if (str.Contains("Fumitrack"))
                                {
                                    string aa = str;
                                    aa = aa.Replace("Fumitrack", databaseName);
                                    DataAccess.ExecuteSQL(aa, ConnectionLocal);
                                }
                                else
                                {
                                    DataAccess.ExecuteSQL(str, ConnectionLocal);
                                }
                            }
                            catch
                            {
                            }
                        }
                    }
                    catch (Exception hata)
                    {
                        loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "CreateDatabase2: " + hata.Message);
                    }
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "CreateDatabase: " + hata.Message+ "  "+ConnectionLocal);
            }
            return check;
        }


        public static int InsertUygulamaYapilanFirmalar(string firmaAdi, string telefon, string eposta, string sehir, string adres, string aciklama)
        {
            int FirmaID = 0;
            
            try
            {
                string sql = " insert into UygulamaYapilanFirmalar (FirmaAdi,Telefon,Eposta,Sehir,Adres,Aciklama) " +
                                     " values ('" + firmaAdi + "', '" + telefon + "'," +
                                     "          '" + eposta + "', '" + sehir + "', '" + adres + "', '" + aciklama + "')" +
                                     "SELECT SCOPE_IDENTITY()";
                string ID = DataAccess.ExecuteScalarSQL(sql, ConnectionLocal);
                FirmaID = Convert.ToInt32(ID);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "InsertUygulamaYapilanFirmalar: " + hata.Message);

            }
            return FirmaID;
        }

        public static int UpdateUygulamaYapilanFirmalar(int ID, string firmaAdi, string telefon, string eposta, string sehir, string adres, string aciklama)
        {
            int check = 0;

            try
            {
                string sql = " Update UygulamaYapilanFirmalar set FirmaAdi='" + firmaAdi + "',Telefon='" + telefon + "'," +
                    "Eposta='" + eposta + "',Sehir='" + sehir + "',Adres='" + adres + "',Aciklama='" + aciklama + "' " +
                    "where ID="+ID+"";
                check = DataAccess.ExecuteSQL(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "UpdateUygulamaYapilanFirmalar: " + hata.Message);

            }
            return check;
        }

        public static DataTable GetUygulamaYapilanFirmalar(string FirmaAdi)
        {
            DataTable Firmalar = null;
            string sql = "";

            try
            {
                if (string.IsNullOrEmpty(FirmaAdi))
                    sql = " select * from UygulamaYapilanFirmalar";
                else
                    sql = " select * from UygulamaYapilanFirmalar where FirmaAdi='"+ FirmaAdi + "'";

                Firmalar = DataAccess.GetDataTable(sql, ConnectionLocal);

                if (string.IsNullOrEmpty(FirmaAdi))
                {
                    DataRow emptyRow = Firmalar.NewRow();
                    emptyRow["ID"] = 0;
                    emptyRow["FirmaAdi"] = "Yeni Firma (New Company)";
                    Firmalar.Rows.Add(emptyRow);
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetUygulamaYapilanFirmalar: " + hata.Message);

            }
            return Firmalar;
        }

        public static DataTable GetGecmisUygulamalar()
        {
            DataTable GecmisUygulamalar = null;
            string sql = "";

            try
            {
                sql = "select UYF.FirmaAdi, DP.IlaclamaTarih, DP.ID from DozajPlan DP " +
                    "inner join UygulamaYapilanFirmalar UYF on DP.FirmaID=UYF.ID";

                GecmisUygulamalar = DataAccess.GetDataTable(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetGecmisUygulamalar: " + hata.Message);

            }
            return GecmisUygulamalar;
        }

        public static int GetDozajPlanID(int DozajPlanID)
        {
            int ID = 0;
            string sql = "";

            try
            {
                sql = " select ID from DozajPlan where ID='" + DozajPlanID + "'";

                DataTable dt= DataAccess.GetDataTable(sql, ConnectionLocal);
                if(dt.Rows.Count>0)
                ID = Convert.ToInt32(dt.Rows[0][0].ToString());
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetDozajPlanID: " + hata.Message);

            }
            return ID;
        }

        public static int DeleteGecmisUyglByID(string DozajPlanID)
        {
            int check=0;
            string sql = "";

            try
            {
                sql = "Delete from DozajPlan where ID='" + DozajPlanID + "'";

                check= DataAccess.ExecuteSQL(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetDozajPlanID: " + hata.Message);

            }
            return check;
        }



            public static int UpdateDozajPlanAclsKapGunandSaat(string AclsTarih, string AclsSaat, string KapTarih, string KapSaat, int DozajPlanID)
        {
            int check = 0;

            try
            {
                string sql = " Update DozajPlan set AcilisTarih='"+ AclsTarih + "', AcilisSaat='"+ AclsSaat + "', " +
                    "KapanisTarih='"+ KapTarih + "', KapanisSaat='"+ KapSaat + "' " +
                    "where ID=" + DozajPlanID + "";
                check = DataAccess.ExecuteSQL(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "UpdateDozajPlanAclsKapGunandSaat: " + hata.Message);

            }
            return check;
        }

        public static int UpdateDozajPlan(int ID,int FirmaID, string IlaclamaTarih, string FumiLisans1
            , string FumiAdSoyad1, string FumiLisans2, string FumiAdSoyad2, string YapildigiYer, string KayitNo, string UrunCinsi
            , string UrunMiktari, string KontrolNo, string AmbalajAdedi, string AmbalajSekli, string NeMaksatlaY, string KonteynerNo
            , string HedefZararli, string Dozaj, string Emtia, string FumiSekli, string BasincTip)
        {
            int check = 0;

            try
            {
                string sql = " Update DozajPlan set FirmaID=" + FirmaID + ",IlaclamaTarih='" + IlaclamaTarih + "'," +
                    "FumiLisans1='" + FumiLisans1 + "',FumiAdSoyad1='" + FumiAdSoyad1 + "',FumiLisans2='" + FumiLisans2 + "'," +
                    "FumiAdSoyad2='" + FumiAdSoyad2 + "',YapildigiYer='" + YapildigiYer + "',KayitNo='" + KayitNo + "'," +
                    "UrunCinsi='" + UrunCinsi + "',UrunMiktari='" + UrunMiktari + "',KontrolNo='" + KontrolNo + "'," +
                    "AmbalajAdedi='" + AmbalajAdedi + "',AmbalajSekli='" + AmbalajSekli + "',NeMaksatlaY='" + NeMaksatlaY + "'," +
                    "KonteynerNo='" + KonteynerNo + "',HedefZararli='" + HedefZararli + "',Dozaj='" + Dozaj + "',Emtia='" + Emtia + "'," +
                    "FumiSekli='"+ FumiSekli + "',BasincTip='"+ BasincTip + "' " +
                    "where ID=" + ID + "";
                check = DataAccess.ExecuteSQL(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "UpdateDozajPlan: " + hata.Message);

            }
            return check;
        }

        public static int InsertDozajPlan(int FirmaID, string IlaclamaTarih, string FumiLisans1
            , string FumiAdSoyad1, string FumiLisans2, string FumiAdSoyad2, string YapildigiYer, string KayitNo, string UrunCinsi
            , string UrunMiktari, string KontrolNo, string AmbalajAdedi, string AmbalajSekli, string NeMaksatlaY, string KonteynerNo, 
            string HedefZararli, string Dozaj, string Emtia, string FumiSekli, string BasincTip)
        {
            int DozajPlanID = 0;

            try
            {
                string sql = " insert into DozajPlan (FirmaID, IlaclamaTarih, FumiLisans1, " +
                    "FumiAdSoyad1, FumiLisans2, FumiAdSoyad2, YapildigiYer, KayitNo, UrunCinsi, " +
                    "UrunMiktari, KontrolNo, AmbalajAdedi, AmbalajSekli, NeMaksatlaY, KonteynerNo, " +
                    "HedefZararli, Dozaj, Emtia, FumiSekli, BasincTip) " +
                                     " values ('" + FirmaID + "', '" + IlaclamaTarih + "', '" + FumiLisans1 + "', " +
                                     "'" + FumiAdSoyad1 + "','" + FumiLisans2 + "','" + FumiAdSoyad2 + "','" + YapildigiYer + "'," +
                                     "'" + KayitNo + "','" + UrunCinsi + "','" + UrunMiktari + "','" + KontrolNo + "'," +
                                     "'" + AmbalajAdedi + "','" + AmbalajSekli + "','" + NeMaksatlaY + "','" + KonteynerNo + "'," +
                                     "'" + HedefZararli + "', '" + Dozaj + "', '" + Emtia + "', '"+ FumiSekli + "', '"+ BasincTip + "')" +
                                     "SELECT SCOPE_IDENTITY()";
                string ID = DataAccess.ExecuteScalarSQL(sql, ConnectionLocal);
                DozajPlanID = Convert.ToInt32(ID);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "InsertDozajPlan: " + hata.Message);

            }
            return DozajPlanID;
        }

        public static DataTable GetAlanBilgileriAll(int DozajPlanID)
        {
            DataTable AlanBilgileri = null;
            string sql = "";

            try
            {
                sql = " select ID,AlanAdi from AlanBilgileri where DozajPlanID="+ DozajPlanID + "";

                AlanBilgileri = DataAccess.GetDataTable(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetAlanBilgileriAll: " + hata.Message);

            }
            return AlanBilgileri;
        }

        public static DataTable GetGecmisUygulamalarAlldata(int DozPlanID)
        {
            DataTable GecmisUygulamalarAlldata = null;
            string sql = "";

            try
            {
                sql = "Select * from DozajPlan DP " +
                    "inner join AlanBilgileri AB on AB.DozajPlanID = DP.ID " +
                    "inner join UygulamaYapilanFirmalar UYF on DP.FirmaID = UYF.ID" +
                    " inner join IzlemeVeriGirdileri IVG on IVG.DozajPlanID = DP.ID and IVG.AlanBilgileriID = AB.ID" +
                    " where DP.ID = '" + DozPlanID + "'";

                GecmisUygulamalarAlldata = DataAccess.GetDataTable(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetGecmisUygulamalarAlldata: " + hata.Message);

            }
            return GecmisUygulamalarAlldata;
        }

        public static int GetAlanBilgileriID(int AlanBilgileriID)
        {
            int ID = 0;
            string sql = "";

            try
            {
                sql = " select MaruzKalmaSuresi from AlanBilgileri where ID='" + AlanBilgileriID + "'";

                DataTable dt = DataAccess.GetDataTable(sql, ConnectionLocal);
                ID = Convert.ToInt32(dt.Rows[0][0].ToString());
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetAlanBilgileriID: " + hata.Message);

            }
            return ID;
        }

        public static int UpdateAlanBilgileri(int ID, int DozajPlanID, string AlanAdi, string Sicaklik, decimal YarilamaSuresi, 
            decimal MaruzKalmaSuresi, decimal AlanHacmi, decimal FumiMiktar, decimal HedefCT, decimal SilindirSayisi)
        {
            int check = 0;

            try
            {
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
                cmd.Connection = DataAccess.Connection(ConnectionLocal);
                cmd.CommandText = "Update AlanBilgileri set DozajPlanID=" + DozajPlanID + ",AlanAdi='" + AlanAdi + "'," +
                    "Sicaklik='" + Sicaklik + "',YarilamaSuresi=@YarilamaSuresi, MaruzKalmaSuresi='" + MaruzKalmaSuresi + "',AlanHacmi=@AlanHacmi," +
                    "FumiMiktar=@FumiMiktar, HedefCT='" + HedefCT + "', SilindirSayisi=@SilindirSayisi where ID=" + ID + "";

                cmd.Parameters.Add("@YarilamaSuresi", SqlDbType.Decimal).Value = YarilamaSuresi;
                cmd.Parameters.Add("@AlanHacmi", SqlDbType.Decimal).Value = AlanHacmi;
                cmd.Parameters.Add("@FumiMiktar", SqlDbType.Decimal).Value = FumiMiktar;
                cmd.Parameters.Add("@SilindirSayisi", SqlDbType.Decimal).Value = SilindirSayisi;
                check = cmd.ExecuteNonQuery();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "UpdateAlanBilgileri: " + hata.Message);

            }
            return check;
        }

        public static int InsertAlanBilgileri(int DozajPlanID, string AlanAdi, string Sicaklik, decimal YarilamaSuresi, 
            decimal MaruzKalmaSuresi, decimal AlanHacmi, decimal FumiMiktar, decimal HedefCT, decimal SilindirSayisi)
        {
            int AlanBilgileriID = 0;

            try
            {
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
                cmd.Connection = DataAccess.Connection(ConnectionLocal);
                cmd.CommandText = "insert into AlanBilgileri (DozajPlanID, AlanAdi, Sicaklik, YarilamaSuresi, MaruzKalmaSuresi, " +
                    "AlanHacmi, FumiMiktar, HedefCT, SilindirSayisi) " +
                                     " values ('" + DozajPlanID + "', '" + AlanAdi + "', '" + Sicaklik + "', " +
                                     "@YarilamaSuresi, '" + MaruzKalmaSuresi + "', @AlanHacmi, @FumiMiktar, '" + HedefCT + "', @SilindirSayisi)" +
                                     "SELECT SCOPE_IDENTITY()";

                cmd.Parameters.Add("@YarilamaSuresi", SqlDbType.Decimal).Value = YarilamaSuresi;
                cmd.Parameters.Add("@AlanHacmi", SqlDbType.Decimal).Value = AlanHacmi;
                cmd.Parameters.Add("@FumiMiktar", SqlDbType.Decimal).Value = FumiMiktar;
                cmd.Parameters.Add("@SilindirSayisi", SqlDbType.Decimal).Value = SilindirSayisi;
                string ID = cmd.ExecuteScalar().ToString();
                AlanBilgileriID = Convert.ToInt32(ID);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "InsertAlanBilgileri: " + hata.Message);

            }
            return AlanBilgileriID;
        }

        public static DataTable GetUygulamaPlanbyAlanID(int AlanBilgID)
        {
            DataTable table = new DataTable();
            string sql = "";

            try
            {
                sql = "SELECT AB.AlanAdi,GirisHatti,MalzemeCinsi,SisCapi,Uzunluk,FanKapasitesi,FanSayisi,UP.SilindirSayisi" +
                    " FROM UygulamaPlan UP " +
                    "inner join AlanBilgileri AB on AB.ID = UP.AlanBilgileriID " +
                    "where UP.AlanBilgileriID = '" + AlanBilgID + "'";

                table = DataAccess.GetDataTable(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetUygulamaPlanbyAlanID: " + hata.Message);

            }
            return table;
        }

        public static int UpdateUygulamaPlan(int ID, int AlanBilgileriID, string GirisHatti, string MalzemeCinsi, string SisCapi, string Uzunluk, string FanKapasitesi, string FanSayisi, string SilindirSayisi)
        {
            int check = 0;

            try
            {
                string sql = " Update UygulamaPlan set AlanBilgileriID=" + AlanBilgileriID + ",GirisHatti='" + GirisHatti + "'," +
                    "MalzemeCinsi='" + MalzemeCinsi + "',SisCapi='" + SisCapi + "',Uzunluk='" + Uzunluk + "',FanKapasitesi='" + FanKapasitesi + "'," +
                    "FanSayisi='" + FanSayisi + "',SilindirSayisi='" + SilindirSayisi + "' " +
                    "where ID=" + ID + "";
                check = DataAccess.ExecuteSQL(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "UpdateUygulamaPlan: " + hata.Message);

            }
            return check;
        }

        public static int InsertUygulamaPlan(int AlanBilgileriID, string GirisHatti, string MalzemeCinsi, string SisCapi, string Uzunluk, string FanKapasitesi, string FanSayisi, string SilindirSayisi)
        {
            int UygulamaPlanID = 0;

            try
            {
                string sql = " insert into UygulamaPlan (AlanBilgileriID, GirisHatti, MalzemeCinsi, SisCapi, Uzunluk, FanKapasitesi, FanSayisi, SilindirSayisi) " +
                                     " values ('" + AlanBilgileriID + "', '" + GirisHatti + "', '" + MalzemeCinsi + "', " +
                                     "'" + SisCapi + "', '" + Uzunluk + "', '" + FanKapasitesi + "', '" + FanSayisi + "', '" + SilindirSayisi + "')" +
                                     "SELECT SCOPE_IDENTITY()";
                string ID = DataAccess.ExecuteScalarSQL(sql, ConnectionLocal);
                UygulamaPlanID = Convert.ToInt32(ID);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "InsertUygulamaPlan: " + hata.Message);

            }
            return UygulamaPlanID;
        }

        public static DataTable GetIzlemePlanbyID(int AlanBilgID)
        {
            DataTable table=new DataTable();
            string sql = "";

            try
            {
                sql = " SELECT AB.AlanAdi,IzlemeNoktasiAdi,OlcumNoktasi,HortumMesafesi " +
                    "FROM IzlemePlan IZP " +
                    "inner join AlanBilgileri AB on AB.ID = IZP.AlanBilgileriID " +
                    "where IZP.AlanBilgileriID = '"+ AlanBilgID + "'";

                table = DataAccess.GetDataTable(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetIzlemePlanbyID: " + hata.Message);

            }
            return table;
        }

        public static int UpdateIzlemePlan(int ID, int AlanBilgileriID, string IzlemeNoktasiAdi, string OlcumNoktasi, string HortumMesafesi)
        {
            int check = 0;

            try
            {
                string sql = " Update IzlemePlan set AlanBilgileriID=" + AlanBilgileriID + ",IzlemeNoktasiAdi='" + IzlemeNoktasiAdi + "'," +
                    "OlcumNoktasi='" + OlcumNoktasi + "',HortumMesafesi='" + HortumMesafesi + "'"+
                    "where ID=" + ID + "";
                check = DataAccess.ExecuteSQL(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "UpdateIzlemePlan: " + hata.Message);

            }
            return check;
        }

        public static int InsertIzlemePlan(int AlanBilgileriID, string IzlemeNoktasiAdi, string OlcumNoktasi, string HortumMesafesi)
        {
            int IzlemePlanID = 0;

            try
            {
                string sql = " insert into IzlemePlan (AlanBilgileriID, IzlemeNoktasiAdi, OlcumNoktasi, HortumMesafesi) " +
                                     " values ('" + AlanBilgileriID + "', '" + IzlemeNoktasiAdi + "', '" + OlcumNoktasi + "', " +
                                     "'" + HortumMesafesi + "')" +
                                     "SELECT SCOPE_IDENTITY()";
                string ID = DataAccess.ExecuteScalarSQL(sql, ConnectionLocal);
                IzlemePlanID = Convert.ToInt32(ID);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "InsertIzlemePlan: " + hata.Message);

            }
            return IzlemePlanID;
        }

        public static DataTable GetIzlemePlanbyAlanID(int AlanBilgileriID)
        {
            DataTable dt = null;
            string sql = "";

            try
            {
                sql = " select IzlemeNoktasiAdi,ID from IzlemePlan where AlanBilgileriID='" + AlanBilgileriID + "'";

                dt = DataAccess.GetDataTable(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetIzlemePlanbyAlanID: " + hata.Message);

            }
            return dt;
        }

        public static DataTable GetMaruzKalmaSuresiandAlanHacmi(int AlanBilgileriID)
        {
            DataTable dt = new DataTable();
            string sql = "";

            try
            {
                sql = " select MaruzKalmaSuresi,AlanHacmi from AlanBilgileri where ID='" + AlanBilgileriID + "'";

                dt = DataAccess.GetDataTable(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetMaruzKalmaSuresiandAlanHacmi: " + hata.Message);

            }
            return dt;
        }

        public static int InsertIzlemeVeriGirdi(int DozajPlanID, int AlanBilgileriID, string Tarih, string Saat, string Sicaklik, int IzlemeNokBilgID
            , decimal OrtKonsantrasyon, string HatOrtKonsantrasyon, decimal HLT, string Aciklama)
        {
            int IzlemeVeriGirdiID = 0;

            try
            {
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
                cmd.Connection = DataAccess.Connection(ConnectionLocal);
                cmd.CommandText = "insert into IzlemeVeriGirdileri (DozajPlanID, AlanBilgileriID, Tarih, Saat, Sicaklik, IzlemeNokBilgID," +
                    "OrtKonsantrasyon, HatOrtKonsantrasyon, HLT, Aciklama) " +
                                     " values ('" + DozajPlanID + "', '" + AlanBilgileriID + "', '" + Tarih + "', " +
                                     "'" + Saat + "', '"+ Sicaklik + "', '" + IzlemeNokBilgID + "', @OrtKonsantrasyon, @HatOrtKonsantrasyon," +
                                     " @HLT, '" + Aciklama + "')" +
                                     "SELECT SCOPE_IDENTITY()";

                cmd.Parameters.Add("@OrtKonsantrasyon", SqlDbType.Decimal).Value = OrtKonsantrasyon;
                cmd.Parameters.Add("@HatOrtKonsantrasyon", SqlDbType.NVarChar).Value = HatOrtKonsantrasyon;
                cmd.Parameters.Add("@HLT", SqlDbType.Decimal).Value = HLT;
                string ID = cmd.ExecuteScalar().ToString();
                IzlemeVeriGirdiID = Convert.ToInt32(ID);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "InsertIzlemeVeriGirdi: " + hata.Message);

            }
            return IzlemeVeriGirdiID;
        }

        
        public static int UpdateIzlemeVeriGirdi(int ID, int DozajPlanID, int AlanBilgileriID, string Tarih, string Saat, string Sicaklik, int IzlemeNokBilgID
            , decimal OrtKonsantrasyon, string HatOrtKonsantrasyon, decimal HLT, string Aciklama)
        {
            int IzlemeVeriGirdiID = 0;

            try
            {
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
                cmd.Connection = DataAccess.Connection(ConnectionLocal);
                cmd.CommandText = "Update IzlemeVeriGirdileri set DozajPlanID='" + DozajPlanID + "', AlanBilgileriID='" + AlanBilgileriID + "'" +
                    ", Tarih='" + Tarih + "', Saat='" + Saat + "', Sicaklik='"+ Sicaklik+ "', IzlemeNokBilgID='" + IzlemeNokBilgID + "'" +
                    ", OrtKonsantrasyon=@OrtKonsantrasyon, HatOrtKonsantrasyon=@HatOrtKonsantrasyon, HLT=@HLT" +
                    ", Aciklama='" + Aciklama + "'" +
                    "where ID='" + ID + "'";

                cmd.Parameters.Add("@OrtKonsantrasyon", SqlDbType.Decimal).Value = OrtKonsantrasyon;
                cmd.Parameters.Add("@HatOrtKonsantrasyon", SqlDbType.NVarChar).Value = HatOrtKonsantrasyon;
                cmd.Parameters.Add("@HLT", SqlDbType.Decimal).Value = HLT;
                IzlemeVeriGirdiID = cmd.ExecuteNonQuery();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "UpdateIzlemeVeriGirdi: " + hata.Message);

            }
            return IzlemeVeriGirdiID;
        }

        public static DataTable GetIzlemeVeriGirdileri(int DozajPlanID, int AlanBilgID)
        {
            DataTable dt = new DataTable();
            string sql = "";

            try
            {
                sql = "select AB.AlanAdi,IVG.Tarih,IVG.Saat,IVG.Sicaklik,IzP.IzlemeNoktasiAdi,IVG.IzlemeNokBilgID,IVG.OrtKonsantrasyon, IVG.HatOrtKonsantrasyon" +
                    ", IVG.HLT,IVG.Aciklama,IVG.ID " +
                    "from IzlemeVeriGirdileri IVG " +
                    "inner join IzlemePlan IzP on IVG.IzlemeNokBilgID = IzP.ID " +
                    "inner join AlanBilgileri AB on IVG.AlanBilgileriID = AB.ID " +
                    "where IVG.DozajPlanID = '"+ DozajPlanID + "' and IVG.AlanBilgileriID = '"+ AlanBilgID + "'";

                dt = DataAccess.GetDataTable(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetIzlemeVeriGirdileri: " + hata.Message);

            }
            return dt;
        }

        public static DataTable GetIzlemeVeriGirdiHLTandHatOrtKonsantr(int DozajPlanID, int AlanBilgID)
        {
            DataTable dt = new DataTable();
            string sql = "";

            try
            {
                sql = "select * " +
                    "from IzlemeVeriGirdileri " +
                    "where DozajPlanID = '" + DozajPlanID + "' and AlanBilgileriID = '" + AlanBilgID + "' " +
                    "";

                dt = DataAccess.GetDataTable(sql, ConnectionLocal);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GetIzlemeVeriGirdiHLTandHatOrtKonsantr: " + hata.Message);

            }
            return dt;
        }

        public static void DataGridColor(DataGridView datagrid)
        {
            try
            {
                datagrid.ColumnHeadersDefaultCellStyle.BackColor = ColorTranslator.FromHtml(Properties.Settings.Default["DataGridColor"].ToString());
                datagrid.ColumnHeadersDefaultCellStyle.Font = new Font(datagrid.Font, FontStyle.Bold);
                datagrid.DefaultCellStyle.ForeColor = Color.Black;
                datagrid.AlternatingRowsDefaultCellStyle.BackColor = Color.Khaki;
                datagrid.EnableHeadersVisualStyles = false;
                datagrid.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "DataGridColor: " + hata.Message);

            }
        }

        public static void CopyFumiExcelServertoLocal()
        {
            try
            {
                DirectoryInfo dirInfo = new DirectoryInfo(Properties.Settings.Default.FumitrackExcelPath);
                FileInfo[] fileInfos = dirInfo.GetFiles();
                foreach (FileInfo file in fileInfos)
                {
                    File.Copy(file.FullName, Path.Combine(@Properties.Settings.Default.sistemDizini, file.Name), true);
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "CopyFumiExcelServertoLocal: " + hata.Message);

            }
        }

        public static void DatabaseSync()
        {
            try
            {
                
                string script = Properties.Resources.Sync;//File.ReadAllText(path);

                string[] ScriptSplitter = script.Split(new string[] { "\r\nGO\r\n" }, StringSplitOptions.None);

                foreach (string str in ScriptSplitter)
                {
                    try
                    {
                        DataAccess.ExecuteSQL(str, ConnectionLocal);
                    }
                    catch(Exception e)
                    {
                    }
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "DatabaseSync: " + hata.Message);

            }
        }

        public static DataTable FumiExcelImport()
        {
            DataTable duzgunDT = new DataTable();
            try
            {
                string sheetName = "";
                DataTable merge = new DataTable();
                string conStr = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'", @Application.StartupPath + @"\Report\" + Properties.Settings.Default.FumiExcelName, "YES");
                //string conStr = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'", @Properties.Settings.Default.sistemDizini + @"\" + Properties.Settings.Default.FumiExcelName, "YES");
                using (OleDbConnection con = new OleDbConnection(conStr))
                {
                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        cmd.Connection = con;
                        con.Open();
                        DataTable dtExcelSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        for (int i = 0; i < dtExcelSchema.Rows.Count; i++)
                        {
                            sheetName = dtExcelSchema.Rows[i]["TABLE_NAME"].ToString();
                            using (OleDbDataAdapter oda = new OleDbDataAdapter())
                            {
                                DataTable dt = new DataTable();
                                cmd.CommandText = "SELECT * From [" + sheetName + "]";
                                oda.SelectCommand = cmd;
                                oda.Fill(dt);
                                merge.Merge(dt);
                            }
                        }
                        con.Close();
                    }
                }


                duzgunDT = merge.Clone();

                foreach (DataRow row in merge.Rows)
                {
                    if (!string.IsNullOrEmpty(row.ItemArray[0].ToString()) && row.ItemArray[0].ToString() != "0")
                    {
                        duzgunDT.ImportRow(row);
                    }
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "FumiExcelImport: " + hata.Message);

            }

            return duzgunDT;
        }

        public static bool CheckInternetConnection()
        {
            try
            {
                Ping myPing = new Ping();
                String host = "google.com";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                return (reply.Status == IPStatus.Success);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "CheckInternetConnection: " + hata.Message);
                return false;
            }
        }

        public static Bitmap Print(TabPage page, PrintPreviewDialog printPreviewDialog, System.Drawing.Printing.PrintDocument printDocument)
        {
            Bitmap bitmap = null ;
            try
            {
                var size = page.PreferredSize;
                if (page.Name== "tabPageGraph")
                {
                    size = page.Size;
                }
               
                var height = size.Height;
                bitmap = new Bitmap(page.Width, height);

                //panelAll.DrawToBitmap(bitmap, new Rectangle(0, 0, panelAll.Width, height));
                foreach (Control control in page.Controls)
                {
                    foreach (Control childControl in control.Controls)
                    {
                        if (!childControl.Name.StartsWith("btn") && childControl.Name!= "listBoxGrafkFiltre")
                            control.DrawToBitmap(bitmap, control.Bounds);
                    }
                }
                printPreviewDialog.Document = printDocument;
                printPreviewDialog.PrintPreviewControl.Zoom = 1;
                ((Form)printPreviewDialog).WindowState = FormWindowState.Maximized;
                //printPreviewDialog.ShowDialog();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Print: " + hata.Message);
            }
            return bitmap;
        }

        public static void OpenDocReport(string docName)
        {
            try
            {
                FileInfo fi = new FileInfo(Application.StartupPath + @"\Report\"+ docName);
                if (fi.Exists)
                {
                    System.Diagnostics.Process.Start(Application.StartupPath + @"\Report\" + docName);
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "OpenDocReport: " + hata.Message);
            }
        }
        static string Lng = "";
        public static void SetLanguage(Form frm)
        {
            try
            {
                if (Properties.Settings.Default.Dil == "Tr")
                {
                    Localization.Culture = new System.Globalization.CultureInfo("tr-TR");
                    Lng = "Turkish";
                }
                else
                {
                    Localization.Culture = new System.Globalization.CultureInfo("en-US");
                    Lng = "English";
                }

                string sql = "select FormName, ObjectName,"+ Lng + "  " +
                    "from Language";

                TableLanguage.Clear();
                TableLanguage = DataAccess.GetDataTable(sql, ConnectionLocal);
                
                ChildControls(frm.Controls);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "SetLanguage: " + hata.Message);
            }
        }
        static DataTable TableLanguage = new DataTable();
        public static void ChildControls(Control.ControlCollection controls)
        {
            try
            {
                foreach (Control childControl in controls)
                {
                    try
                    {
                        if (childControl is ComboBox)
                        {

                            ComboBox cmb = (ComboBox)childControl;
                            if (cmb.Name == "cmbDozaj")
                                continue;
                            List<string> LangText2 = new List<string>();
                            LangText2 = TableLanguage.Rows.Cast<DataRow>()
                                       .Where(x => x["FormName"].ToString().ToUpper() == cmb.FindForm().Name.ToUpper())
                                       .Where(x => x["ObjectName"].ToString().ToUpper() == cmb.Name.ToUpper())
                                       .Select(x => x[Lng].ToString())
                                       .ToList();
                            cmb.Items.Clear();
                            foreach (string item in LangText2)
                            {
                                cmb.Items.Add(item);
                            }
                        }
                    }
                    catch { }

                    try
                    {
                        if (childControl is DataGridView)
                        {
                            DataGridView dtg = (DataGridView)childControl;
                            foreach (DataGridViewColumn c in dtg.Columns)
                            {
                                List<string> LangText2 = new List<string>();
                                LangText2 = TableLanguage.Rows.Cast<DataRow>()
                                           .Where(x => x["FormName"].ToString().ToUpper() == dtg.FindForm().Name.ToUpper())
                                           .Where(x => x["ObjectName"].ToString().ToUpper() == c.Name.ToUpper())
                                           .Select(x => x[Lng].ToString())
                                           .ToList();
                                c.HeaderText = LangText2[0];
                            }
                        }
                    }
                    catch { }

                    try
                    {
                        if (childControl is System.Windows.Forms.DataVisualization.Charting.Chart)
                        {

                            System.Windows.Forms.DataVisualization.Charting.Chart chrt = (System.Windows.Forms.DataVisualization.Charting.Chart)childControl;
                            foreach (System.Windows.Forms.DataVisualization.Charting.Series c in chrt.Series)
                            {
                                List<string> LangText2 = new List<string>();
                                LangText2 = TableLanguage.Rows.Cast<DataRow>()
                                           .Where(x => x["FormName"].ToString().ToUpper() == chrt.FindForm().Name.ToUpper())
                                           .Where(x => x["ObjectName"].ToString().ToUpper() == c.Name.ToUpper())
                                           .Select(x => x[Lng].ToString())
                                           .ToList();
                                c.LegendText = LangText2[0];
                            }
                            foreach (System.Windows.Forms.DataVisualization.Charting.ChartArea c in chrt.ChartAreas)
                            {
                                if (Lng == "Turkish")
                                {
                                    c.AxisX.Title = "Zaman";
                                    c.AxisY.Title = "Konsantrasyon";
                                }
                                else
                                {
                                    c.AxisX.Title = "Time";
                                    c.AxisY.Title = "Concentration";
                                }
                            }
                        }
                    }
                    catch { }

                    try
                    {
                        if (childControl is MenuStrip)
                        {
                            MenuStrip mstrp = (MenuStrip)childControl;

                            try
                            {
                                GetMenuItems(mstrp.Items);
                                void GetMenuItems(ToolStripItemCollection menuItems)
                                {
                                    try
                                    {
                                        foreach (ToolStripMenuItem itm in menuItems)
                                        {
                                            List<string> LangText2 = new List<string>();
                                            LangText2 = TableLanguage.Rows.Cast<DataRow>()
                                                       .Where(x => x["FormName"].ToString().ToUpper() == mstrp.FindForm().Name.ToUpper())
                                                       .Where(x => x["ObjectName"].ToString().ToUpper() == itm.Name.ToUpper())
                                                       .Select(x => x[Lng].ToString())
                                                       .ToList();
                                            itm.Text = LangText2[0];

                                            if (itm.HasDropDown)
                                            {
                                                GetMenuItems(itm.DropDownItems);
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        if (!string.IsNullOrEmpty(childControl.Name))
                        {
                            List<string> LangText = new List<string>();
                            LangText = TableLanguage.Rows.Cast<DataRow>()
                                       .Where(x => x["FormName"].ToString().ToUpper() == childControl.FindForm().Name.ToUpper())
                                       .Where(x => x["ObjectName"].ToString().ToUpper() == childControl.Name.ToUpper())
                                       .Select(x => x[Lng].ToString())
                                       .ToList();
                            childControl.Text = LangText[0];
                        }
                    }
                    catch (Exception hata)
                    {
                        loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "ChildControls2: " + hata.Message);
                    }
                    ChildControls(childControl.Controls);
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "ChildControls: " + hata.Message);
            }
        }

    }
}
