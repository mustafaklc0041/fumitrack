﻿
namespace Fumitrack
{
    class loglama
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void error(System.Type form, string ifade)
        {
            log.Error("- " + form + " " + ifade);
        }
    }
}
