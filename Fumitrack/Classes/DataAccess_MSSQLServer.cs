﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Fumitrack.Classes
{
	//////MS-SQL server  Edition
    class DataAccess
    {
        
        //static string _ConnectionString = Fumitrack.Properties.Settings.Default.FumitrackConnectionString;
            //"server=(local); database=DatabaseName;Trusted_Connection=true";


        static SqlConnection _Connection = null;
        public static SqlConnection Connection(string ConnectionString)
        {
            //get
            //{
            if (_Connection!=null &&_Connection.ConnectionString != ConnectionString)
                _Connection = null;

                if (_Connection == null)
                {
                    _Connection = new SqlConnection(ConnectionString);
                    _Connection.Open();

                    return _Connection;
                }
                else if (_Connection.State != System.Data.ConnectionState.Open)
                {
                    _Connection.Open();

                    return _Connection;
                }
                else
                {
                    return _Connection;
                }
            //}
        }

        public static DataSet GetDataSet(string sql, string connection)
        {
            SqlCommand cmd = new SqlCommand(sql, Connection(connection));
            SqlDataAdapter adp = new SqlDataAdapter(cmd);

            DataSet ds = new DataSet();
            adp.Fill(ds);
            Connection(connection).Close();

            return ds;
        }

        public static DataTable GetDataTable(string sql, string connection)
        {
            DataSet ds = GetDataSet(sql, connection);

            if (ds.Tables.Count > 0)
                return ds.Tables[0];
            return null;
        }

        public static int ExecuteSQL(string sql, string connection)
        {
            SqlCommand cmd = new SqlCommand(sql, Connection(connection));
            return cmd.ExecuteNonQuery();
        }
        public static string ExecuteScalarSQL(string sql, string connection)
        {
            SqlCommand cmd = new SqlCommand(sql, Connection(connection));
            return cmd.ExecuteScalar().ToString();
        }
    }

}
