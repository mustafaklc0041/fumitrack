﻿using System;
using System.Reflection;
using Fumitrack.Language;
using MetroFramework.Forms;

namespace Fumitrack.GUI
{
    public partial class AboutForm : MetroForm
    {
        public AboutForm()
        {
            InitializeComponent();

            this.Text = Localization.AboutFormText;
            this.txtUrunAdi.Text = "PROPESTGAS FumiTrack ©";//AssemblyProduct;
            this.txtYazilimVersiyon.Text = String.Format("Version {0}", AssemblyVersion);
        }

        public string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyVersion
        {
            get
            {
                if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
                {
                    Version ver = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion;
                    return string.Format("{0}.{1}.{2}.{3}", ver.Major, ver.Minor, ver.Build, ver.Revision);
                }
                else
                {
                    var ver = Assembly.GetExecutingAssembly().GetName().Version;
                    return string.Format("{0}.{1}.{2}.{3}", ver.Major, ver.Minor, ver.Build, ver.Revision);
                }

            }
        }

        private void AboutForm_Load(object sender, EventArgs e)
        {
            try
            {
                txtAciklama.Text = Localization.About;
            }
            catch 
            {
                
            }
        }
    }
}
