﻿namespace Fumitrack.GUI
{
    partial class FumitrackMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FumitrackMainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, "0,0,0,0");
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, "0,0,0,0");
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, "0,0,0,0");
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint4 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(8D, "0,0,0,0");
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint5 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(12D, "0,0,0,0");
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint6 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(18D, "0,0,0,0");
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint7 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(24D, "0,0,0,0");
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint8 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint9 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint10 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint11 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(8D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint12 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(12D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint13 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(18D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint14 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(24D, 0D);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NewtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReporttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalRaporToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.avustralyaRaporToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.turkceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingilizceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.avustralyaRapor2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.turkce2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingilizce2ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.avustralyaRapor3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.turkce3ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ingilizce3ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.avustralyaRapor4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.turkce4ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ingilizce4ToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.LogoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.türkçeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingilizceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageHistory = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dtgrdGecmisUyglm = new System.Windows.Forms.DataGridView();
            this.FirmaAdi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IlaclamaTarihi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DozajPlanIDGecmis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemRapor = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemVeriGor = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSil = new System.Windows.Forms.ToolStripMenuItem();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tabPageCustomerInfo = new System.Windows.Forms.TabPage();
            this.panel19 = new System.Windows.Forms.Panel();
            this.btnFirmaKydt = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbFirmaAdi = new System.Windows.Forms.ComboBox();
            this.txtTelefon = new System.Windows.Forms.MaskedTextBox();
            this.txtAdres = new System.Windows.Forms.TextBox();
            this.txtAciklama = new System.Windows.Forms.TextBox();
            this.txtEposta = new System.Windows.Forms.TextBox();
            this.txtSehir = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabPageFumigationDosagePlan = new System.Windows.Forms.TabPage();
            this.panelHedefZararli = new System.Windows.Forms.Panel();
            this.btnHdfZararlIptal = new System.Windows.Forms.Button();
            this.btnHdfZararlTamam = new System.Windows.Forms.Button();
            this.dtgrdHedefZararli = new System.Windows.Forms.DataGridView();
            this.Secim = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.HedefZararlilar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel24 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtSunuc = new System.Windows.Forms.TextBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.btnDzjPlanKydt = new System.Windows.Forms.Button();
            this.panel25 = new System.Windows.Forms.Panel();
            this.btnDzjPlanYazdr = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.dtgrdYapiAlanBlg = new System.Windows.Forms.DataGridView();
            this.AlanAdi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sicaklik = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EstHLT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaruzKalmaSuresi = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.AlanHacmi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GerekliIlac = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KullanilacakSilindirSayisi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HedefCT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnDzjPlaniHesapla = new System.Windows.Forms.Button();
            this.panel23 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtAmbalajSekli = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtAmbalajAdedi = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtPartiNo = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtUrunMiktari = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtUrunCinsi = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnHedefZararli = new System.Windows.Forms.Button();
            this.txtNeMaksatlaY = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtKonteynerNo = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.cmbEmtia = new System.Windows.Forms.ComboBox();
            this.cmbBsncTip = new System.Windows.Forms.ComboBox();
            this.cmbFumiSekli = new System.Windows.Forms.ComboBox();
            this.cmbDozaj = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtFumLisans2 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtFumAdSoyad2 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtFumAdSoyad1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtYapildigiYer = new System.Windows.Forms.TextBox();
            this.txtKayitNo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtFumLisans1 = new System.Windows.Forms.TextBox();
            this.dtIlaclamaTrh = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbHedefZararli = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabPageIntroductionPlan = new System.Windows.Forms.TabPage();
            this.panel21 = new System.Windows.Forms.Panel();
            this.btnUyglmPlnKydt = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnUyglmPlanYzdr = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dtgrdUyglmPlan = new System.Windows.Forms.DataGridView();
            this.AlanAdi2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.GirisHatti = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Silindir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Uzunluk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FanKapasitesi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FanSayisi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SilindirSayisi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabPageMonitoringPlan = new System.Windows.Forms.TabPage();
            this.label29 = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.btnIzlmPlnKydt = new System.Windows.Forms.Button();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnIzlemePlanYazdr = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.dtgrdIzlemePlan = new System.Windows.Forms.DataGridView();
            this.AlanAdi4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.IzlemeNoktasiAdi2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OlcumNoktasi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HortumMesafesi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tabPageMonitoringDataInput = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtSonucIzlemeVeriGirdi = new System.Windows.Forms.TextBox();
            this.panel20 = new System.Windows.Forms.Panel();
            this.btnIzlemeVeriGrafk = new System.Windows.Forms.Button();
            this.panel26 = new System.Windows.Forms.Panel();
            this.btnIzlemeVeriGirdiYazdr = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.dtgrdIzlemeVeriGrdi = new System.Windows.Forms.DataGridView();
            this.AlanAdi3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tarih = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Zaman = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sicaklik2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IzlemeNoktasiAdi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IzlemeNoktasiID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Konsantrasyon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HatOrtKonsantrasyon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HLT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aciklama2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnVeriHesapla = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.listBoxVeriFiltre = new System.Windows.Forms.ListBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.dtBasSaat = new System.Windows.Forms.DateTimePicker();
            this.dtBasTarih = new System.Windows.Forms.DateTimePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.tabPageGraph = new System.Windows.Forms.TabPage();
            this.panel18 = new System.Windows.Forms.Panel();
            this.chartZamanGrafk = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel16 = new System.Windows.Forms.Panel();
            this.lblGrfkIlaveMesaj = new System.Windows.Forms.Label();
            this.lblGrfkBaslik = new System.Windows.Forms.Label();
            this.btnZamanGrafikYazdr = new System.Windows.Forms.Button();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.listBoxGrafkFiltre = new System.Windows.Forms.ListBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageHistory.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGecmisUyglm)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.tabPageCustomerInfo.SuspendLayout();
            this.panel19.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPageFumigationDosagePlan.SuspendLayout();
            this.panelHedefZararli.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdHedefZararli)).BeginInit();
            this.panel24.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel25.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdYapiAlanBlg)).BeginInit();
            this.panel8.SuspendLayout();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPageIntroductionPlan.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdUyglmPlan)).BeginInit();
            this.tabPageMonitoringPlan.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel14.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdIzlemePlan)).BeginInit();
            this.tabPageMonitoringDataInput.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel26.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdIzlemeVeriGrdi)).BeginInit();
            this.panel11.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabPageGraph.SuspendLayout();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartZamanGrafk)).BeginInit();
            this.panel16.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.menuStrip1.Font = new System.Drawing.Font("Tahoma", 11F);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1007, 26);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewtoolStripMenuItem,
            this.ReporttoolStripMenuItem,
            this.LogoutToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(61, 22);
            this.fileToolStripMenuItem.Text = "Dosya";
            // 
            // NewtoolStripMenuItem
            // 
            this.NewtoolStripMenuItem.Image = global::Fumitrack.Properties.Resources.newFile;
            this.NewtoolStripMenuItem.Name = "NewtoolStripMenuItem";
            this.NewtoolStripMenuItem.Size = new System.Drawing.Size(185, 30);
            this.NewtoolStripMenuItem.Text = "Yeni Proje";
            this.NewtoolStripMenuItem.Click += new System.EventHandler(this.NewtoolStripMenuItem_Click);
            // 
            // ReporttoolStripMenuItem
            // 
            this.ReporttoolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.normalRaporToolStripMenuItem,
            this.avustralyaRaporToolStripMenuItem,
            this.avustralyaRapor2ToolStripMenuItem,
            this.avustralyaRapor3ToolStripMenuItem,
            this.avustralyaRapor4ToolStripMenuItem});
            this.ReporttoolStripMenuItem.Image = global::Fumitrack.Properties.Resources.Report;
            this.ReporttoolStripMenuItem.Name = "ReporttoolStripMenuItem";
            this.ReporttoolStripMenuItem.Size = new System.Drawing.Size(185, 30);
            this.ReporttoolStripMenuItem.Text = "Rapor";
            // 
            // normalRaporToolStripMenuItem
            // 
            this.normalRaporToolStripMenuItem.Name = "normalRaporToolStripMenuItem";
            this.normalRaporToolStripMenuItem.Size = new System.Drawing.Size(363, 22);
            this.normalRaporToolStripMenuItem.Text = "Fumigasyon Raporu";
            this.normalRaporToolStripMenuItem.Click += new System.EventHandler(this.normalRaporToolStripMenuItem_Click);
            // 
            // avustralyaRaporToolStripMenuItem
            // 
            this.avustralyaRaporToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.turkceToolStripMenuItem,
            this.ingilizceToolStripMenuItem});
            this.avustralyaRaporToolStripMenuItem.Name = "avustralyaRaporToolStripMenuItem";
            this.avustralyaRaporToolStripMenuItem.Size = new System.Drawing.Size(363, 22);
            this.avustralyaRaporToolStripMenuItem.Text = "BMSB-Sulfuryl-Fluoride-Treatment-Certificate";
            // 
            // turkceToolStripMenuItem
            // 
            this.turkceToolStripMenuItem.Name = "turkceToolStripMenuItem";
            this.turkceToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.turkceToolStripMenuItem.Text = "Türkçe";
            this.turkceToolStripMenuItem.Click += new System.EventHandler(this.turkceToolStripMenuItem_Click);
            // 
            // ingilizceToolStripMenuItem
            // 
            this.ingilizceToolStripMenuItem.Name = "ingilizceToolStripMenuItem";
            this.ingilizceToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.ingilizceToolStripMenuItem.Text = "İngilizce";
            this.ingilizceToolStripMenuItem.Click += new System.EventHandler(this.ingilizceToolStripMenuItem_Click);
            // 
            // avustralyaRapor2ToolStripMenuItem
            // 
            this.avustralyaRapor2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.turkce2ToolStripMenuItem,
            this.ingilizce2ToolStripMenuItem1});
            this.avustralyaRapor2ToolStripMenuItem.Name = "avustralyaRapor2ToolStripMenuItem";
            this.avustralyaRapor2ToolStripMenuItem.Size = new System.Drawing.Size(363, 22);
            this.avustralyaRapor2ToolStripMenuItem.Text = "BMSB-Sulfuryl-Metric";
            // 
            // turkce2ToolStripMenuItem
            // 
            this.turkce2ToolStripMenuItem.Name = "turkce2ToolStripMenuItem";
            this.turkce2ToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.turkce2ToolStripMenuItem.Text = "Türkçe";
            this.turkce2ToolStripMenuItem.Click += new System.EventHandler(this.turkce2ToolStripMenuItem_Click);
            // 
            // ingilizce2ToolStripMenuItem1
            // 
            this.ingilizce2ToolStripMenuItem1.Name = "ingilizce2ToolStripMenuItem1";
            this.ingilizce2ToolStripMenuItem1.Size = new System.Drawing.Size(126, 22);
            this.ingilizce2ToolStripMenuItem1.Text = "İngilizce";
            this.ingilizce2ToolStripMenuItem1.Click += new System.EventHandler(this.ingilizce2ToolStripMenuItem1_Click);
            // 
            // avustralyaRapor3ToolStripMenuItem
            // 
            this.avustralyaRapor3ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.turkce3ToolStripMenuItem1,
            this.ingilizce3ToolStripMenuItem2});
            this.avustralyaRapor3ToolStripMenuItem.Name = "avustralyaRapor3ToolStripMenuItem";
            this.avustralyaRapor3ToolStripMenuItem.Size = new System.Drawing.Size(363, 22);
            this.avustralyaRapor3ToolStripMenuItem.Text = "BMSB-Sulfuryl-Imperial";
            // 
            // turkce3ToolStripMenuItem1
            // 
            this.turkce3ToolStripMenuItem1.Name = "turkce3ToolStripMenuItem1";
            this.turkce3ToolStripMenuItem1.Size = new System.Drawing.Size(126, 22);
            this.turkce3ToolStripMenuItem1.Text = "Türkçe";
            this.turkce3ToolStripMenuItem1.Click += new System.EventHandler(this.turkce3ToolStripMenuItem1_Click);
            // 
            // ingilizce3ToolStripMenuItem2
            // 
            this.ingilizce3ToolStripMenuItem2.Name = "ingilizce3ToolStripMenuItem2";
            this.ingilizce3ToolStripMenuItem2.Size = new System.Drawing.Size(126, 22);
            this.ingilizce3ToolStripMenuItem2.Text = "İngilizce";
            this.ingilizce3ToolStripMenuItem2.Click += new System.EventHandler(this.ingilizce3ToolStripMenuItem2_Click);
            // 
            // avustralyaRapor4ToolStripMenuItem
            // 
            this.avustralyaRapor4ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.turkce4ToolStripMenuItem2,
            this.ingilizce4ToolStripMenuItem3});
            this.avustralyaRapor4ToolStripMenuItem.Name = "avustralyaRapor4ToolStripMenuItem";
            this.avustralyaRapor4ToolStripMenuItem.Size = new System.Drawing.Size(363, 22);
            this.avustralyaRapor4ToolStripMenuItem.Text = "BMSB-Sulfuryl-3rd-Party";
            // 
            // turkce4ToolStripMenuItem2
            // 
            this.turkce4ToolStripMenuItem2.Name = "turkce4ToolStripMenuItem2";
            this.turkce4ToolStripMenuItem2.Size = new System.Drawing.Size(126, 22);
            this.turkce4ToolStripMenuItem2.Text = "Türkçe";
            this.turkce4ToolStripMenuItem2.Click += new System.EventHandler(this.turkce4ToolStripMenuItem2_Click);
            // 
            // ingilizce4ToolStripMenuItem3
            // 
            this.ingilizce4ToolStripMenuItem3.Name = "ingilizce4ToolStripMenuItem3";
            this.ingilizce4ToolStripMenuItem3.Size = new System.Drawing.Size(126, 22);
            this.ingilizce4ToolStripMenuItem3.Text = "İngilizce";
            this.ingilizce4ToolStripMenuItem3.Click += new System.EventHandler(this.ingilizce4ToolStripMenuItem3_Click);
            // 
            // LogoutToolStripMenuItem
            // 
            this.LogoutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("LogoutToolStripMenuItem.Image")));
            this.LogoutToolStripMenuItem.Name = "LogoutToolStripMenuItem";
            this.LogoutToolStripMenuItem.Size = new System.Drawing.Size(185, 30);
            this.LogoutToolStripMenuItem.Text = "Oturumu Kapat";
            this.LogoutToolStripMenuItem.Click += new System.EventHandler(this.LogoutToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.dilToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(66, 22);
            this.helpToolStripMenuItem.Text = "Yardım";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.aboutToolStripMenuItem.Text = "Hakkında";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // dilToolStripMenuItem
            // 
            this.dilToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.türkçeToolStripMenuItem,
            this.ingilizceToolStripMenuItem1});
            this.dilToolStripMenuItem.Name = "dilToolStripMenuItem";
            this.dilToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.dilToolStripMenuItem.Text = "Dil";
            // 
            // türkçeToolStripMenuItem
            // 
            this.türkçeToolStripMenuItem.Name = "türkçeToolStripMenuItem";
            this.türkçeToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.türkçeToolStripMenuItem.Text = "Türkçe";
            this.türkçeToolStripMenuItem.Click += new System.EventHandler(this.türkçeToolStripMenuItem_Click);
            // 
            // ingilizceToolStripMenuItem1
            // 
            this.ingilizceToolStripMenuItem1.Name = "ingilizceToolStripMenuItem1";
            this.ingilizceToolStripMenuItem1.Size = new System.Drawing.Size(126, 22);
            this.ingilizceToolStripMenuItem1.Text = "İngilizce";
            this.ingilizceToolStripMenuItem1.Click += new System.EventHandler(this.ingilizceToolStripMenuItem1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageHistory);
            this.tabControl1.Controls.Add(this.tabPageCustomerInfo);
            this.tabControl1.Controls.Add(this.tabPageFumigationDosagePlan);
            this.tabControl1.Controls.Add(this.tabPageIntroductionPlan);
            this.tabControl1.Controls.Add(this.tabPageMonitoringPlan);
            this.tabControl1.Controls.Add(this.tabPageMonitoringDataInput);
            this.tabControl1.Controls.Add(this.tabPageGraph);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tabControl1.Location = new System.Drawing.Point(0, 26);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1007, 638);
            this.tabControl1.TabIndex = 8;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPageHistory
            // 
            this.tabPageHistory.AutoScroll = true;
            this.tabPageHistory.Controls.Add(this.groupBox7);
            this.tabPageHistory.Controls.Add(this.panel4);
            this.tabPageHistory.Location = new System.Drawing.Point(4, 26);
            this.tabPageHistory.Name = "tabPageHistory";
            this.tabPageHistory.Size = new System.Drawing.Size(999, 608);
            this.tabPageHistory.TabIndex = 8;
            this.tabPageHistory.Text = "Geçmiş Uygulamalarım";
            this.tabPageHistory.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.White;
            this.groupBox7.Controls.Add(this.dtgrdGecmisUyglm);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox7.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox7.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox7.Location = new System.Drawing.Point(0, 18);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(999, 295);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Geçmiş Uygulamalarım";
            // 
            // dtgrdGecmisUyglm
            // 
            this.dtgrdGecmisUyglm.AllowUserToAddRows = false;
            this.dtgrdGecmisUyglm.AllowUserToDeleteRows = false;
            this.dtgrdGecmisUyglm.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgrdGecmisUyglm.BackgroundColor = System.Drawing.Color.White;
            this.dtgrdGecmisUyglm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdGecmisUyglm.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FirmaAdi,
            this.IlaclamaTarihi,
            this.DozajPlanIDGecmis});
            this.dtgrdGecmisUyglm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgrdGecmisUyglm.Location = new System.Drawing.Point(3, 21);
            this.dtgrdGecmisUyglm.MultiSelect = false;
            this.dtgrdGecmisUyglm.Name = "dtgrdGecmisUyglm";
            this.dtgrdGecmisUyglm.ReadOnly = true;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dtgrdGecmisUyglm.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgrdGecmisUyglm.RowTemplate.ContextMenuStrip = this.contextMenuStrip1;
            this.dtgrdGecmisUyglm.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdGecmisUyglm.Size = new System.Drawing.Size(993, 271);
            this.dtgrdGecmisUyglm.TabIndex = 1;
            this.dtgrdGecmisUyglm.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dtgrdGecmisUyglm_MouseDown);
            // 
            // FirmaAdi
            // 
            this.FirmaAdi.HeaderText = "Firma Adı";
            this.FirmaAdi.Name = "FirmaAdi";
            this.FirmaAdi.ReadOnly = true;
            // 
            // IlaclamaTarihi
            // 
            this.IlaclamaTarihi.HeaderText = "İlaçlama Tarihi";
            this.IlaclamaTarihi.Name = "IlaclamaTarihi";
            this.IlaclamaTarihi.ReadOnly = true;
            // 
            // DozajPlanIDGecmis
            // 
            this.DozajPlanIDGecmis.HeaderText = "DozajPlanIDGecmis";
            this.DozajPlanIDGecmis.Name = "DozajPlanIDGecmis";
            this.DozajPlanIDGecmis.ReadOnly = true;
            this.DozajPlanIDGecmis.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemRapor,
            this.toolStripMenuItemVeriGor,
            this.toolStripMenuItemSil});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(188, 70);
            // 
            // toolStripMenuItemRapor
            // 
            this.toolStripMenuItemRapor.Name = "toolStripMenuItemRapor";
            this.toolStripMenuItemRapor.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItemRapor.Text = "Rapor (Report)";
            this.toolStripMenuItemRapor.Click += new System.EventHandler(this.toolStripMenuItemRapor_Click);
            // 
            // toolStripMenuItemVeriGor
            // 
            this.toolStripMenuItemVeriGor.Name = "toolStripMenuItemVeriGor";
            this.toolStripMenuItemVeriGor.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItemVeriGor.Text = "Verileri Gör (See Data)";
            this.toolStripMenuItemVeriGor.Click += new System.EventHandler(this.toolStripMenuItemVeriGor_Click);
            // 
            // toolStripMenuItemSil
            // 
            this.toolStripMenuItemSil.Name = "toolStripMenuItemSil";
            this.toolStripMenuItemSil.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItemSil.Text = "Sil (Delete)";
            this.toolStripMenuItemSil.Click += new System.EventHandler(this.toolStripMenuItemSil_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(999, 18);
            this.panel4.TabIndex = 1;
            // 
            // tabPageCustomerInfo
            // 
            this.tabPageCustomerInfo.AutoScroll = true;
            this.tabPageCustomerInfo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPageCustomerInfo.Controls.Add(this.panel19);
            this.tabPageCustomerInfo.Controls.Add(this.groupBox1);
            this.tabPageCustomerInfo.Controls.Add(this.panel1);
            this.tabPageCustomerInfo.Location = new System.Drawing.Point(4, 26);
            this.tabPageCustomerInfo.Name = "tabPageCustomerInfo";
            this.tabPageCustomerInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCustomerInfo.Size = new System.Drawing.Size(999, 608);
            this.tabPageCustomerInfo.TabIndex = 0;
            this.tabPageCustomerInfo.Text = "Uygulama Yapılan Firma Bilgileri";
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.btnFirmaKydt);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel19.Location = new System.Drawing.Point(814, 316);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(182, 289);
            this.panel19.TabIndex = 3;
            // 
            // btnFirmaKydt
            // 
            this.btnFirmaKydt.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnFirmaKydt.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnFirmaKydt.Location = new System.Drawing.Point(0, 0);
            this.btnFirmaKydt.Name = "btnFirmaKydt";
            this.btnFirmaKydt.Size = new System.Drawing.Size(182, 43);
            this.btnFirmaKydt.TabIndex = 7;
            this.btnFirmaKydt.Text = "Kaydet ve İlerle >>";
            this.btnFirmaKydt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFirmaKydt.UseVisualStyleBackColor = true;
            this.btnFirmaKydt.Click += new System.EventHandler(this.BtnFirmaKydt_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.cmbFirmaAdi);
            this.groupBox1.Controls.Add(this.txtTelefon);
            this.groupBox1.Controls.Add(this.txtAdres);
            this.groupBox1.Controls.Add(this.txtAciklama);
            this.groupBox1.Controls.Add(this.txtEposta);
            this.groupBox1.Controls.Add(this.txtSehir);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox1.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox1.Location = new System.Drawing.Point(3, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(993, 295);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Uygulama Yapılan Firma Bilgileri";
            // 
            // cmbFirmaAdi
            // 
            this.cmbFirmaAdi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFirmaAdi.FormattingEnabled = true;
            this.cmbFirmaAdi.Location = new System.Drawing.Point(174, 45);
            this.cmbFirmaAdi.Name = "cmbFirmaAdi";
            this.cmbFirmaAdi.Size = new System.Drawing.Size(364, 26);
            this.cmbFirmaAdi.TabIndex = 1;
            this.cmbFirmaAdi.SelectedIndexChanged += new System.EventHandler(this.CmbFirmaAdi_SelectedIndexChanged);
            // 
            // txtTelefon
            // 
            this.txtTelefon.Location = new System.Drawing.Point(174, 94);
            this.txtTelefon.Mask = "(999) 000-0000";
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(364, 25);
            this.txtTelefon.TabIndex = 3;
            // 
            // txtAdres
            // 
            this.txtAdres.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdres.Location = new System.Drawing.Point(673, 94);
            this.txtAdres.Multiline = true;
            this.txtAdres.Name = "txtAdres";
            this.txtAdres.Size = new System.Drawing.Size(260, 77);
            this.txtAdres.TabIndex = 4;
            // 
            // txtAciklama
            // 
            this.txtAciklama.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAciklama.Location = new System.Drawing.Point(174, 198);
            this.txtAciklama.Multiline = true;
            this.txtAciklama.Name = "txtAciklama";
            this.txtAciklama.Size = new System.Drawing.Size(759, 78);
            this.txtAciklama.TabIndex = 6;
            // 
            // txtEposta
            // 
            this.txtEposta.Location = new System.Drawing.Point(174, 146);
            this.txtEposta.Name = "txtEposta";
            this.txtEposta.Size = new System.Drawing.Size(364, 25);
            this.txtEposta.TabIndex = 5;
            // 
            // txtSehir
            // 
            this.txtSehir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSehir.Location = new System.Drawing.Point(673, 45);
            this.txtSehir.Name = "txtSehir";
            this.txtSehir.Size = new System.Drawing.Size(260, 25);
            this.txtSehir.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(48, 201);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 18);
            this.label5.TabIndex = 0;
            this.label5.Text = "Açıklama";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(48, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 18);
            this.label4.TabIndex = 0;
            this.label4.Text = "E-Posta Adresi";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(48, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "Telefon";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(610, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 18);
            this.label6.TabIndex = 0;
            this.label6.Text = "Şehir";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(610, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Adres";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(48, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "*Firma İsmi";
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(993, 18);
            this.panel1.TabIndex = 0;
            // 
            // tabPageFumigationDosagePlan
            // 
            this.tabPageFumigationDosagePlan.AutoScroll = true;
            this.tabPageFumigationDosagePlan.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPageFumigationDosagePlan.Controls.Add(this.panelHedefZararli);
            this.tabPageFumigationDosagePlan.Controls.Add(this.panel24);
            this.tabPageFumigationDosagePlan.Controls.Add(this.groupBox8);
            this.tabPageFumigationDosagePlan.Controls.Add(this.panel23);
            this.tabPageFumigationDosagePlan.Controls.Add(this.panel7);
            this.tabPageFumigationDosagePlan.Controls.Add(this.groupBox3);
            this.tabPageFumigationDosagePlan.Controls.Add(this.groupBox2);
            this.tabPageFumigationDosagePlan.Controls.Add(this.panel2);
            this.tabPageFumigationDosagePlan.Location = new System.Drawing.Point(4, 26);
            this.tabPageFumigationDosagePlan.Name = "tabPageFumigationDosagePlan";
            this.tabPageFumigationDosagePlan.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFumigationDosagePlan.Size = new System.Drawing.Size(999, 608);
            this.tabPageFumigationDosagePlan.TabIndex = 1;
            this.tabPageFumigationDosagePlan.Text = "Fumigasyon Dozaj Planı";
            // 
            // panelHedefZararli
            // 
            this.panelHedefZararli.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelHedefZararli.Controls.Add(this.btnHdfZararlIptal);
            this.panelHedefZararli.Controls.Add(this.btnHdfZararlTamam);
            this.panelHedefZararli.Controls.Add(this.dtgrdHedefZararli);
            this.panelHedefZararli.Location = new System.Drawing.Point(226, 161);
            this.panelHedefZararli.Name = "panelHedefZararli";
            this.panelHedefZararli.Size = new System.Drawing.Size(166, 20);
            this.panelHedefZararli.TabIndex = 2260;
            this.panelHedefZararli.Visible = false;
            // 
            // btnHdfZararlIptal
            // 
            this.btnHdfZararlIptal.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnHdfZararlIptal.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnHdfZararlIptal.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnHdfZararlIptal.Location = new System.Drawing.Point(-68, 237);
            this.btnHdfZararlIptal.Name = "btnHdfZararlIptal";
            this.btnHdfZararlIptal.Size = new System.Drawing.Size(116, 0);
            this.btnHdfZararlIptal.TabIndex = 2260;
            this.btnHdfZararlIptal.Text = "İPTAL";
            this.btnHdfZararlIptal.UseVisualStyleBackColor = true;
            this.btnHdfZararlIptal.Click += new System.EventHandler(this.btnHdfZararlIptal_Click);
            // 
            // btnHdfZararlTamam
            // 
            this.btnHdfZararlTamam.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnHdfZararlTamam.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnHdfZararlTamam.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnHdfZararlTamam.Location = new System.Drawing.Point(48, 237);
            this.btnHdfZararlTamam.Name = "btnHdfZararlTamam";
            this.btnHdfZararlTamam.Size = new System.Drawing.Size(116, 0);
            this.btnHdfZararlTamam.TabIndex = 2259;
            this.btnHdfZararlTamam.Text = "TAMAM";
            this.btnHdfZararlTamam.UseVisualStyleBackColor = true;
            this.btnHdfZararlTamam.Click += new System.EventHandler(this.btnHdfZararlTamam_Click);
            // 
            // dtgrdHedefZararli
            // 
            this.dtgrdHedefZararli.AllowUserToAddRows = false;
            this.dtgrdHedefZararli.AllowUserToDeleteRows = false;
            this.dtgrdHedefZararli.AllowUserToOrderColumns = true;
            this.dtgrdHedefZararli.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgrdHedefZararli.BackgroundColor = System.Drawing.Color.White;
            this.dtgrdHedefZararli.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdHedefZararli.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Secim,
            this.HedefZararlilar});
            this.dtgrdHedefZararli.Dock = System.Windows.Forms.DockStyle.Top;
            this.dtgrdHedefZararli.Location = new System.Drawing.Point(0, 0);
            this.dtgrdHedefZararli.Name = "dtgrdHedefZararli";
            this.dtgrdHedefZararli.Size = new System.Drawing.Size(164, 237);
            this.dtgrdHedefZararli.TabIndex = 2258;
            // 
            // Secim
            // 
            this.Secim.FillWeight = 35F;
            this.Secim.HeaderText = "Seçim";
            this.Secim.Name = "Secim";
            // 
            // HedefZararlilar
            // 
            this.HedefZararlilar.FillWeight = 149.2386F;
            this.HedefZararlilar.HeaderText = "Hedef Zararlılar";
            this.HedefZararlilar.Name = "HedefZararlilar";
            this.HedefZararlilar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.HedefZararlilar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.groupBox4);
            this.panel24.Controls.Add(this.panel13);
            this.panel24.Controls.Add(this.panel25);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(3, 540);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(993, 64);
            this.panel24.TabIndex = 11;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.White;
            this.groupBox4.Controls.Add(this.txtSunuc);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.groupBox4.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(593, 64);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Sonuçlar";
            // 
            // txtSunuc
            // 
            this.txtSunuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSunuc.Location = new System.Drawing.Point(3, 20);
            this.txtSunuc.Multiline = true;
            this.txtSunuc.Name = "txtSunuc";
            this.txtSunuc.ReadOnly = true;
            this.txtSunuc.Size = new System.Drawing.Size(587, 41);
            this.txtSunuc.TabIndex = 3;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.btnDzjPlanKydt);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel13.Location = new System.Drawing.Point(593, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(200, 64);
            this.panel13.TabIndex = 1;
            // 
            // btnDzjPlanKydt
            // 
            this.btnDzjPlanKydt.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDzjPlanKydt.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnDzjPlanKydt.Location = new System.Drawing.Point(0, 0);
            this.btnDzjPlanKydt.Name = "btnDzjPlanKydt";
            this.btnDzjPlanKydt.Size = new System.Drawing.Size(200, 43);
            this.btnDzjPlanKydt.TabIndex = 29;
            this.btnDzjPlanKydt.Text = "Kaydet ve İlerle >>";
            this.btnDzjPlanKydt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDzjPlanKydt.UseVisualStyleBackColor = true;
            this.btnDzjPlanKydt.Click += new System.EventHandler(this.BtnDzjPlanKydt_Click);
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.btnDzjPlanYazdr);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel25.Location = new System.Drawing.Point(793, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(200, 64);
            this.panel25.TabIndex = 0;
            // 
            // btnDzjPlanYazdr
            // 
            this.btnDzjPlanYazdr.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDzjPlanYazdr.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnDzjPlanYazdr.Image = global::Fumitrack.Properties.Resources.Printer;
            this.btnDzjPlanYazdr.Location = new System.Drawing.Point(0, 0);
            this.btnDzjPlanYazdr.Name = "btnDzjPlanYazdr";
            this.btnDzjPlanYazdr.Size = new System.Drawing.Size(200, 43);
            this.btnDzjPlanYazdr.TabIndex = 30;
            this.btnDzjPlanYazdr.Text = " YAZDIR";
            this.btnDzjPlanYazdr.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDzjPlanYazdr.UseVisualStyleBackColor = true;
            this.btnDzjPlanYazdr.Click += new System.EventHandler(this.btnDzjPlanYazdr_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.White;
            this.groupBox8.Controls.Add(this.panel12);
            this.groupBox8.Controls.Add(this.panel8);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox8.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.groupBox8.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox8.Location = new System.Drawing.Point(3, 342);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(993, 198);
            this.groupBox8.TabIndex = 10;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Yapı/Alan Bilgileri (Bir yapı birden fazla alana sahip olabilir)";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.dtgrdYapiAlanBlg);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(3, 20);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(871, 175);
            this.panel12.TabIndex = 2;
            // 
            // dtgrdYapiAlanBlg
            // 
            this.dtgrdYapiAlanBlg.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgrdYapiAlanBlg.BackgroundColor = System.Drawing.Color.White;
            this.dtgrdYapiAlanBlg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdYapiAlanBlg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AlanAdi,
            this.Sicaklik,
            this.EstHLT,
            this.MaruzKalmaSuresi,
            this.AlanHacmi,
            this.GerekliIlac,
            this.KullanilacakSilindirSayisi,
            this.HedefCT});
            this.dtgrdYapiAlanBlg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgrdYapiAlanBlg.Location = new System.Drawing.Point(0, 0);
            this.dtgrdYapiAlanBlg.Name = "dtgrdYapiAlanBlg";
            this.dtgrdYapiAlanBlg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdYapiAlanBlg.Size = new System.Drawing.Size(871, 175);
            this.dtgrdYapiAlanBlg.TabIndex = 27;
            // 
            // AlanAdi
            // 
            this.AlanAdi.HeaderText = "Alan Adı";
            this.AlanAdi.Name = "AlanAdi";
            // 
            // Sicaklik
            // 
            this.Sicaklik.HeaderText = "Sıcaklık";
            this.Sicaklik.Name = "Sicaklik";
            this.Sicaklik.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Sicaklik.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // EstHLT
            // 
            this.EstHLT.HeaderText = "Yarılama Süresi";
            this.EstHLT.Name = "EstHLT";
            this.EstHLT.ReadOnly = true;
            // 
            // MaruzKalmaSuresi
            // 
            this.MaruzKalmaSuresi.HeaderText = "Maruz Kalma Süresi";
            this.MaruzKalmaSuresi.Items.AddRange(new object[] {
            "12",
            "24"});
            this.MaruzKalmaSuresi.MaxDropDownItems = 2;
            this.MaruzKalmaSuresi.Name = "MaruzKalmaSuresi";
            this.MaruzKalmaSuresi.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MaruzKalmaSuresi.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // AlanHacmi
            // 
            this.AlanHacmi.HeaderText = "*Alan Hacmi (m3)";
            this.AlanHacmi.Name = "AlanHacmi";
            // 
            // GerekliIlac
            // 
            this.GerekliIlac.HeaderText = "Kullanılacak Fumigant Miktarı (gr)";
            this.GerekliIlac.Name = "GerekliIlac";
            this.GerekliIlac.ReadOnly = true;
            // 
            // KullanilacakSilindirSayisi
            // 
            this.KullanilacakSilindirSayisi.HeaderText = "Kullanılacak Silindir Sayısı";
            this.KullanilacakSilindirSayisi.Name = "KullanilacakSilindirSayisi";
            this.KullanilacakSilindirSayisi.ReadOnly = true;
            // 
            // HedefCT
            // 
            this.HedefCT.HeaderText = "Hedef CT (gr)";
            this.HedefCT.Name = "HedefCT";
            this.HedefCT.ReadOnly = true;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnDzjPlaniHesapla);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(874, 20);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(116, 175);
            this.panel8.TabIndex = 1;
            // 
            // btnDzjPlaniHesapla
            // 
            this.btnDzjPlaniHesapla.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnDzjPlaniHesapla.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnDzjPlaniHesapla.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDzjPlaniHesapla.Location = new System.Drawing.Point(0, 141);
            this.btnDzjPlaniHesapla.Name = "btnDzjPlaniHesapla";
            this.btnDzjPlaniHesapla.Size = new System.Drawing.Size(116, 34);
            this.btnDzjPlaniHesapla.TabIndex = 28;
            this.btnDzjPlaniHesapla.Text = "Hesapla";
            this.btnDzjPlaniHesapla.UseVisualStyleBackColor = true;
            this.btnDzjPlaniHesapla.Click += new System.EventHandler(this.BtnDzjPlaniHesapla_Click);
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.White;
            this.panel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel23.Controls.Add(this.pictureBox1);
            this.panel23.Controls.Add(this.txtAmbalajSekli);
            this.panel23.Controls.Add(this.label25);
            this.panel23.Controls.Add(this.txtAmbalajAdedi);
            this.panel23.Controls.Add(this.label26);
            this.panel23.Controls.Add(this.txtPartiNo);
            this.panel23.Controls.Add(this.label23);
            this.panel23.Controls.Add(this.txtUrunMiktari);
            this.panel23.Controls.Add(this.label22);
            this.panel23.Controls.Add(this.txtUrunCinsi);
            this.panel23.Controls.Add(this.label18);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.panel23.Location = new System.Drawing.Point(3, 255);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(993, 87);
            this.panel23.TabIndex = 9;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Fumitrack.Properties.Resources.line;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(8, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(982, 10);
            this.pictureBox1.TabIndex = 2248;
            this.pictureBox1.TabStop = false;
            // 
            // txtAmbalajSekli
            // 
            this.txtAmbalajSekli.Location = new System.Drawing.Point(597, 55);
            this.txtAmbalajSekli.Name = "txtAmbalajSekli";
            this.txtAmbalajSekli.Size = new System.Drawing.Size(186, 24);
            this.txtAmbalajSekli.TabIndex = 26;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label25.Location = new System.Drawing.Point(398, 58);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(153, 17);
            this.label25.TabIndex = 2254;
            this.label25.Text = "Ambalaj Şekli ve Markası";
            // 
            // txtAmbalajAdedi
            // 
            this.txtAmbalajAdedi.Location = new System.Drawing.Point(170, 54);
            this.txtAmbalajAdedi.Name = "txtAmbalajAdedi";
            this.txtAmbalajAdedi.Size = new System.Drawing.Size(209, 24);
            this.txtAmbalajAdedi.TabIndex = 25;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label26.Location = new System.Drawing.Point(9, 58);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(93, 17);
            this.label26.TabIndex = 2252;
            this.label26.Text = "Ambalaj Adedi";
            // 
            // txtPartiNo
            // 
            this.txtPartiNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPartiNo.Location = new System.Drawing.Point(923, 21);
            this.txtPartiNo.Name = "txtPartiNo";
            this.txtPartiNo.Size = new System.Drawing.Size(38, 24);
            this.txtPartiNo.TabIndex = 24;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label23.Location = new System.Drawing.Point(830, 24);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 17);
            this.label23.TabIndex = 2250;
            this.label23.Text = "Parti No";
            // 
            // txtUrunMiktari
            // 
            this.txtUrunMiktari.Location = new System.Drawing.Point(597, 21);
            this.txtUrunMiktari.Name = "txtUrunMiktari";
            this.txtUrunMiktari.Size = new System.Drawing.Size(186, 24);
            this.txtUrunMiktari.TabIndex = 23;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label22.Location = new System.Drawing.Point(398, 23);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(96, 17);
            this.label22.TabIndex = 2248;
            this.label22.Text = "Ürünün Miktarı";
            // 
            // txtUrunCinsi
            // 
            this.txtUrunCinsi.Location = new System.Drawing.Point(170, 20);
            this.txtUrunCinsi.Name = "txtUrunCinsi";
            this.txtUrunCinsi.Size = new System.Drawing.Size(209, 24);
            this.txtUrunCinsi.TabIndex = 22;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label18.Location = new System.Drawing.Point(9, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(85, 17);
            this.label18.TabIndex = 2246;
            this.label18.Text = "Ürünün Cinsi";
            // 
            // panel7
            // 
            this.panel7.Location = new System.Drawing.Point(-27, -98);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(200, 100);
            this.panel7.TabIndex = 8;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.btnHedefZararli);
            this.groupBox3.Controls.Add(this.txtNeMaksatlaY);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.txtKonteynerNo);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.cmbEmtia);
            this.groupBox3.Controls.Add(this.cmbBsncTip);
            this.groupBox3.Controls.Add(this.cmbFumiSekli);
            this.groupBox3.Controls.Add(this.cmbDozaj);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox3.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox3.Location = new System.Drawing.Point(3, 136);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(993, 119);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Hedef Bilgileri";
            // 
            // btnHedefZararli
            // 
            this.btnHedefZararli.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnHedefZararli.ForeColor = System.Drawing.Color.Black;
            this.btnHedefZararli.Location = new System.Drawing.Point(171, 22);
            this.btnHedefZararli.Name = "btnHedefZararli";
            this.btnHedefZararli.Size = new System.Drawing.Size(266, 25);
            this.btnHedefZararli.TabIndex = 15;
            this.btnHedefZararli.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHedefZararli.UseVisualStyleBackColor = true;
            this.btnHedefZararli.Click += new System.EventHandler(this.btnHedefZararli_Click);
            // 
            // txtNeMaksatlaY
            // 
            this.txtNeMaksatlaY.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNeMaksatlaY.Location = new System.Drawing.Point(171, 91);
            this.txtNeMaksatlaY.Name = "txtNeMaksatlaY";
            this.txtNeMaksatlaY.Size = new System.Drawing.Size(791, 24);
            this.txtNeMaksatlaY.TabIndex = 21;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label24.Location = new System.Drawing.Point(2, 94);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(130, 17);
            this.label24.TabIndex = 2256;
            this.label24.Text = "Ne Maksatla Yapıldığı";
            // 
            // txtKonteynerNo
            // 
            this.txtKonteynerNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKonteynerNo.Location = new System.Drawing.Point(913, 60);
            this.txtKonteynerNo.Name = "txtKonteynerNo";
            this.txtKonteynerNo.Size = new System.Drawing.Size(49, 24);
            this.txtKonteynerNo.TabIndex = 20;
            this.txtKonteynerNo.Visible = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label28.Location = new System.Drawing.Point(783, 64);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(101, 17);
            this.label28.TabIndex = 2248;
            this.label28.Text = "*Konteyner No";
            this.label28.Visible = false;
            // 
            // cmbEmtia
            // 
            this.cmbEmtia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbEmtia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmtia.FormattingEnabled = true;
            this.cmbEmtia.Items.AddRange(new object[] {
            "YOK (ABSENT)",
            "HUBUBAT",
            "BAKLİYAT",
            "KURU MEYVE",
            "AHŞAP VE TÜREVLERİ",
            "KAĞIT-MUKAVVA",
            "AVUST. WEB SAYFASINDAKİ İLGİLİ ÜRÜNLER"});
            this.cmbEmtia.Location = new System.Drawing.Point(913, 21);
            this.cmbEmtia.Name = "cmbEmtia";
            this.cmbEmtia.Size = new System.Drawing.Size(49, 25);
            this.cmbEmtia.TabIndex = 17;
            this.cmbEmtia.SelectedIndexChanged += new System.EventHandler(this.cmbEmtia_SelectedIndexChanged);
            // 
            // cmbBsncTip
            // 
            this.cmbBsncTip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBsncTip.FormattingEnabled = true;
            this.cmbBsncTip.Items.AddRange(new object[] {
            "ATMOSFERİK FUMİGASYON",
            "VAKUM FUMİGASYON"});
            this.cmbBsncTip.Location = new System.Drawing.Point(581, 61);
            this.cmbBsncTip.Name = "cmbBsncTip";
            this.cmbBsncTip.Size = new System.Drawing.Size(175, 25);
            this.cmbBsncTip.TabIndex = 19;
            // 
            // cmbFumiSekli
            // 
            this.cmbFumiSekli.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFumiSekli.FormattingEnabled = true;
            this.cmbFumiSekli.Items.AddRange(new object[] {
            "GEMİ FUMİGASYONU",
            "BİNA FUMİGASYONU",
            "ÖRTÜALTI FUMİGASYONU",
            "KONTEYNER FUMİGASYONU",
            "SİLO FUMİGASYONU",
            "VAKUM FUMİGASYONU"});
            this.cmbFumiSekli.Location = new System.Drawing.Point(171, 61);
            this.cmbFumiSekli.Name = "cmbFumiSekli";
            this.cmbFumiSekli.Size = new System.Drawing.Size(266, 25);
            this.cmbFumiSekli.TabIndex = 18;
            this.cmbFumiSekli.SelectedIndexChanged += new System.EventHandler(this.cmbFumiSekli_SelectedIndexChanged);
            // 
            // cmbDozaj
            // 
            this.cmbDozaj.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDozaj.FormattingEnabled = true;
            this.cmbDozaj.Items.AddRange(new object[] {
            "15",
            "16",
            "20",
            "24",
            "50",
            "100"});
            this.cmbDozaj.Location = new System.Drawing.Point(581, 22);
            this.cmbDozaj.Name = "cmbDozaj";
            this.cmbDozaj.Size = new System.Drawing.Size(175, 25);
            this.cmbDozaj.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(467, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Basınç Tipi";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label14.Location = new System.Drawing.Point(2, 64);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(112, 17);
            this.label14.TabIndex = 0;
            this.label14.Text = "Fumigasyon Şekli";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label13.Location = new System.Drawing.Point(826, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(50, 17);
            this.label13.TabIndex = 0;
            this.label13.Text = "*Emtia";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(467, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 17);
            this.label12.TabIndex = 0;
            this.label12.Text = "*Dozaj";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(2, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 17);
            this.label11.TabIndex = 0;
            this.label11.Text = "*Hedef Zararlıları";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.txtFumLisans2);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.txtFumAdSoyad2);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtFumAdSoyad1);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.txtYapildigiYer);
            this.groupBox2.Controls.Add(this.txtKayitNo);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtFumLisans1);
            this.groupBox2.Controls.Add(this.dtIlaclamaTrh);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.cmbHedefZararli);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox2.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox2.Location = new System.Drawing.Point(3, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(993, 115);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Genel Bilgiler";
            // 
            // txtFumLisans2
            // 
            this.txtFumLisans2.Location = new System.Drawing.Point(257, 53);
            this.txtFumLisans2.Name = "txtFumLisans2";
            this.txtFumLisans2.Size = new System.Drawing.Size(311, 24);
            this.txtFumLisans2.TabIndex = 10;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label27.Location = new System.Drawing.Point(11, 56);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(182, 17);
            this.label27.TabIndex = 2246;
            this.label27.Text = "Fumigasyonu Yapan Lisansı2";
            // 
            // txtFumAdSoyad2
            // 
            this.txtFumAdSoyad2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFumAdSoyad2.Location = new System.Drawing.Point(898, 53);
            this.txtFumAdSoyad2.Name = "txtFumAdSoyad2";
            this.txtFumAdSoyad2.Size = new System.Drawing.Size(54, 24);
            this.txtFumAdSoyad2.TabIndex = 11;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label17.Location = new System.Drawing.Point(635, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(203, 17);
            this.label17.TabIndex = 2232;
            this.label17.Text = "Fumigasyonu Yapan Ad Soyad2";
            // 
            // txtFumAdSoyad1
            // 
            this.txtFumAdSoyad1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFumAdSoyad1.Location = new System.Drawing.Point(898, 20);
            this.txtFumAdSoyad1.Name = "txtFumAdSoyad1";
            this.txtFumAdSoyad1.Size = new System.Drawing.Size(54, 24);
            this.txtFumAdSoyad1.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label16.Location = new System.Drawing.Point(635, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(203, 17);
            this.label16.TabIndex = 2230;
            this.label16.Text = "*Fumigasyonu Yapan Ad Soyad";
            // 
            // txtYapildigiYer
            // 
            this.txtYapildigiYer.Location = new System.Drawing.Point(486, 85);
            this.txtYapildigiYer.Name = "txtYapildigiYer";
            this.txtYapildigiYer.Size = new System.Drawing.Size(221, 24);
            this.txtYapildigiYer.TabIndex = 13;
            // 
            // txtKayitNo
            // 
            this.txtKayitNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKayitNo.Location = new System.Drawing.Point(898, 86);
            this.txtKayitNo.Name = "txtKayitNo";
            this.txtKayitNo.Size = new System.Drawing.Size(54, 24);
            this.txtKayitNo.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(748, 89);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 17);
            this.label8.TabIndex = 2225;
            this.label8.Text = "Kayıt No";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label15.Location = new System.Drawing.Point(363, 89);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 17);
            this.label15.TabIndex = 2226;
            this.label15.Text = "Yapıldığı Yer";
            // 
            // txtFumLisans1
            // 
            this.txtFumLisans1.Location = new System.Drawing.Point(257, 20);
            this.txtFumLisans1.Name = "txtFumLisans1";
            this.txtFumLisans1.Size = new System.Drawing.Size(311, 24);
            this.txtFumLisans1.TabIndex = 8;
            // 
            // dtIlaclamaTrh
            // 
            this.dtIlaclamaTrh.CustomFormat = "yyyy-MM-dd";
            this.dtIlaclamaTrh.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtIlaclamaTrh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtIlaclamaTrh.Location = new System.Drawing.Point(134, 85);
            this.dtIlaclamaTrh.Name = "dtIlaclamaTrh";
            this.dtIlaclamaTrh.Size = new System.Drawing.Size(181, 24);
            this.dtIlaclamaTrh.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(11, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(182, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "*Fumigasyonu Yapan Lisansı";
            // 
            // cmbHedefZararli
            // 
            this.cmbHedefZararli.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHedefZararli.FormattingEnabled = true;
            this.cmbHedefZararli.Items.AddRange(new object[] {
            "KIRMA BİTİ (Tribolium confusem)",
            "PİRİNÇ BİTİ (Sitophilus oryzae)",
            "UN BİTİ (Tribolium castaneum)",
            "BUĞDAY BİTİ (Sitophilus granarius)",
            "TESTERELİ BÖCEK (Oryzephilus surinamensis)",
            "TÜRK HUBUBAT ZARARLISI (Cryptolestes turcicus)",
            "EKİN KANBUR BİTİ (Rhizopertha dominica)",
            "EKİN KARA BÖCEĞİ (Tenebroides mauritanicus)",
            "KHAPRA BÖCEĞİ (Trogoderma granarium)",
            "ECZANE BÖCEĞİ (Stegobium paniceum)",
            "KİLER BÖCEĞİ (Dermestes lardarius)",
            "KURUTULMUŞ ÜRÜN ZARARLISI (Necrobia rufipes)",
            "KAHVE ÇEKİRDEĞİ BİTİ (Aracerus fasciculatus)",
            "YER FISTIĞI BÖCEĞİ (Caryedon serratus)",
            "FASULYE TOHUMU BÖCEĞİ (Acanthoscelides obtectus)",
            "TATLI KURT (Lasioderma serricorne)",
            "ARPA GÜVESİ (Sitotroga cerealella)",
            "DEĞİRMEN GÜVESİ (Ephestia kuehniella)",
            "TÜTÜN GÜVESİ (Ephestia elutella)",
            "KURU MEYVE GÜVESİ (Plodia interpunctella)",
            "AKARLAR (Acarus siro)",
            "MISIR BİTİ (Sitophilus zeamais)",
            "GENİŞ-BOYNUZLU BÖCEK (Gnathocerus cornutus)",
            "KARA UN BİTİ (Tribolium destructor)",
            "KAKAO GÜVESİ (Ephestia cautella)",
            "EKŞİLİK BÖCEKLERİ (Carpophilus dimidiatus)",
            "PATATES GÜVESİ (Phthorimoea operculellla)",
            "BEZELYE TOHUM BÖCEĞİ (Bruchus pisorum)",
            "BATI ÇİÇEK THRİPSİ (Frankliniella occidentalis)",
            "TAHTA KURUSU (Cimex lectularius)",
            "ELBİSE GÜVESİ (Order lepidoptera)",
            "HALI BÖCEĞİ (Anthrenus verbasci)",
            "HAMAM BÖCEĞİ (Blatta orientalis)",
            "KURU ODUN TERMİTLERİ (Kalotermes flavicollis)",
            "MOBİLYA BİTİ (Anobium punctatum)",
            "AHŞAP DELİCİ BÖCEKLER (Xestobilum rufovillosum)",
            "KAHVERENGİ KOKMUŞ BÖCEK-BMSB (Halyomorpha halys)"});
            this.cmbHedefZararli.Location = new System.Drawing.Point(379, 20);
            this.cmbHedefZararli.Name = "cmbHedefZararli";
            this.cmbHedefZararli.Size = new System.Drawing.Size(218, 25);
            this.cmbHedefZararli.TabIndex = 1;
            this.cmbHedefZararli.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(11, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "İlaçlama Tarihi";
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(993, 18);
            this.panel2.TabIndex = 1;
            // 
            // tabPageIntroductionPlan
            // 
            this.tabPageIntroductionPlan.AutoScroll = true;
            this.tabPageIntroductionPlan.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPageIntroductionPlan.Controls.Add(this.panel21);
            this.tabPageIntroductionPlan.Controls.Add(this.panel5);
            this.tabPageIntroductionPlan.Controls.Add(this.groupBox6);
            this.tabPageIntroductionPlan.Controls.Add(this.panel3);
            this.tabPageIntroductionPlan.Location = new System.Drawing.Point(4, 26);
            this.tabPageIntroductionPlan.Name = "tabPageIntroductionPlan";
            this.tabPageIntroductionPlan.Size = new System.Drawing.Size(999, 608);
            this.tabPageIntroductionPlan.TabIndex = 2;
            this.tabPageIntroductionPlan.Text = "Uygulama Planı";
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.btnUyglmPlnKydt);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel21.Location = new System.Drawing.Point(694, 321);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(182, 287);
            this.panel21.TabIndex = 24;
            // 
            // btnUyglmPlnKydt
            // 
            this.btnUyglmPlnKydt.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnUyglmPlnKydt.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnUyglmPlnKydt.Location = new System.Drawing.Point(0, 0);
            this.btnUyglmPlnKydt.Name = "btnUyglmPlnKydt";
            this.btnUyglmPlnKydt.Size = new System.Drawing.Size(182, 43);
            this.btnUyglmPlnKydt.TabIndex = 32;
            this.btnUyglmPlnKydt.Text = "Kaydet ve İlerle >>";
            this.btnUyglmPlnKydt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUyglmPlnKydt.UseVisualStyleBackColor = true;
            this.btnUyglmPlnKydt.Click += new System.EventHandler(this.btnUyglmPlnKydt_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnUyglmPlanYzdr);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(876, 321);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(123, 287);
            this.panel5.TabIndex = 23;
            // 
            // btnUyglmPlanYzdr
            // 
            this.btnUyglmPlanYzdr.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnUyglmPlanYzdr.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnUyglmPlanYzdr.Image = global::Fumitrack.Properties.Resources.Printer;
            this.btnUyglmPlanYzdr.Location = new System.Drawing.Point(0, 0);
            this.btnUyglmPlanYzdr.Name = "btnUyglmPlanYzdr";
            this.btnUyglmPlanYzdr.Size = new System.Drawing.Size(123, 43);
            this.btnUyglmPlanYzdr.TabIndex = 33;
            this.btnUyglmPlanYzdr.Text = " YAZDIR";
            this.btnUyglmPlanYzdr.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUyglmPlanYzdr.UseVisualStyleBackColor = true;
            this.btnUyglmPlanYzdr.Click += new System.EventHandler(this.btnUyglmPlanYzdr_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.White;
            this.groupBox6.Controls.Add(this.dtgrdUyglmPlan);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.groupBox6.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox6.Location = new System.Drawing.Point(0, 18);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(999, 303);
            this.groupBox6.TabIndex = 22;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Giriş Hatlarını ve Fanları Girin";
            // 
            // dtgrdUyglmPlan
            // 
            this.dtgrdUyglmPlan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgrdUyglmPlan.BackgroundColor = System.Drawing.Color.White;
            this.dtgrdUyglmPlan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdUyglmPlan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AlanAdi2,
            this.GirisHatti,
            this.RH,
            this.Silindir,
            this.Uzunluk,
            this.FanKapasitesi,
            this.FanSayisi,
            this.SilindirSayisi});
            this.dtgrdUyglmPlan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgrdUyglmPlan.Location = new System.Drawing.Point(3, 20);
            this.dtgrdUyglmPlan.Name = "dtgrdUyglmPlan";
            this.dtgrdUyglmPlan.Size = new System.Drawing.Size(993, 280);
            this.dtgrdUyglmPlan.TabIndex = 31;
            this.dtgrdUyglmPlan.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgrdUyglmPlan_CellContentClick);
            this.dtgrdUyglmPlan.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dtgrdUyglmPlan_DataError);
            this.dtgrdUyglmPlan.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DtgrdUyglmPlan_RowsAdded);
            // 
            // AlanAdi2
            // 
            this.AlanAdi2.HeaderText = "*Alan Adı";
            this.AlanAdi2.Name = "AlanAdi2";
            this.AlanAdi2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AlanAdi2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // GirisHatti
            // 
            this.GirisHatti.HeaderText = "Giriş Hattı";
            this.GirisHatti.Name = "GirisHatti";
            // 
            // RH
            // 
            this.RH.HeaderText = "Dağıtımda Kullanılack Malzeme Cinsi";
            this.RH.Name = "RH";
            // 
            // Silindir
            // 
            this.Silindir.HeaderText = "Dağıtım Sis Çapı";
            this.Silindir.Name = "Silindir";
            // 
            // Uzunluk
            // 
            this.Uzunluk.HeaderText = "Uzunluk";
            this.Uzunluk.Name = "Uzunluk";
            // 
            // FanKapasitesi
            // 
            this.FanKapasitesi.HeaderText = "Fan Kapasitesi";
            this.FanKapasitesi.Name = "FanKapasitesi";
            // 
            // FanSayisi
            // 
            this.FanSayisi.HeaderText = "Fan Sayısı";
            this.FanSayisi.Name = "FanSayisi";
            // 
            // SilindirSayisi
            // 
            this.SilindirSayisi.HeaderText = "Silindir Sayısı";
            this.SilindirSayisi.Name = "SilindirSayisi";
            this.SilindirSayisi.ReadOnly = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(999, 18);
            this.panel3.TabIndex = 2;
            // 
            // tabPageMonitoringPlan
            // 
            this.tabPageMonitoringPlan.AutoScroll = true;
            this.tabPageMonitoringPlan.Controls.Add(this.label29);
            this.tabPageMonitoringPlan.Controls.Add(this.panel22);
            this.tabPageMonitoringPlan.Controls.Add(this.panel14);
            this.tabPageMonitoringPlan.Controls.Add(this.groupBox9);
            this.tabPageMonitoringPlan.Controls.Add(this.panel6);
            this.tabPageMonitoringPlan.Location = new System.Drawing.Point(4, 26);
            this.tabPageMonitoringPlan.Name = "tabPageMonitoringPlan";
            this.tabPageMonitoringPlan.Size = new System.Drawing.Size(999, 608);
            this.tabPageMonitoringPlan.TabIndex = 3;
            this.tabPageMonitoringPlan.Text = "İzleme Planı";
            this.tabPageMonitoringPlan.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Dock = System.Windows.Forms.DockStyle.Top;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label29.ForeColor = System.Drawing.Color.DarkRed;
            this.label29.Location = new System.Drawing.Point(0, 344);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(432, 14);
            this.label29.TabIndex = 26;
            this.label29.Text = "Satırı silmek isterseniz satırı seçtikten sonra \"Delete\" tuşuna basınız...";
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.btnIzlmPlnKydt);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel22.Location = new System.Drawing.Point(694, 344);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(182, 264);
            this.panel22.TabIndex = 25;
            // 
            // btnIzlmPlnKydt
            // 
            this.btnIzlmPlnKydt.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnIzlmPlnKydt.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnIzlmPlnKydt.Location = new System.Drawing.Point(0, 0);
            this.btnIzlmPlnKydt.Name = "btnIzlmPlnKydt";
            this.btnIzlmPlnKydt.Size = new System.Drawing.Size(182, 43);
            this.btnIzlmPlnKydt.TabIndex = 35;
            this.btnIzlmPlnKydt.Text = "Kaydet ve İlerle >>";
            this.btnIzlmPlnKydt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnIzlmPlnKydt.UseVisualStyleBackColor = true;
            this.btnIzlmPlnKydt.Click += new System.EventHandler(this.btnIzlmPlnKydt_Click);
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.btnIzlemePlanYazdr);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel14.Location = new System.Drawing.Point(876, 344);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(123, 264);
            this.panel14.TabIndex = 24;
            // 
            // btnIzlemePlanYazdr
            // 
            this.btnIzlemePlanYazdr.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnIzlemePlanYazdr.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnIzlemePlanYazdr.Image = global::Fumitrack.Properties.Resources.Printer;
            this.btnIzlemePlanYazdr.Location = new System.Drawing.Point(0, 0);
            this.btnIzlemePlanYazdr.Name = "btnIzlemePlanYazdr";
            this.btnIzlemePlanYazdr.Size = new System.Drawing.Size(123, 43);
            this.btnIzlemePlanYazdr.TabIndex = 36;
            this.btnIzlemePlanYazdr.Text = " YAZDIR";
            this.btnIzlemePlanYazdr.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnIzlemePlanYazdr.UseVisualStyleBackColor = true;
            this.btnIzlemePlanYazdr.Click += new System.EventHandler(this.btnIzlemePlanYazdr_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.White;
            this.groupBox9.Controls.Add(this.dtgrdIzlemePlan);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox9.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.groupBox9.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox9.Location = new System.Drawing.Point(0, 18);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(999, 326);
            this.groupBox9.TabIndex = 23;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "İlaçlama İzleme Bilgilerini Girin";
            // 
            // dtgrdIzlemePlan
            // 
            this.dtgrdIzlemePlan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgrdIzlemePlan.BackgroundColor = System.Drawing.Color.White;
            this.dtgrdIzlemePlan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdIzlemePlan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AlanAdi4,
            this.IzlemeNoktasiAdi2,
            this.OlcumNoktasi,
            this.HortumMesafesi});
            this.dtgrdIzlemePlan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgrdIzlemePlan.Location = new System.Drawing.Point(3, 20);
            this.dtgrdIzlemePlan.Name = "dtgrdIzlemePlan";
            this.dtgrdIzlemePlan.Size = new System.Drawing.Size(993, 303);
            this.dtgrdIzlemePlan.TabIndex = 34;
            this.dtgrdIzlemePlan.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dtgrdUyglmPlan_DataError);
            this.dtgrdIzlemePlan.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DtgrdIzlemePlan_RowsAdded);
            // 
            // AlanAdi4
            // 
            this.AlanAdi4.HeaderText = "Alan Adı";
            this.AlanAdi4.Name = "AlanAdi4";
            this.AlanAdi4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AlanAdi4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // IzlemeNoktasiAdi2
            // 
            this.IzlemeNoktasiAdi2.HeaderText = "*İzleme Noktası Adı";
            this.IzlemeNoktasiAdi2.Name = "IzlemeNoktasiAdi2";
            // 
            // OlcumNoktasi
            // 
            this.OlcumNoktasi.HeaderText = "Ölçüm Noktası";
            this.OlcumNoktasi.Name = "OlcumNoktasi";
            // 
            // HortumMesafesi
            // 
            this.HortumMesafesi.HeaderText = "Hortum Mesafesi";
            this.HortumMesafesi.Name = "HortumMesafesi";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(999, 18);
            this.panel6.TabIndex = 2;
            // 
            // tabPageMonitoringDataInput
            // 
            this.tabPageMonitoringDataInput.AutoScroll = true;
            this.tabPageMonitoringDataInput.Controls.Add(this.groupBox5);
            this.tabPageMonitoringDataInput.Controls.Add(this.panel20);
            this.tabPageMonitoringDataInput.Controls.Add(this.panel26);
            this.tabPageMonitoringDataInput.Controls.Add(this.groupBox12);
            this.tabPageMonitoringDataInput.Controls.Add(this.panel10);
            this.tabPageMonitoringDataInput.Controls.Add(this.panel9);
            this.tabPageMonitoringDataInput.Location = new System.Drawing.Point(4, 26);
            this.tabPageMonitoringDataInput.Name = "tabPageMonitoringDataInput";
            this.tabPageMonitoringDataInput.Size = new System.Drawing.Size(999, 608);
            this.tabPageMonitoringDataInput.TabIndex = 5;
            this.tabPageMonitoringDataInput.Text = "İzleme Veri Girdileri";
            this.tabPageMonitoringDataInput.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.White;
            this.groupBox5.Controls.Add(this.txtSonucIzlemeVeriGirdi);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.groupBox5.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox5.Location = new System.Drawing.Point(0, 449);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(753, 64);
            this.groupBox5.TabIndex = 28;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Sonuçlar";
            this.groupBox5.Visible = false;
            // 
            // txtSonucIzlemeVeriGirdi
            // 
            this.txtSonucIzlemeVeriGirdi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSonucIzlemeVeriGirdi.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.txtSonucIzlemeVeriGirdi.ForeColor = System.Drawing.Color.Red;
            this.txtSonucIzlemeVeriGirdi.Location = new System.Drawing.Point(3, 20);
            this.txtSonucIzlemeVeriGirdi.Multiline = true;
            this.txtSonucIzlemeVeriGirdi.Name = "txtSonucIzlemeVeriGirdi";
            this.txtSonucIzlemeVeriGirdi.ReadOnly = true;
            this.txtSonucIzlemeVeriGirdi.Size = new System.Drawing.Size(747, 41);
            this.txtSonucIzlemeVeriGirdi.TabIndex = 3;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.btnIzlemeVeriGrafk);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel20.Location = new System.Drawing.Point(753, 449);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(123, 159);
            this.panel20.TabIndex = 27;
            // 
            // btnIzlemeVeriGrafk
            // 
            this.btnIzlemeVeriGrafk.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnIzlemeVeriGrafk.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnIzlemeVeriGrafk.Image = global::Fumitrack.Properties.Resources.graph;
            this.btnIzlemeVeriGrafk.Location = new System.Drawing.Point(0, 0);
            this.btnIzlemeVeriGrafk.Name = "btnIzlemeVeriGrafk";
            this.btnIzlemeVeriGrafk.Size = new System.Drawing.Size(123, 43);
            this.btnIzlemeVeriGrafk.TabIndex = 42;
            this.btnIzlemeVeriGrafk.Text = " GRAFİK";
            this.btnIzlemeVeriGrafk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnIzlemeVeriGrafk.UseVisualStyleBackColor = true;
            this.btnIzlemeVeriGrafk.Click += new System.EventHandler(this.btnIzlemeVeriGrafk_Click);
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.btnIzlemeVeriGirdiYazdr);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel26.Location = new System.Drawing.Point(876, 449);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(123, 159);
            this.panel26.TabIndex = 26;
            // 
            // btnIzlemeVeriGirdiYazdr
            // 
            this.btnIzlemeVeriGirdiYazdr.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnIzlemeVeriGirdiYazdr.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnIzlemeVeriGirdiYazdr.Image = global::Fumitrack.Properties.Resources.Printer;
            this.btnIzlemeVeriGirdiYazdr.Location = new System.Drawing.Point(0, 0);
            this.btnIzlemeVeriGirdiYazdr.Name = "btnIzlemeVeriGirdiYazdr";
            this.btnIzlemeVeriGirdiYazdr.Size = new System.Drawing.Size(123, 43);
            this.btnIzlemeVeriGirdiYazdr.TabIndex = 43;
            this.btnIzlemeVeriGirdiYazdr.Text = " YAZDIR";
            this.btnIzlemeVeriGirdiYazdr.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnIzlemeVeriGirdiYazdr.UseVisualStyleBackColor = true;
            this.btnIzlemeVeriGirdiYazdr.Click += new System.EventHandler(this.btnIzlemeVeriGirdiYazdr_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.White;
            this.groupBox12.Controls.Add(this.panel17);
            this.groupBox12.Controls.Add(this.panel11);
            this.groupBox12.Controls.Add(this.groupBox13);
            this.groupBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox12.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.groupBox12.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox12.Location = new System.Drawing.Point(0, 123);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(999, 326);
            this.groupBox12.TabIndex = 24;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Veri Girişini İzleme";
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.dtgrdIzlemeVeriGrdi);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(149, 20);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(762, 303);
            this.panel17.TabIndex = 2;
            // 
            // dtgrdIzlemeVeriGrdi
            // 
            this.dtgrdIzlemeVeriGrdi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgrdIzlemeVeriGrdi.BackgroundColor = System.Drawing.Color.White;
            this.dtgrdIzlemeVeriGrdi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdIzlemeVeriGrdi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AlanAdi3,
            this.Tarih,
            this.Zaman,
            this.Sicaklik2,
            this.IzlemeNoktasiAdi,
            this.IzlemeNoktasiID,
            this.Konsantrasyon,
            this.HatOrtKonsantrasyon,
            this.HLT,
            this.Aciklama2});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 10.5F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Firebrick;
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgrdIzlemeVeriGrdi.DefaultCellStyle = dataGridViewCellStyle3;
            this.dtgrdIzlemeVeriGrdi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgrdIzlemeVeriGrdi.Location = new System.Drawing.Point(0, 0);
            this.dtgrdIzlemeVeriGrdi.Name = "dtgrdIzlemeVeriGrdi";
            this.dtgrdIzlemeVeriGrdi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdIzlemeVeriGrdi.Size = new System.Drawing.Size(762, 303);
            this.dtgrdIzlemeVeriGrdi.TabIndex = 40;
            this.dtgrdIzlemeVeriGrdi.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DtgrdIzlemeVeriGrdi_EditingControlShowing);
            // 
            // AlanAdi3
            // 
            this.AlanAdi3.HeaderText = "Alan Adı";
            this.AlanAdi3.Name = "AlanAdi3";
            this.AlanAdi3.ReadOnly = true;
            // 
            // Tarih
            // 
            this.Tarih.FillWeight = 80F;
            this.Tarih.HeaderText = "Tarih";
            this.Tarih.Name = "Tarih";
            this.Tarih.ReadOnly = true;
            // 
            // Zaman
            // 
            dataGridViewCellStyle2.NullValue = null;
            this.Zaman.DefaultCellStyle = dataGridViewCellStyle2;
            this.Zaman.FillWeight = 90F;
            this.Zaman.HeaderText = "*Saat Verisi (Ör:09:35)";
            this.Zaman.Name = "Zaman";
            // 
            // Sicaklik2
            // 
            this.Sicaklik2.HeaderText = "Sıcaklık";
            this.Sicaklik2.Name = "Sicaklik2";
            // 
            // IzlemeNoktasiAdi
            // 
            this.IzlemeNoktasiAdi.HeaderText = "İzleme Noktası Adı";
            this.IzlemeNoktasiAdi.Name = "IzlemeNoktasiAdi";
            this.IzlemeNoktasiAdi.ReadOnly = true;
            // 
            // IzlemeNoktasiID
            // 
            this.IzlemeNoktasiID.FillWeight = 5F;
            this.IzlemeNoktasiID.HeaderText = "IzlemeNoktasiID";
            this.IzlemeNoktasiID.Name = "IzlemeNoktasiID";
            this.IzlemeNoktasiID.Visible = false;
            // 
            // Konsantrasyon
            // 
            this.Konsantrasyon.FillWeight = 120F;
            this.Konsantrasyon.HeaderText = "*Ölçülen Konsantrasyon";
            this.Konsantrasyon.Name = "Konsantrasyon";
            // 
            // HatOrtKonsantrasyon
            // 
            this.HatOrtKonsantrasyon.HeaderText = "Ortalama Konsantrasyon";
            this.HatOrtKonsantrasyon.Name = "HatOrtKonsantrasyon";
            this.HatOrtKonsantrasyon.ReadOnly = true;
            // 
            // HLT
            // 
            this.HLT.FillWeight = 70F;
            this.HLT.HeaderText = "HLT";
            this.HLT.Name = "HLT";
            this.HLT.ReadOnly = true;
            // 
            // Aciklama2
            // 
            this.Aciklama2.FillWeight = 200F;
            this.Aciklama2.HeaderText = "Açıklama";
            this.Aciklama2.Name = "Aciklama2";
            this.Aciklama2.ReadOnly = true;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.btnVeriHesapla);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(911, 20);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(85, 303);
            this.panel11.TabIndex = 1;
            // 
            // btnVeriHesapla
            // 
            this.btnVeriHesapla.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnVeriHesapla.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.btnVeriHesapla.ForeColor = System.Drawing.Color.Black;
            this.btnVeriHesapla.Location = new System.Drawing.Point(0, 0);
            this.btnVeriHesapla.Name = "btnVeriHesapla";
            this.btnVeriHesapla.Size = new System.Drawing.Size(85, 39);
            this.btnVeriHesapla.TabIndex = 41;
            this.btnVeriHesapla.Text = "HESAPLA";
            this.btnVeriHesapla.UseVisualStyleBackColor = true;
            this.btnVeriHesapla.Click += new System.EventHandler(this.btnVeriHesapla_Click);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.listBoxVeriFiltre);
            this.groupBox13.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox13.Font = new System.Drawing.Font("Tahoma", 10F);
            this.groupBox13.Location = new System.Drawing.Point(3, 20);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(146, 303);
            this.groupBox13.TabIndex = 0;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Veri Filtresi";
            // 
            // listBoxVeriFiltre
            // 
            this.listBoxVeriFiltre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxVeriFiltre.FormattingEnabled = true;
            this.listBoxVeriFiltre.ItemHeight = 16;
            this.listBoxVeriFiltre.Location = new System.Drawing.Point(3, 20);
            this.listBoxVeriFiltre.Name = "listBoxVeriFiltre";
            this.listBoxVeriFiltre.Size = new System.Drawing.Size(140, 280);
            this.listBoxVeriFiltre.TabIndex = 39;
            this.listBoxVeriFiltre.SelectedIndexChanged += new System.EventHandler(this.listBoxVeriFiltre_SelectedIndexChanged);
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.pictureBox2);
            this.panel10.Controls.Add(this.dtBasSaat);
            this.panel10.Controls.Add(this.dtBasTarih);
            this.panel10.Controls.Add(this.label21);
            this.panel10.Controls.Add(this.label20);
            this.panel10.Controls.Add(this.label19);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 18);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(999, 105);
            this.panel10.TabIndex = 5;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::Fumitrack.Properties.Resources.line;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox2.Location = new System.Drawing.Point(0, 95);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(999, 10);
            this.pictureBox2.TabIndex = 2237;
            this.pictureBox2.TabStop = false;
            // 
            // dtBasSaat
            // 
            this.dtBasSaat.CustomFormat = "HH:mm";
            this.dtBasSaat.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtBasSaat.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtBasSaat.Location = new System.Drawing.Point(81, 68);
            this.dtBasSaat.Name = "dtBasSaat";
            this.dtBasSaat.ShowUpDown = true;
            this.dtBasSaat.Size = new System.Drawing.Size(138, 22);
            this.dtBasSaat.TabIndex = 38;
            this.dtBasSaat.Value = new System.DateTime(2019, 11, 6, 0, 0, 0, 0);
            // 
            // dtBasTarih
            // 
            this.dtBasTarih.CustomFormat = "yyyy-MM-dd";
            this.dtBasTarih.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtBasTarih.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtBasTarih.Location = new System.Drawing.Point(81, 35);
            this.dtBasTarih.Name = "dtBasTarih";
            this.dtBasTarih.Size = new System.Drawing.Size(138, 22);
            this.dtBasTarih.TabIndex = 37;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.label21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label21.Location = new System.Drawing.Point(31, 72);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 17);
            this.label21.TabIndex = 1;
            this.label21.Text = "Saat";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.label20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label20.Location = new System.Drawing.Point(31, 39);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(38, 17);
            this.label20.TabIndex = 1;
            this.label20.Text = "Tarih";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Underline);
            this.label19.ForeColor = System.Drawing.Color.Firebrick;
            this.label19.Location = new System.Drawing.Point(60, 8);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(111, 17);
            this.label19.TabIndex = 1;
            this.label19.Text = "Başlangıç Zamanı";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(999, 18);
            this.panel9.TabIndex = 4;
            // 
            // tabPageGraph
            // 
            this.tabPageGraph.AutoScroll = true;
            this.tabPageGraph.Controls.Add(this.panel18);
            this.tabPageGraph.Controls.Add(this.panel16);
            this.tabPageGraph.Controls.Add(this.groupBox14);
            this.tabPageGraph.Controls.Add(this.panel15);
            this.tabPageGraph.Location = new System.Drawing.Point(4, 26);
            this.tabPageGraph.Name = "tabPageGraph";
            this.tabPageGraph.Size = new System.Drawing.Size(999, 608);
            this.tabPageGraph.TabIndex = 7;
            this.tabPageGraph.Text = "Fumigasyon Konsantrasyon Zaman Grafiği";
            this.tabPageGraph.UseVisualStyleBackColor = true;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.chartZamanGrafk);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(200, 18);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(799, 539);
            this.panel18.TabIndex = 10;
            // 
            // chartZamanGrafk
            // 
            chartArea1.AxisX.Interval = 1D;
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea1.AxisX.Maximum = 24D;
            chartArea1.AxisX.Minimum = 0D;
            chartArea1.AxisX.Title = "Zaman";
            chartArea1.AxisX.TitleFont = new System.Drawing.Font("Tahoma", 9F);
            chartArea1.AxisX.TitleForeColor = System.Drawing.Color.RoyalBlue;
            chartArea1.AxisY.Interval = 1D;
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea1.AxisY.Maximum = 25D;
            chartArea1.AxisY.Minimum = 0D;
            chartArea1.AxisY.Title = "Konsantrasyon";
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("Tahoma", 9F);
            chartArea1.AxisY.TitleForeColor = System.Drawing.Color.RoyalBlue;
            chartArea1.BackColor = System.Drawing.Color.White;
            chartArea1.Name = "ChartArea1";
            this.chartZamanGrafk.ChartAreas.Add(chartArea1);
            this.chartZamanGrafk.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            legend1.TitleForeColor = System.Drawing.Color.Empty;
            this.chartZamanGrafk.Legends.Add(legend1);
            this.chartZamanGrafk.Location = new System.Drawing.Point(0, 0);
            this.chartZamanGrafk.Name = "chartZamanGrafk";
            this.chartZamanGrafk.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series1.BorderWidth = 2;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Color = System.Drawing.Color.Red;
            series1.Font = new System.Drawing.Font("Tahoma", 9F);
            series1.Legend = "Legend1";
            series1.LegendText = "Alt Sınır";
            series1.Name = "Alt Sınır";
            dataPoint2.IsValueShownAsLabel = false;
            dataPoint2.IsVisibleInLegend = true;
            series1.Points.Add(dataPoint1);
            series1.Points.Add(dataPoint2);
            series1.Points.Add(dataPoint3);
            series1.Points.Add(dataPoint4);
            series1.Points.Add(dataPoint5);
            series1.Points.Add(dataPoint6);
            series1.Points.Add(dataPoint7);
            series1.YValuesPerPoint = 4;
            series2.BorderWidth = 3;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Color = System.Drawing.Color.Green;
            series2.Font = new System.Drawing.Font("Tahoma", 10F);
            series2.Label = "#VAL";
            series2.Legend = "Legend1";
            series2.LegendText = "Gerçekleşen";
            series2.Name = "Gerçekleşen";
            series2.Points.Add(dataPoint8);
            series2.Points.Add(dataPoint9);
            series2.Points.Add(dataPoint10);
            series2.Points.Add(dataPoint11);
            series2.Points.Add(dataPoint12);
            series2.Points.Add(dataPoint13);
            series2.Points.Add(dataPoint14);
            this.chartZamanGrafk.Series.Add(series1);
            this.chartZamanGrafk.Series.Add(series2);
            this.chartZamanGrafk.Size = new System.Drawing.Size(799, 539);
            this.chartZamanGrafk.TabIndex = 0;
            this.chartZamanGrafk.Text = "chart1";
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.lblGrfkIlaveMesaj);
            this.panel16.Controls.Add(this.lblGrfkBaslik);
            this.panel16.Controls.Add(this.btnZamanGrafikYazdr);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel16.Location = new System.Drawing.Point(200, 557);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(799, 51);
            this.panel16.TabIndex = 9;
            // 
            // lblGrfkIlaveMesaj
            // 
            this.lblGrfkIlaveMesaj.AutoSize = true;
            this.lblGrfkIlaveMesaj.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblGrfkIlaveMesaj.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblGrfkIlaveMesaj.Location = new System.Drawing.Point(0, 34);
            this.lblGrfkIlaveMesaj.Name = "lblGrfkIlaveMesaj";
            this.lblGrfkIlaveMesaj.Size = new System.Drawing.Size(0, 17);
            this.lblGrfkIlaveMesaj.TabIndex = 5;
            // 
            // lblGrfkBaslik
            // 
            this.lblGrfkBaslik.AutoSize = true;
            this.lblGrfkBaslik.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblGrfkBaslik.Location = new System.Drawing.Point(0, 0);
            this.lblGrfkBaslik.Name = "lblGrfkBaslik";
            this.lblGrfkBaslik.Size = new System.Drawing.Size(0, 17);
            this.lblGrfkBaslik.TabIndex = 5;
            // 
            // btnZamanGrafikYazdr
            // 
            this.btnZamanGrafikYazdr.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnZamanGrafikYazdr.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnZamanGrafikYazdr.Image = global::Fumitrack.Properties.Resources.Printer;
            this.btnZamanGrafikYazdr.Location = new System.Drawing.Point(676, 0);
            this.btnZamanGrafikYazdr.Name = "btnZamanGrafikYazdr";
            this.btnZamanGrafikYazdr.Size = new System.Drawing.Size(123, 51);
            this.btnZamanGrafikYazdr.TabIndex = 45;
            this.btnZamanGrafikYazdr.Text = " YAZDIR";
            this.btnZamanGrafikYazdr.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnZamanGrafikYazdr.UseVisualStyleBackColor = true;
            this.btnZamanGrafikYazdr.Click += new System.EventHandler(this.btnZamanGrafikYazdr_Click);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.listBoxGrafkFiltre);
            this.groupBox14.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox14.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.groupBox14.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox14.Location = new System.Drawing.Point(0, 18);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(200, 590);
            this.groupBox14.TabIndex = 8;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Veri Filtresi";
            // 
            // listBoxGrafkFiltre
            // 
            this.listBoxGrafkFiltre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxGrafkFiltre.FormattingEnabled = true;
            this.listBoxGrafkFiltre.ItemHeight = 17;
            this.listBoxGrafkFiltre.Location = new System.Drawing.Point(3, 20);
            this.listBoxGrafkFiltre.Name = "listBoxGrafkFiltre";
            this.listBoxGrafkFiltre.Size = new System.Drawing.Size(194, 567);
            this.listBoxGrafkFiltre.TabIndex = 44;
            this.listBoxGrafkFiltre.SelectedIndexChanged += new System.EventHandler(this.listBoxGrafkFiltre_SelectedIndexChanged);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(999, 18);
            this.panel15.TabIndex = 7;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Text = "Baskı önizleme";
            this.printPreviewDialog1.Visible = false;
            // 
            // FumitrackMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1024, 611);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FumitrackMainForm";
            this.Text = "FumiTrack";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FumitrackMainForm_FormClosing);
            this.Load += new System.EventHandler(this.FumitrackMainForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FumitrackMainForm_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPageHistory.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdGecmisUyglm)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabPageCustomerInfo.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPageFumigationDosagePlan.ResumeLayout(false);
            this.panelHedefZararli.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdHedefZararli)).EndInit();
            this.panel24.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdYapiAlanBlg)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPageIntroductionPlan.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdUyglmPlan)).EndInit();
            this.tabPageMonitoringPlan.ResumeLayout(false);
            this.tabPageMonitoringPlan.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdIzlemePlan)).EndInit();
            this.tabPageMonitoringDataInput.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdIzlemeVeriGrdi)).EndInit();
            this.panel11.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabPageGraph.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartZamanGrafk)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LogoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageCustomerInfo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MaskedTextBox txtTelefon;
        private System.Windows.Forms.TextBox txtAdres;
        private System.Windows.Forms.TextBox txtAciklama;
        private System.Windows.Forms.TextBox txtEposta;
        private System.Windows.Forms.TextBox txtSehir;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage tabPageFumigationDosagePlan;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cmbEmtia;
        private System.Windows.Forms.ComboBox cmbFumiSekli;
        private System.Windows.Forms.ComboBox cmbDozaj;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbHedefZararli;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.DateTimePicker dtIlaclamaTrh;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabPage tabPageIntroductionPlan;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dtgrdUyglmPlan;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabPage tabPageMonitoringPlan;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView dtgrdIzlemePlan;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TabPage tabPageMonitoringDataInput;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.DataGridView dtgrdIzlemeVeriGrdi;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.ListBox listBoxVeriFiltre;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.DateTimePicker dtBasSaat;
        public System.Windows.Forms.DateTimePicker dtBasTarih;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TabPage tabPageGraph;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.ListBox listBoxGrafkFiltre;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.ToolStripMenuItem NewtoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReporttoolStripMenuItem;
        private System.Windows.Forms.ComboBox cmbBsncTip;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFumLisans1;
        private System.Windows.Forms.Button btnVeriHesapla;
        private System.Windows.Forms.TabPage tabPageHistory;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button btnIzlemeVeriGrafk;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView dtgrdGecmisUyglm;
        private System.Windows.Forms.Button btnIzlemePlanYazdr;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button btnFirmaKydt;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Button btnUyglmPlnKydt;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Button btnIzlmPlnKydt;
        private System.Windows.Forms.ComboBox cmbFirmaAdi;
        private System.Windows.Forms.TextBox txtFumAdSoyad2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtFumAdSoyad1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtYapildigiYer;
        private System.Windows.Forms.TextBox txtKayitNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtFumLisans2;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtKonteynerNo;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem normalRaporToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem avustralyaRaporToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirmaAdi;
        private System.Windows.Forms.DataGridViewTextBoxColumn IlaclamaTarihi;
        private System.Windows.Forms.DataGridViewTextBoxColumn DozajPlanIDGecmis;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnUyglmPlanYzdr;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.TextBox txtAmbalajSekli;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtAmbalajAdedi;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtPartiNo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtUrunMiktari;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtUrunCinsi;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txtNeMaksatlaY;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ToolStripMenuItem turkceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingilizceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem avustralyaRapor2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem turkce2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingilizce2ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem avustralyaRapor3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem turkce3ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ingilizce3ToolStripMenuItem2;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtSunuc;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button btnDzjPlanKydt;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Button btnDzjPlanYazdr;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.DataGridView dtgrdYapiAlanBlg;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnDzjPlaniHesapla;
        private System.Windows.Forms.DataGridView dtgrdHedefZararli;
        private System.Windows.Forms.Button btnHedefZararli;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Secim;
        private System.Windows.Forms.DataGridViewTextBoxColumn HedefZararlilar;
        private System.Windows.Forms.Panel panelHedefZararli;
        private System.Windows.Forms.Button btnHdfZararlIptal;
        private System.Windows.Forms.Button btnHdfZararlTamam;
        private System.Windows.Forms.ToolStripMenuItem avustralyaRapor4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem turkce4ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem ingilizce4ToolStripMenuItem3;
        private System.Windows.Forms.DataGridViewComboBoxColumn AlanAdi4;
        private System.Windows.Forms.DataGridViewTextBoxColumn IzlemeNoktasiAdi2;
        private System.Windows.Forms.DataGridViewTextBoxColumn OlcumNoktasi;
        private System.Windows.Forms.DataGridViewTextBoxColumn HortumMesafesi;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Button btnIzlemeVeriGirdiYazdr;
        private System.Windows.Forms.Button btnZamanGrafikYazdr;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.ToolStripMenuItem dilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem türkçeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingilizceToolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemRapor;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemVeriGor;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtSonucIzlemeVeriGirdi;
        private System.Windows.Forms.Label lblGrfkIlaveMesaj;
        private System.Windows.Forms.Label lblGrfkBaslik;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlanAdi;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sicaklik;
        private System.Windows.Forms.DataGridViewTextBoxColumn EstHLT;
        private System.Windows.Forms.DataGridViewComboBoxColumn MaruzKalmaSuresi;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlanHacmi;
        private System.Windows.Forms.DataGridViewTextBoxColumn GerekliIlac;
        private System.Windows.Forms.DataGridViewTextBoxColumn KullanilacakSilindirSayisi;
        private System.Windows.Forms.DataGridViewTextBoxColumn HedefCT;
        private System.Windows.Forms.DataGridViewComboBoxColumn AlanAdi2;
        private System.Windows.Forms.DataGridViewTextBoxColumn GirisHatti;
        private System.Windows.Forms.DataGridViewTextBoxColumn RH;
        private System.Windows.Forms.DataGridViewTextBoxColumn Silindir;
        private System.Windows.Forms.DataGridViewTextBoxColumn Uzunluk;
        private System.Windows.Forms.DataGridViewTextBoxColumn FanKapasitesi;
        private System.Windows.Forms.DataGridViewTextBoxColumn FanSayisi;
        private System.Windows.Forms.DataGridViewTextBoxColumn SilindirSayisi;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSil;
        public System.Windows.Forms.DataVisualization.Charting.Chart chartZamanGrafk;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlanAdi3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tarih;
        private System.Windows.Forms.DataGridViewTextBoxColumn Zaman;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sicaklik2;
        private System.Windows.Forms.DataGridViewTextBoxColumn IzlemeNoktasiAdi;
        private System.Windows.Forms.DataGridViewTextBoxColumn IzlemeNoktasiID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Konsantrasyon;
        private System.Windows.Forms.DataGridViewTextBoxColumn HatOrtKonsantrasyon;
        private System.Windows.Forms.DataGridViewTextBoxColumn HLT;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aciklama2;
    }
}