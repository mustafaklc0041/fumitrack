﻿using Fumitrack.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fumitrack.GUI
{
    public partial class FumitrackMainForm : Form
    {
        public FumitrackMainForm()
        {
            InitializeComponent();
            Classes.DatabaseCommands.DataGridColor(dtgrdGecmisUyglm);
            Classes.DatabaseCommands.DataGridColor(dtgrdIzlemePlan);
            Classes.DatabaseCommands.DataGridColor(dtgrdIzlemeVeriGrdi);
            Classes.DatabaseCommands.DataGridColor(dtgrdUyglmPlan);
            Classes.DatabaseCommands.DataGridColor(dtgrdYapiAlanBlg);
            Classes.DatabaseCommands.DataGridColor(dtgrdHedefZararli);
            Classes.DatabaseCommands.SetLanguage(this);
            FilldtgrdHedefZararli();
            tabControl1.SelectedTab = tabPageCustomerInfo;
        }
        Bitmap bitmap;

        #region ANAFORM

        private void türkçeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default.Dil = "Tr";
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.Reload();
                türkçeToolStripMenuItem.Checked = true;
                ingilizceToolStripMenuItem1.Checked = false;
                Classes.DatabaseCommands.SetLanguage(this);
                FilldtgrdHedefZararli();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "türkçeToolStripMenuItem_Click: " + hata.Message);
            }
        }

        private void ingilizceToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default.Dil = "En";
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.Reload();
                ingilizceToolStripMenuItem1.Checked = true;
                türkçeToolStripMenuItem.Checked = false;
                Classes.DatabaseCommands.SetLanguage(this);
                FilldtgrdHedefZararli();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "ingilizceToolStripMenuItem1_Click: " + hata.Message);
            }
        }

        private void LogoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string message = Localization.LogOut;
                if (DialogResult.Yes == MessageBox.Show(this, message, this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    foreach (Form form in Application.OpenForms)
                    {
                        if (form.GetType() == typeof(LoginPage))
                        {
                            this.Dispose();
                            form.Activate();
                            form.Show();
                            form.BringToFront();
                            return;
                        }
                    }
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "LogoutToolStripMenuItem_Click: " + hata.Message);
            }
        }

        private void dtgrdUyglmPlan_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
        }

        private void NewtoolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string message = Localization.QNewProject;
                if (DialogResult.Yes== MessageBox.Show(this, message, this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    FumitrackMainForm f = new FumitrackMainForm();
                    this.Dispose();
                    f.Show();
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "NewtoolStripMenuItem_Click: " + hata.Message);
            }
        }

        private void normalRaporToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Report.Report report = new Report.Report();
            try
            {
                report.lblTarih.Text = dtIlaclamaTrh.Text;
                report.lblKayitNo.Text = txtKayitNo.Text;
                report.lblYptrnFirma.Text = cmbFirmaAdi.Text;
                report.lblYpldYer.Text = txtYapildigiYer.Text;
                report.lblFumiSekli.Text = cmbFumiSekli.Text;
                report.lblKonteynerNo.Text = txtKonteynerNo.Text;
                report.lblUrunCinsi.Text = txtUrunCinsi.Text;
                report.lblKontrolNo.Text = txtPartiNo.Text;
                report.lblAmbljAdet.Text = txtAmbalajAdedi.Text;
                report.lblUrunMiktar.Text = txtUrunMiktari.Text;
                report.lblAmbljSekli.Text = txtAmbalajSekli.Text;
                report.lblIlaclananHcm.Text = Math.Round(TopAlanHacmi,1).ToString();
                report.lblFumiScklk.Text = dtgrdYapiAlanBlg.Rows[0].Cells["Sicaklik"].Value.ToString();
                report.lblFumiDoz.Text = cmbDozaj.Text;
                report.lblFumiMktr.Text = txtSunuc.Text.Split('|')[1];
                report.lblKapanGunSaat.Text = KapanisTarih + " - " + KapanisSaat;
                report.lblAclsGunSaat.Text = dtBasTarih.Text + " - " + dtBasSaat.Text;
                report.lblNeMaksatlaY.Text = txtNeMaksatlaY.Text;
                report.lblZararliAd.Text = btnHedefZararli.Text;
                report.lblFumiYpnAdSoyad1.Text = txtFumAdSoyad1.Text+"-"+txtFumLisans1.Text;
                report.lblFumiYpnAdSoyad2.Text = txtFumAdSoyad2.Text + "-" + txtFumLisans2.Text;
                report.lblEmtia.Text = cmbEmtia.Text;
                report.lblBasincTipi.Text = cmbBsncTip.Text;

            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "normalRaporToolStripMenuItem_Click: " + hata.Message);
            }
            finally
            {
                report.Show();
            }
        }

        void FillcmbFirmaAdi()
        {
            try
            {
                DataTable Firmalar = Classes.DatabaseCommands.GetUygulamaYapilanFirmalar("");

                if (Firmalar != null && Firmalar.Rows.Count > 0)
                {
                    cmbFirmaAdi.DataSource = Firmalar;
                    cmbFirmaAdi.DisplayMember = "FirmaAdi";
                    cmbFirmaAdi.ValueMember = "ID";
                    cmbFirmaAdi.SelectedIndex = cmbFirmaAdi.Items.Count - 1;
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "FillcmbFirmaAdi: " + hata.Message);
            }
        }

        void FillGecmisUygulamalar()
        {
            try
            {
                DataTable GecmisUygulamalar = Classes.DatabaseCommands.GetGecmisUygulamalar();

                if (GecmisUygulamalar != null && GecmisUygulamalar.Rows.Count > 0)
                {
                    dtgrdGecmisUyglm.Rows.Clear();
                    foreach (DataRow row in GecmisUygulamalar.Rows)
                    {
                        dtgrdGecmisUyglm.Rows.Add(row["FirmaAdi"].ToString(), 
                            row["IlaclamaTarih"].ToString(), row["ID"].ToString());
                    }
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "FillGecmisUygulamalar: " + hata.Message);
            }
        }

        //DataTable t = new DataTable();
        //void ChildControls(Control.ControlCollection controls)
        //{
        //    try
        //    {
        //        foreach (Control childControl in controls)
        //        {
        //            try
        //            {
        //                if (childControl.AccessibilityObject.Role.ToString().Contains("ComboBox"))
        //                {
        //                    ComboBox cmb = (ComboBox)childControl;
        //                    foreach (string itm in cmb.Items)
        //                    {
        //                        DataRow row2 = t.NewRow();
        //                        row2["FormName"] = cmb.FindForm().Name;
        //                        row2["ObjectName"] = cmb.Name;
        //                        row2["Turkish"] = itm;
        //                        row2["English"] = "";
        //                        if (!string.IsNullOrEmpty(cmb.Name))
        //                            t.Rows.Add(row2);
        //                    }
        //                }
        //            }
        //            catch { }

        //            try
        //            {
        //                if (childControl.AccessibilityObject.Name.Contains("DataGridView"))
        //                {
        //                    DataGridView dtg = (DataGridView)childControl;
        //                    foreach (DataGridViewColumn c in dtg.Columns)
        //                    {
        //                        DataRow row2 = t.NewRow();
        //                        row2["FormName"] = dtg.FindForm().Name;
        //                        row2["ObjectName"] = c.Name;
        //                        row2["Turkish"] = c.HeaderText;
        //                        row2["English"] = "";
        //                        if (!string.IsNullOrEmpty(c.Name))
        //                            t.Rows.Add(row2);
        //                    }
        //                }
        //            }
        //            catch { }

        //            try
        //            {
        //                if (childControl.AccessibilityObject.Name.Contains("chart"))
        //                {
        //                    System.Windows.Forms.DataVisualization.Charting.Chart chrt = (System.Windows.Forms.DataVisualization.Charting.Chart)childControl;
        //                    foreach (System.Windows.Forms.DataVisualization.Charting.Series c in chrt.Series)
        //                    {
        //                        DataRow row2 = t.NewRow();
        //                        row2["FormName"] = chrt.FindForm().Name;
        //                        row2["ObjectName"] = c.Name;
        //                        row2["Turkish"] = c.Name;
        //                        row2["English"] = "";
        //                        if (!string.IsNullOrEmpty(c.Name))
        //                            t.Rows.Add(row2);
        //                    }
        //                    foreach (System.Windows.Forms.DataVisualization.Charting.ChartArea c in chrt.ChartAreas)
        //                    {
        //                        DataRow row2 = t.NewRow();
        //                        row2["FormName"] = chrt.FindForm().Name;
        //                        row2["ObjectName"] = c.AxisX.Title;
        //                        row2["Turkish"] = c.AxisX.Title;
        //                        row2["English"] = "";
        //                        if (!string.IsNullOrEmpty(c.AxisX.Title))
        //                            t.Rows.Add(row2);

        //                        DataRow row3 = t.NewRow();
        //                        row3["FormName"] = chrt.FindForm().Name;
        //                        row3["ObjectName"] = c.AxisY.Title;
        //                        row3["Turkish"] = c.AxisY.Title;
        //                        row3["English"] = "";
        //                        if (!string.IsNullOrEmpty(c.AxisY.Title))
        //                            t.Rows.Add(row3);
        //                    }
        //                }
        //            }
        //            catch { }

        //            try
        //            {
        //                if (childControl.AccessibilityObject.Name.Contains("menuStrip"))
        //                {
        //                    MenuStrip mstrp = (MenuStrip)childControl;

        //                    try
        //                    {
        //                        GetMenuItems(mstrp.Items);
        //                        void GetMenuItems(ToolStripItemCollection menuItems)
        //                        {
        //                            try
        //                            {
        //                                foreach (ToolStripMenuItem itm in menuItems)
        //                                {
        //                                    DataRow row2 = t.NewRow();
        //                                    row2["FormName"] = mstrp.FindForm().Name;
        //                                    row2["ObjectName"] = itm.Name;
        //                                    row2["Turkish"] = itm.Text;
        //                                    row2["English"] = "";
        //                                    if (!string.IsNullOrEmpty(itm.Name))
        //                                        t.Rows.Add(row2);

        //                                    if (itm.HasDropDown)
        //                                    {
        //                                        GetMenuItems(itm.DropDownItems);
        //                                    }
        //                                }
        //                            }
        //                            catch (Exception)
        //                            {
        //                            }
        //                        }
        //                    }
        //                    catch (Exception)
        //                    {
        //                    }
        //                }
        //            }
        //            catch (Exception)
        //            {
        //            }
        //            DataRow row = t.NewRow();
        //            row["FormName"] = childControl.FindForm().Name;
        //            row["ObjectName"] = childControl.Name;
        //            row["Turkish"] = childControl.Text;
        //            row["English"] = "";

        //            if (!string.IsNullOrEmpty(childControl.Name))
        //                t.Rows.Add(row);

        //            ChildControls(childControl.Controls);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}


        private void FumitrackMainForm_Load(object sender, EventArgs e)
        {
            try
            {
                //t.Columns.Add("FormName");
                //t.Columns.Add("ObjectName");
                //t.Columns.Add("Turkish");
                //t.Columns.Add("English");
                //ChildControls(this.Controls);
                //GUI.AdminPanelForm apf = new AdminPanelForm();
                //ChildControls(apf.Controls);
                //GUI.LoginPage lp = new LoginPage();
                //ChildControls(lp.Controls);

                //for (int i = 0; i < t.Rows.Count; i++)
                //{
                //    string sql = " insert into Language (FormName, ObjectName, Turkish, English) " +
                //                     " values ('" + t.Rows[i].ItemArray[0].ToString() + "', '" + t.Rows[i].ItemArray[1].ToString() + "'," +
                //                     "'" + t.Rows[i].ItemArray[2].ToString() + "', '" + t.Rows[i].ItemArray[3].ToString() + "')";
                //    Classes.DataAccess.ExecuteSQL(sql, Fumitrack.Properties.Settings.Default.LocalConnectionString);
                //}


                FillcmbFirmaAdi();
                dtBasSaat.Value = DateTime.Now.ToLocalTime();
                dtgrdIzlemeVeriGrdi.AutoGenerateColumns = false;


            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "FumitrackMainForm_Load: FillcmbFirmaAdi " + hata.Message);
            }

            try
            {
                FillGecmisUygulamalar();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "FumitrackMainForm_Load: FillGecmisUygulamalar " + hata.Message);
            }
        }
        

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (tabControl1.SelectedTab == tabPageIntroductionPlan)
                {
                    foreach (DataGridViewRow row in dtgrdUyglmPlan.Rows)
                    {
                        DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)(row.Cells["AlanAdi2"]);

                        if (cell.DataSource == null || cell.Items.Count < 1)
                        {
                            DataTable AlanAdlari = Classes.DatabaseCommands.GetAlanBilgileriAll(DozajPlanID);
                            cell.DataSource = AlanAdlari;
                            cell.DisplayMember = "AlanAdi";
                            cell.ValueMember = "ID";
                        }

                    }
                }

                else if (tabControl1.SelectedTab == tabPageMonitoringPlan)
                {
                    foreach (DataGridViewRow row in dtgrdIzlemePlan.Rows)
                    {
                        DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)(row.Cells["AlanAdi4"]);

                        if (cell.DataSource == null || cell.Items.Count < 1)
                        {
                            DataTable AlanAdlari = Classes.DatabaseCommands.GetAlanBilgileriAll(DozajPlanID);
                            cell.DataSource = AlanAdlari;
                            cell.DisplayMember = "AlanAdi";
                            cell.ValueMember = "ID";
                        }

                    }
                }

                else if (tabControl1.SelectedTab == tabPageMonitoringDataInput)
                {
                    //if (listBoxVeriFiltre.DataSource == null)
                    {
                        DataTable AlanAdlari = Classes.DatabaseCommands.GetAlanBilgileriAll(DozajPlanID);
                        //foreach (DataRow item in AlanAdlari.Rows)
                        {
                            //if(!listBoxVeriFiltre.Items.Contains(item))
                            {
                                listBoxVeriFiltre.DataSource = AlanAdlari;
                                listBoxVeriFiltre.DisplayMember = "AlanAdi";
                                listBoxVeriFiltre.ValueMember = "ID";

                                try
                                {
                                    listBoxVeriFiltre.SelectedIndex = listBoxVeriFiltreIndex;
                                }
                                catch
                                {
                                }
                            }
                        }

                    }
                }

                else if (tabControl1.SelectedTab == tabPageGraph)
                {
                    //if (listBoxGrafkFiltre.DataSource == null)
                    {
                        DataTable AlanAdlari = Classes.DatabaseCommands.GetAlanBilgileriAll(DozajPlanID);
                        listBoxGrafkFiltre.DataSource = AlanAdlari;
                        listBoxGrafkFiltre.DisplayMember = "AlanAdi";
                        listBoxGrafkFiltre.ValueMember = "ID";

                        try
                        {
                            listBoxGrafkFiltre.SelectedIndex = listBoxGrafkFiltreIndex;
                        }
                        catch
                        {
                        }
                    }
                }
                else if (tabControl1.SelectedTab == tabPageHistory)
                {
                    FillGecmisUygulamalar();
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "tabControl1_SelectedIndexChanged: " + hata.Message);
            }
        }

        private void FumitrackMainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            string message = Localization.Quit;
            if (DialogResult.Yes == MessageBox.Show(this, message, this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            { Environment.Exit(0); }
            else
                e.Cancel=true;
                
        }

        public static Tuple<int, int> getNewWidthandHeight(Bitmap bitmap, System.Drawing.Printing.PrintPageEventArgs e)
        {
            float newWidth = 0;
            float newHeight = 0;
            try
            {
                Image i = bitmap;

                newWidth = i.Width * 50 / i.HorizontalResolution;
                newHeight = i.Height * 120 / i.VerticalResolution;
                float widthFactor = newWidth / e.MarginBounds.Width;
                float heightFactor = newHeight / e.MarginBounds.Height;
                if (widthFactor > 1 | heightFactor > 1)
                {
                    if (widthFactor > heightFactor)
                    {
                        newWidth = newWidth / widthFactor;
                        newHeight = newHeight / widthFactor;
                    }
                    else
                    {
                        newWidth = newWidth / heightFactor;
                        newHeight = newHeight / heightFactor;
                    }
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "getNewWidthandHeight: " + hata.Message);
            }
            return new Tuple<int, int>((int)newWidth, (int)newHeight);
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            try
            {
                Tuple<int, int> tuple = null;
                tuple = getNewWidthandHeight(bitmap, e);
                if (printDocument1.DefaultPageSettings.Landscape == true)
                    e.Graphics.DrawImage(bitmap, 30, 100, tuple.Item1 + 500, tuple.Item2);

                else
                    e.Graphics.DrawImage(bitmap, 30, 150, tuple.Item1 + 130, tuple.Item2);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "printDocument1_PrintPage: " + hata.Message);
            }
        }

        #endregion

        #region TAB FİRMA BİLGİLERİ
        int FirmaID = 0;
        private void BtnFirmaKydt_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(cmbFirmaAdi.Text))
                {
                    string message = Localization.Null;
                    MessageBox.Show(this, message, label1.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                int check = 0;
                DataTable dt = Classes.DatabaseCommands.GetUygulamaYapilanFirmalar(cmbFirmaAdi.Text);
                if (dt != null && dt.Rows.Count > 0)
                {
                    FirmaID = Convert.ToInt32(dt.Rows[0][0].ToString());
                    check = Classes.DatabaseCommands.UpdateUygulamaYapilanFirmalar(FirmaID, cmbFirmaAdi.Text, txtTelefon.Text,
                         txtEposta.Text, txtSehir.Text, txtAdres.Text, txtAciklama.Text);
                }
                else
                {
                    FirmaID = Classes.DatabaseCommands.InsertUygulamaYapilanFirmalar(cmbFirmaAdi.Text, txtTelefon.Text,
                        txtEposta.Text, txtSehir.Text, txtAdres.Text, txtAciklama.Text);
                    check = FirmaID;
                }

                if (check != 0)
                    tabControl1.SelectedTab = tabPageFumigationDosagePlan;

                else
                {
                    string message = Localization.ErrorSave;
                    MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "BtnFirmaKydt_Click: " + hata.Message);
            }
        }

        void ClearFirmaBilgileri()
        {
            txtTelefon.Text = string.Empty;
            txtEposta.Text = string.Empty;
            txtSehir.Text = string.Empty;
            txtAdres.Text = string.Empty;
            txtAciklama.Text = string.Empty;
            cmbFirmaAdi.Text = "";
        }

        private void CmbFirmaAdi_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(cmbFirmaAdi.ValueMember))
                    return;

                if (cmbFirmaAdi.Text == "Yeni Firma (New Company)")
                {
                    cmbFirmaAdi.DropDownStyle = ComboBoxStyle.DropDown;
                    ClearFirmaBilgileri();
                }

                else
                {
                    cmbFirmaAdi.DropDownStyle = ComboBoxStyle.DropDownList;
                    DataTable dt = (DataTable)cmbFirmaAdi.DataSource;
                    var filtered = dt.AsEnumerable()
                                    .Where(r => r.Field<String>("FirmaAdi").Contains(cmbFirmaAdi.Text));

                    DataRow SelectedFirmaInf = filtered.First();
                    txtTelefon.Text = SelectedFirmaInf["Telefon"].ToString();
                    txtEposta.Text = SelectedFirmaInf["Eposta"].ToString();
                    txtSehir.Text = SelectedFirmaInf["Sehir"].ToString();
                    txtAdres.Text = SelectedFirmaInf["Adres"].ToString();
                    txtAciklama.Text = SelectedFirmaInf["Aciklama"].ToString();
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "CmbFirmaAdi_SelectedIndexChanged: " + hata.Message);
            }
        }

        private void btnFirmaYazdr_Click(object sender, EventArgs e)
        {
            try
            {
                bitmap = Classes.DatabaseCommands.Print(tabPageCustomerInfo, printPreviewDialog1, printDocument1);
                printPreviewDialog1.ShowDialog();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnFirmaYazdr_Click: " + hata.Message);
            }
        }

        #endregion

        #region TAB DOZAJ PLANI

        private void cmbEmtia_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbEmtia.Text == "YOK" || cmbEmtia.Text == "ABSENT")
                {
                    txtUrunCinsi.Text = string.Empty;
                    txtAmbalajAdedi.Text = string.Empty;
                    txtUrunMiktari.Text = string.Empty;
                    txtAmbalajSekli.Text = string.Empty;
                    txtPartiNo.Text = string.Empty;
                    panel23.Visible = false;
                }
                else
                    panel23.Visible = true;
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "cmbEmtia_SelectedIndexChanged: " + hata.Message);

            }
        }

        private void btnHedefZararli_Click(object sender, EventArgs e)
        {
            try
            {
                panelHedefZararli.Visible = true;
                panelHedefZararli.Size = new System.Drawing.Size(532, 270);
                panelHedefZararli.Location = tabPageFumigationDosagePlan.PointToClient(Control.MousePosition);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnHedefZararli_Click: " + hata.Message);

            }
        }

        private void btnHdfZararlTamam_Click(object sender, EventArgs e)
        {
            try
            {
                string metin = "";
                int sayac = 0;
                foreach (DataGridViewRow row in dtgrdHedefZararli.Rows)
                {
                    DataGridViewCheckBoxCell ch1 = (DataGridViewCheckBoxCell)row.Cells["Secim"];
                    if (ch1.Value == null)
                        ch1.Value = false;

                    switch (ch1.Value.ToString())
                    {
                        case "True":
                            metin += row.Cells["HedefZararlilar"].Value.ToString() + "-";
                            sayac++;
                            break;
                        case "False":
                            break;
                    }
                }
                if (sayac > 3)
                {
                    string message = Localization.Max3;
                    MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (sayac == 0)
                {
                    string message = Localization.Min1;
                    MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    btnHedefZararli.Text = metin;
                    panelHedefZararli.Visible = false;
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnHdfZararlTamam_Click: " + hata.Message);

            }
        }

        private void btnHdfZararlIptal_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dtgrdHedefZararli.Rows)
                {
                    DataGridViewCheckBoxCell ch1 = (DataGridViewCheckBoxCell)row.Cells["Secim"];
                    ch1.Value = false;
                }
                btnHedefZararli.Text = string.Empty;
                panelHedefZararli.Visible = false;
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnHdfZararlIptal_Click: " + hata.Message);

            }
        }

        void FilldtgrdHedefZararli()
        {
            try
            {
                dtgrdHedefZararli.Rows.Clear();
                for (int i = 0; i < cmbHedefZararli.Items.Count; i++)
                {
                    dtgrdHedefZararli.Rows.Add(false, cmbHedefZararli.Items[i].ToString());
                }
                dtgrdHedefZararli.Columns["HedefZararlilar"].ReadOnly = true;
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "FilldtgrdHedefZararli: " + hata.Message);
            }
        }

        decimal TopAlanHacmi = 0;
        void SetDtgrdYapiAlanBlg_Column()
        {
            try
            {
                //DataGridViewRow row = dtgrdYapiAlanBlg.Rows[dtgrdYapiAlanBlg.Rows.Count-2];
                foreach (DataGridViewRow row in dtgrdYapiAlanBlg.SelectedRows)
                {
                    if (!string.IsNullOrEmpty(cmbDozaj.Text) && row.Cells["AlanHacmi"].Value != null && !string.IsNullOrEmpty(row.Cells["AlanHacmi"].Value.ToString()))
                    {
                        row.Cells["HedefCT"].Value = cmbDozaj.Text;
                        //Yarılama Süresi
                        row.Cells["EstHLT"].Value = Convert.ToDouble(cmbDozaj.Text) / 2;

                        decimal fumiMiktar = Convert.ToDecimal(cmbDozaj.Text) * Convert.ToDecimal(row.Cells["AlanHacmi"].Value);
                        row.Cells["GerekliIlac"].Value = Math.Round((fumiMiktar), 1);

                        decimal GerekliIlac = Convert.ToDecimal(row.Cells["GerekliIlac"].Value);
                        decimal silindirSayisi = GerekliIlac / 10000;
                        silindirSayisi = Math.Floor(100 * silindirSayisi) / 100;
                        row.Cells["KullanilacakSilindirSayisi"].Value = silindirSayisi.ToString("N2");
                    }
                    else
                    {
                        string message = Localization.Null;
                        MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }

                //TOPLAM
                decimal Toplam = 0;
                TopAlanHacmi = 0;
                foreach (DataGridViewRow row in dtgrdYapiAlanBlg.Rows)
                {
                    if (row.Cells["GerekliIlac"].Value != null && !string.IsNullOrEmpty(row.Cells["GerekliIlac"].Value.ToString()))
                    {
                        Toplam += Convert.ToDecimal(row.Cells["GerekliIlac"].Value);
                    }
                    if (row.Cells["AlanHacmi"].Value != null && !string.IsNullOrEmpty(row.Cells["AlanHacmi"].Value.ToString()))
                    {
                        TopAlanHacmi += Convert.ToDecimal(row.Cells["AlanHacmi"].Value);
                    }
                }

                if (Properties.Settings.Default.Dil == "Tr")
                    txtSunuc.Text = "Toplam Fumigant Miktarı :|" + Toplam.ToString() + "|gr (Yaklaşık |" + (Math.Floor(10 * (Toplam / 10000)) / 10).ToString("N1") + "| adet silindir.)";
                else
                    txtSunuc.Text = "Total Fumigant Amount :|" + Toplam.ToString() + "|g (About |" + (Math.Floor(10 * (Toplam / 10000)) / 10).ToString("N1") + "| piece cylinder.)";

            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "SetDtgrdYapiAlanBlg_Column: " + hata.Message);
            }
        }

        private void BtnDzjPlaniHesapla_Click(object sender, EventArgs e)
        {
            try
            {
                SetDtgrdYapiAlanBlg_Column();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "BtnDzjPlaniHesapla_Click: " + hata.Message);
            }
        }

        int DozajPlanID = 0;
        List<int> AlanBilgileriIDs = new List<int>();
        private void BtnDzjPlanKydt_Click(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(btnHedefZararli.Text) || string.IsNullOrEmpty(cmbDozaj.Text) || string.IsNullOrEmpty(cmbEmtia.Text)
                   || string.IsNullOrEmpty(txtFumLisans1.Text) || string.IsNullOrEmpty(txtFumAdSoyad1.Text)
                   || (txtKonteynerNo.Visible == true && string.IsNullOrEmpty(txtKonteynerNo.Text)) || dtgrdYapiAlanBlg.Rows.Count < 1)
                {
                    string message = Localization.Null;
                    MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                int check = 0;
                DozajPlanID = Classes.DatabaseCommands.GetDozajPlanID(DozajPlanID);
                if (DozajPlanID != 0)
                {
                    check = Classes.DatabaseCommands.UpdateDozajPlan(DozajPlanID, FirmaID, dtIlaclamaTrh.Text
                    , txtFumLisans1.Text, txtFumAdSoyad1.Text, txtFumLisans2.Text, txtFumAdSoyad2.Text,
                    txtYapildigiYer.Text, txtKayitNo.Text, txtUrunCinsi.Text, txtUrunMiktari.Text, txtPartiNo.Text,
                    txtAmbalajAdedi.Text, txtAmbalajSekli.Text, txtNeMaksatlaY.Text, txtKonteynerNo.Text, btnHedefZararli.Text,
                    cmbDozaj.Text, cmbEmtia.Text, cmbFumiSekli.Text, cmbBsncTip.Text);
                }
                else
                {
                    DozajPlanID = Classes.DatabaseCommands.InsertDozajPlan(FirmaID, dtIlaclamaTrh.Text
                    , txtFumLisans1.Text, txtFumAdSoyad1.Text, txtFumLisans2.Text, txtFumAdSoyad2.Text,
                    txtYapildigiYer.Text, txtKayitNo.Text, txtUrunCinsi.Text, txtUrunMiktari.Text, txtPartiNo.Text,
                    txtAmbalajAdedi.Text, txtAmbalajSekli.Text, txtNeMaksatlaY.Text, txtKonteynerNo.Text, btnHedefZararli.Text,
                    cmbDozaj.Text, cmbEmtia.Text, cmbFumiSekli.Text, cmbBsncTip.Text);
                }
                foreach (DataGridViewRow row in dtgrdYapiAlanBlg.Rows)
                {
                    if (row.IsNewRow)
                        continue;

                    if (row.Cells["KullanilacakSilindirSayisi"].Value == null)
                    {
                        string message = Localization.NoCalc;
                        MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    string AlanAdi = "";
                    string Sicaklik = "";
                    decimal YarilamaSuresi = 0;
                    decimal MaruzKalmaSuresi = 0;
                    decimal AlanHacmi = 0;
                    decimal FumiMiktar = 0;
                    decimal HedefCT = 0;
                    decimal SilindirSayisi = 0;

                    if (row.Cells["AlanAdi"].Value != null)
                        AlanAdi = row.Cells["AlanAdi"].Value.ToString();

                    if (row.Cells["Sicaklik"].Value != null)
                        Sicaklik = row.Cells["Sicaklik"].Value.ToString();

                    if (row.Cells["EstHLT"].Value != null)
                        YarilamaSuresi = decimal.Parse(row.Cells["EstHLT"].Value.ToString());

                    if (row.Cells["MaruzKalmaSuresi"].Value != null)
                        MaruzKalmaSuresi = decimal.Parse(row.Cells["MaruzKalmaSuresi"].Value.ToString());

                    if (row.Cells["AlanHacmi"].Value != null)
                        AlanHacmi = decimal.Parse(row.Cells["AlanHacmi"].Value.ToString());

                    if (row.Cells["GerekliIlac"].Value != null)
                        FumiMiktar = decimal.Parse(row.Cells["GerekliIlac"].Value.ToString());

                    if (row.Cells["HedefCT"].Value != null)
                        HedefCT = decimal.Parse(row.Cells["HedefCT"].Value.ToString());

                    if (row.Cells["KullanilacakSilindirSayisi"].Value != null)
                        SilindirSayisi = decimal.Parse(row.Cells["KullanilacakSilindirSayisi"].Value.ToString());


                    try
                    {
                        if (AlanBilgileriIDs[row.Index] != 0)
                        {
                            check = Classes.DatabaseCommands.UpdateAlanBilgileri(AlanBilgileriIDs[row.Index], DozajPlanID, AlanAdi, Sicaklik, YarilamaSuresi, MaruzKalmaSuresi
                                , AlanHacmi, FumiMiktar, HedefCT, SilindirSayisi);
                        }
                        else 
                        {
                            AlanBilgileriIDs.Add(Classes.DatabaseCommands.InsertAlanBilgileri(DozajPlanID, AlanAdi, Sicaklik, YarilamaSuresi, MaruzKalmaSuresi
                                , AlanHacmi, FumiMiktar, HedefCT, SilindirSayisi));
                        }
                    }
                    catch
                    {
                        AlanBilgileriIDs.Add(Classes.DatabaseCommands.InsertAlanBilgileri(DozajPlanID, AlanAdi, Sicaklik, YarilamaSuresi, MaruzKalmaSuresi
                                , AlanHacmi, FumiMiktar, HedefCT, SilindirSayisi));
                    }

                }

                if (check != 0 || (AlanBilgileriIDs.Count > 0 && DozajPlanID != 0))
                {
                    dtgrdYapiAlanBlg.ReadOnly = true;
                    dtgrdYapiAlanBlg.AllowUserToAddRows = false;
                    dtgrdYapiAlanBlg.AllowUserToDeleteRows = false;
                    tabControl1.SelectedTab = tabPageIntroductionPlan;
                }

                else
                {
                    string message = Localization.ErrorSave;
                    MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "BtnDzjPlanKydt_Click: " + hata.Message);
            }
        }

        private void cmbFumiSekli_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbFumiSekli.Text.Contains("KONTEYNER") || cmbFumiSekli.Text.Contains("Container"))
                {
                    label28.Visible = true;
                    txtKonteynerNo.Visible = true;
                }
                else
                {
                    label28.Visible = false;
                    txtKonteynerNo.Visible = false;
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "cmbFumiSekli_SelectedIndexChanged: " + hata.Message);
            }
        }

        private void btnDzjPlanYazdr_Click(object sender, EventArgs e)
        {
            try
            {
                btnDzjPlaniHesapla.Visible = false;
                btnDzjPlanKydt.Visible = false;
                btnDzjPlanYazdr.Visible = false;
                bitmap = Classes.DatabaseCommands.Print(tabPageFumigationDosagePlan, printPreviewDialog1, printDocument1);
                printPreviewDialog1.ShowDialog();
                btnDzjPlaniHesapla.Visible = true;
                btnDzjPlanKydt.Visible = true;
                btnDzjPlanYazdr.Visible = true;
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnDzjPlanYazdr_Click: " + hata.Message);
            }
        }

        #endregion

        #region TAB UYGULAMA PLANI
        
        List<int> UygulamaPlanIDs = new List<int>();
        private void btnUyglmPlnKydt_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtgrdUyglmPlan.Rows.Count < 1)
                {
                    string message = Localization.Null;
                    MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                int check = 0;
                foreach (DataGridViewRow row in dtgrdUyglmPlan.Rows)
                {
                    if (row.IsNewRow)
                        continue;

                    string AlanAdi = "";
                    int AlanAdiID = 0;
                    string GirisHatti = "";
                    string MalzemeCinsi = "";
                    string SisCapi = "";
                    string Uzunluk = "";
                    string FanKapasitesi = "";
                    string FanSayisi = "";
                    string SilindirSayisi = "";

                    if (row.Cells["AlanAdi2"].Value != null)
                        AlanAdiID = Convert.ToInt32(row.Cells["AlanAdi2"].Value.ToString());
                    AlanAdi = row.Cells["AlanAdi2"].FormattedValue.ToString();

                    if (row.Cells["GirisHatti"].Value != null)
                        GirisHatti = row.Cells["GirisHatti"].Value.ToString();

                    if (row.Cells["RH"].Value != null)
                        MalzemeCinsi = row.Cells["RH"].Value.ToString();

                    if (row.Cells["Silindir"].Value != null)
                        SisCapi = row.Cells["Silindir"].Value.ToString();

                    if (row.Cells["Uzunluk"].Value != null)
                        Uzunluk = row.Cells["Uzunluk"].Value.ToString();

                    if (row.Cells["FanKapasitesi"].Value != null)
                        FanKapasitesi = row.Cells["FanKapasitesi"].Value.ToString();

                    if (row.Cells["FanSayisi"].Value != null)
                        FanSayisi = row.Cells["FanSayisi"].Value.ToString();

                    if (row.Cells["SilindirSayisi"].Value != null)
                        SilindirSayisi = row.Cells["SilindirSayisi"].Value.ToString();

                    //int AlanAdiID = Classes.DatabaseCommands.GetAlanBilgileribyAlanAdi(AlanAdi);

                    try
                    {
                        //UygulamaPlanIDs.Add(Classes.DatabaseCommands.GetUygulamaPlanID(UygulamaPlanIDs[row.Index]));
                        if (UygulamaPlanIDs[row.Index] != 0)
                        {

                            check = Classes.DatabaseCommands.UpdateUygulamaPlan(UygulamaPlanIDs[row.Index], AlanAdiID, GirisHatti, MalzemeCinsi,
                                SisCapi, Uzunluk, FanKapasitesi, FanSayisi, SilindirSayisi);
                        }
                        else 
                        {

                            UygulamaPlanIDs.Add(Classes.DatabaseCommands.InsertUygulamaPlan(AlanAdiID, GirisHatti, MalzemeCinsi,
                                                        SisCapi, Uzunluk, FanKapasitesi, FanSayisi, SilindirSayisi));
                        }
                    }
                    catch
                    {
                        UygulamaPlanIDs.Add(Classes.DatabaseCommands.InsertUygulamaPlan(AlanAdiID, GirisHatti, MalzemeCinsi,
                                                        SisCapi, Uzunluk, FanKapasitesi, FanSayisi, SilindirSayisi));
                    }

                }

                if (check != 0 || UygulamaPlanIDs.Count > 0)
                {
                    dtgrdUyglmPlan.ReadOnly = true;
                    dtgrdUyglmPlan.AllowUserToAddRows = false;
                    dtgrdUyglmPlan.AllowUserToDeleteRows = false;
                    tabControl1.SelectedTab = tabPageMonitoringPlan;
                }

                else
                {
                    string message = Localization.ErrorSave;
                    MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnUyglmPlnKydt_Click: " + hata.Message);
            }
        }

        private void DtgrdUyglmPlan_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)(dtgrdUyglmPlan.Rows[e.RowIndex].Cells["AlanAdi2"]);

                if (cell.DataSource == null)
                {
                    DataTable AlanAdlari = Classes.DatabaseCommands.GetAlanBilgileriAll(DozajPlanID);
                    cell.DataSource = AlanAdlari;
                    cell.DisplayMember = "AlanAdi";
                    cell.ValueMember = "ID";
                }


            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "DtgrdUyglmPlan_RowsAdded: " + hata.Message);
            }
        }

        private void dtgrdUyglmPlan_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == dtgrdUyglmPlan.Columns["AlanAdi2"].Index && e.RowIndex >= 0)
                {
                    DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)(dtgrdUyglmPlan.Rows[e.RowIndex].Cells["AlanAdi2"]);
                    if (!string.IsNullOrEmpty(cell.FormattedValue.ToString()))
                    {
                        foreach (DataGridViewRow row in dtgrdYapiAlanBlg.Rows)
                        {
                            if (row.Cells["AlanAdi"].Value.ToString().Equals(cell.FormattedValue.ToString()))
                            {
                                decimal GerekliIlac = Convert.ToDecimal(row.Cells["GerekliIlac"].Value.ToString());
                                dtgrdUyglmPlan.Rows[e.RowIndex].Cells["SilindirSayisi"].Value = Math.Floor(100 * (GerekliIlac / 10000)) / 100; 
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "dtgrdUyglmPlan_CellContentClick: " + hata.Message);
            }
        }

        private void btnUyglmPlanYzdr_Click(object sender, EventArgs e)
        {
            try
            {
                bitmap = Classes.DatabaseCommands.Print(tabPageIntroductionPlan, printPreviewDialog1, printDocument1);
                printPreviewDialog1.ShowDialog();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnUyglmPlanYzdr_Click: " + hata.Message);
            }
        }

        #endregion

        #region TAB İZLEME PLANI

        List<int> IzlemePlanIDs = new List<int>();
        private void btnIzlmPlnKydt_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtgrdIzlemePlan.Rows.Count < 1)
                {
                    string message = Localization.Null;
                    MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                int check = 0;
                foreach (DataGridViewRow row in dtgrdIzlemePlan.Rows)
                {
                    if (row.IsNewRow)
                        continue;

                    string AlanAdi = "";
                    int AlanAdiID = 0;
                    string IzlemeNoktasiAdi = "";
                    string OlcumNoktasi = "";
                    string HortumMesafesi = "";

                    if (row.Cells["AlanAdi4"].Value != null)
                        AlanAdiID = Convert.ToInt32(row.Cells["AlanAdi4"].Value.ToString());
                    AlanAdi = row.Cells["AlanAdi4"].FormattedValue.ToString();

                    if (row.Cells["IzlemeNoktasiAdi2"].Value != null)
                        IzlemeNoktasiAdi = row.Cells["IzlemeNoktasiAdi2"].Value.ToString();

                    if (string.IsNullOrEmpty(IzlemeNoktasiAdi))
                    {
                        string message = Localization.EmptyIzlemeNokAd;
                        MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (row.Cells["OlcumNoktasi"].Value != null)
                        OlcumNoktasi = row.Cells["OlcumNoktasi"].Value.ToString();

                    if (row.Cells["HortumMesafesi"].Value != null)
                        HortumMesafesi = row.Cells["HortumMesafesi"].Value.ToString();

                    //int AlanAdiID = Classes.DatabaseCommands.GetAlanBilgileribyAlanAdi(AlanAdi);
                    //IzlemePlanID = Classes.DatabaseCommands.GetIzlemePlanID(IzlemePlanID);
                    try
                    {
                        if (IzlemePlanIDs[row.Index] != 0)
                        {

                            check = Classes.DatabaseCommands.UpdateIzlemePlan(IzlemePlanIDs[row.Index], AlanAdiID, IzlemeNoktasiAdi, OlcumNoktasi,
                                HortumMesafesi);
                        }
                        else
                        {
                            IzlemePlanIDs.Add(Classes.DatabaseCommands.InsertIzlemePlan(AlanAdiID, IzlemeNoktasiAdi, OlcumNoktasi,
                                   HortumMesafesi));
                        }
                    }
                    catch
                    {
                        IzlemePlanIDs.Add(Classes.DatabaseCommands.InsertIzlemePlan(AlanAdiID, IzlemeNoktasiAdi, OlcumNoktasi,
                                   HortumMesafesi));
                    }

                }

                if (check != 0 || IzlemePlanIDs.Count > 0)
                {
                    dtgrdIzlemePlan.ReadOnly = true;
                    dtgrdIzlemePlan.AllowUserToAddRows = false;
                    dtgrdIzlemePlan.AllowUserToDeleteRows = false;
                    label29.Visible = false;
                    tabControl1.SelectedTab = tabPageMonitoringDataInput;
                }

                else
                {
                    string message = Localization.ErrorSave;
                    MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnIzlmPlnKydt_Click: " + hata.Message);
            }
        }

        private void DtgrdIzlemePlan_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)(dtgrdIzlemePlan.Rows[e.RowIndex].Cells["AlanAdi4"]);

                if (cell.DataSource == null)
                {
                    DataTable AlanAdlari = Classes.DatabaseCommands.GetAlanBilgileriAll(DozajPlanID);
                    cell.DataSource = AlanAdlari;
                    cell.DisplayMember = "AlanAdi";
                    cell.ValueMember = "ID";
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "DtgrdIzlemePlan_RowsAdded: " + hata.Message);
            }
        }

        private void DtgrdIzlemeVeriGrdi_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                e.Control.KeyPress -= new KeyPressEventHandler(ColumnZaman_KeyPress);
                if (dtgrdIzlemeVeriGrdi.CurrentCell.ColumnIndex == 2 || dtgrdIzlemeVeriGrdi.CurrentCell.ColumnIndex == 5) //2=Zaman Kolonu , 5=Ölçülen Konsantrasyon Kolonu
                {
                    TextBox tb = e.Control as TextBox;
                    if (tb != null)
                    {
                        tb.KeyPress += new KeyPressEventHandler(ColumnZaman_KeyPress);
                    }
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "DtgrdIzlemeVeriGrdi_EditingControlShowing: " + hata.Message);
            }
        }

        private void ColumnZaman_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dtgrdIzlemeVeriGrdi.SelectedRows)
                {
                    if (dtgrdIzlemeVeriGrdi.CurrentCell.ColumnIndex == 2)
                    {
                        if (row.Cells["Zaman"].EditedFormattedValue != null &&
                    row.Cells["Zaman"].EditedFormattedValue.ToString().Length > 4 && !(char.IsControl(e.KeyChar))
                    && (e.KeyChar != ':'))
                            e.Handled = true;
                    }
                    if (dtgrdIzlemeVeriGrdi.CurrentCell.ColumnIndex == 5)
                    {
                        if (row.Cells["Konsantrasyon"].EditedFormattedValue != null &&
                    !(char.IsControl(e.KeyChar))
                    && (e.KeyChar == '.'))
                            e.Handled = true;
                    }

                        
                }
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ':') && (e.KeyChar != ','))
                {
                    e.Handled = true;
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "ColumnZaman_KeyPress: " + hata.Message);
            }
        }

        private void btnIzlemePlanYazdr_Click(object sender, EventArgs e)
        {
            try
            {
                bitmap = Classes.DatabaseCommands.Print(tabPageMonitoringPlan, printPreviewDialog1, printDocument1);
                printPreviewDialog1.ShowDialog();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnIzlemePlanYazdr_Click: " + hata.Message);
            }
        }
        #endregion

        #region TAB İZLEME VERİ GİRDİ

        private void btnIzlemeVeriGirdiYazdr_Click(object sender, EventArgs e)
        {
            try
            {
                btnVeriHesapla.Visible = false;
                bitmap = Classes.DatabaseCommands.Print(tabPageMonitoringDataInput, printPreviewDialog1, printDocument1);
                printPreviewDialog1.ShowDialog();
                btnVeriHesapla.Visible = true;
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnIzlemeVeriGirdiYazdr_Click: " + hata.Message);
            }
        }

        int InsertKapTarihandSaat = 0;
        string KapanisTarih = "";
        string KapanisSaat = "";
        int MaruzKalmaSure = 0;
        DataTable IzlemeVeriGirdiIDs = new DataTable();
        List<string> HatSayisi = new List<string>();
        private void btnVeriHesapla_Click(object sender, EventArgs e)
        {
            try
            {
                if (IzlemeVeriGirdiIDs.Columns.Count < 1)
                {
                    for (int i = 0; i < listBoxVeriFiltre.Items.Count; i++)
                    {
                        IzlemeVeriGirdiIDs.Columns.Add(listBoxVeriFiltre.GetItemText(listBoxVeriFiltre.Items[i]));
                    }
                    foreach (DataRowView item in listBoxVeriFiltre.Items)
                    {
                        int id = int.Parse(item.Row[listBoxVeriFiltre.ValueMember].ToString());
                        string txt =item.Row[listBoxVeriFiltre.DisplayMember].ToString();
                        DataTable dt = Classes.DatabaseCommands.GetIzlemeVeriGirdileri(DozajPlanID, id);
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            DataRow rr = dt.Rows[j];
                            try
                            {
                                IzlemeVeriGirdiIDs.Rows[j][txt] = rr["ID"].ToString();
                            }
                            catch
                            {
                                DataRow IDRow = IzlemeVeriGirdiIDs.NewRow();
                                IDRow[txt] = rr["ID"].ToString();
                                IzlemeVeriGirdiIDs.Rows.Add(IDRow);
                            }
                        }
                    }
                }
               

                if (panel10.Enabled)
                {
                    try
                    {
                        panel10.Enabled = false;
                        DataTable FumiExcel = Classes.DatabaseCommands.FumiExcelImport();
                        DataTable MaruzKalmaSureandAlanHacmi = Classes.DatabaseCommands.GetMaruzKalmaSuresiandAlanHacmi(Convert.ToInt32(listBoxVeriFiltre.SelectedValue.ToString()));
                        
                        if (MaruzKalmaSureandAlanHacmi.Rows.Count > 0)
                        {
                            MaruzKalmaSure = Convert.ToInt16(MaruzKalmaSureandAlanHacmi.Rows[0]["MaruzKalmaSuresi"].ToString());
                        }
                        if (MaruzKalmaSure == 12)
                        {
                            KapanisSaat = Convert.ToDateTime(dtBasSaat.Value.ToShortTimeString()).AddHours(
                                MaruzKalmaSure).ToString("HH:mm");
                            string baslangicTarih = dtBasTarih.Text + " " + dtBasSaat.Value.ToShortTimeString();
                            KapanisTarih = Convert.ToDateTime(baslangicTarih).AddHours(
                                MaruzKalmaSure).ToString("yyyy-MM-dd");
                            MaruzKalmaSure = 4;
                        }

                        if (MaruzKalmaSure == 24)
                        {
                            KapanisSaat = Convert.ToDateTime(dtBasSaat.Value.ToShortTimeString()).AddHours(
                                MaruzKalmaSure).ToString("HH:mm");
                            string baslangicTarih = dtBasTarih.Text + " " + dtBasSaat.Value.ToShortTimeString();
                            KapanisTarih = Convert.ToDateTime(baslangicTarih).AddHours(
                                MaruzKalmaSure).ToString("yyyy-MM-dd");
                            MaruzKalmaSure = 6;
                        }
                        int xlsRow = 0;
                        if (MaruzKalmaSure == 6)
                            xlsRow = 4;

                        for (int i = 0; i < MaruzKalmaSure; i++)
                        {
                            string baslangicTarih = dtBasTarih.Text + " " + dtBasSaat.Value.ToShortTimeString();
                            string tarih = Convert.ToDateTime(baslangicTarih).AddHours(
                                Convert.ToInt16(FumiExcel.Rows[xlsRow][0].ToString())).ToString("yyyy-MM-dd");
                            dtgrdIzlemeVeriGrdi.Rows[i].Cells["Tarih"].Value = tarih;
                            xlsRow++;
                        }
                    }
                    catch (Exception hata)
                    {
                        loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "açılış ve kapanış saatleri ayarla: " + hata.Message);
                    }
                }
                if (InsertKapTarihandSaat == 0)
                    InsertKapTarihandSaat= Classes.DatabaseCommands.UpdateDozajPlanAclsKapGunandSaat(dtBasTarih.Text, dtBasSaat.Text, KapanisTarih, KapanisSaat, DozajPlanID);

                HatSayisi = dtgrdIzlemeVeriGrdi.Rows.Cast<DataGridViewRow>()
                               .Where(x => !x.IsNewRow)
                               .Where(x => x.Cells["IzlemeNoktasiAdi"].Value != null)
                               .Select(x => x.Cells["IzlemeNoktasiAdi"].Value.ToString())
                               .Distinct()
                               .ToList();

                decimal TopKonsantrasyonOrt=0;
                foreach (DataGridViewRow row in dtgrdIzlemeVeriGrdi.Rows)
                {
                    if (row.IsNewRow)
                        continue;

                    if (row.Cells["Zaman"].Value == null || row.Cells["Konsantrasyon"].Value == null)
                    {
                        string message = Localization.Null;
                        MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    string tarih = "";
                    string saat = "", Sicaklik2="";
                    int IzlemeNoktasiID = 0;
                    decimal OrtKonsantrasyon = 0;
                    decimal HLT = 0;
                    string aciklama = "";

                    if (row.Cells["Tarih"].Value != null)
                        tarih = row.Cells["Tarih"].Value.ToString();
                    if (row.Cells["Zaman"].Value != null)
                        saat = row.Cells["Zaman"].Value.ToString();
                    if (row.Cells["Sicaklik2"].Value != null)
                        Sicaklik2 = row.Cells["Sicaklik2"].Value.ToString();
                    if (row.Cells["IzlemeNoktasiID"].Value != null)
                        IzlemeNoktasiID = Convert.ToInt32(row.Cells["IzlemeNoktasiID"].Value.ToString());
                    if (row.Cells["Konsantrasyon"].Value != null && !string.IsNullOrEmpty(row.Cells["Konsantrasyon"].Value.ToString()))
                        OrtKonsantrasyon = Convert.ToDecimal(row.Cells["Konsantrasyon"].Value.ToString());
                    if (row.Cells["HLT"].Value != null && !string.IsNullOrEmpty(row.Cells["HLT"].Value.ToString()))
                        HLT = Convert.ToDecimal(row.Cells["HLT"].Value.ToString());
                    if (row.Cells["Aciklama2"].Value != null)
                        aciklama = row.Cells["Aciklama2"].Value.ToString();

                    
                    decimal TopKonsantrasyon = 0;

                    if (row.Index % HatSayisi.Count == 0)
                    {
                        for (int i = row.Index; i < HatSayisi.Count + row.Index; i++)
                        {
                            TopKonsantrasyonOrt = 0;
                            if (!string.IsNullOrEmpty(dtgrdIzlemeVeriGrdi.Rows[i].Cells["Konsantrasyon"].Value.ToString()) && dtgrdIzlemeVeriGrdi.Rows[i].Cells["Konsantrasyon"].Value!=null)
                            TopKonsantrasyon += decimal.Parse(dtgrdIzlemeVeriGrdi.Rows[i].Cells["Konsantrasyon"].Value.ToString());
                        }
                    }
                    if (TopKonsantrasyon != 0)
                    {
                        TopKonsantrasyonOrt = TopKonsantrasyon / HatSayisi.Count;
                        TopKonsantrasyonOrt = decimal.Parse((Math.Floor(10 * TopKonsantrasyonOrt) / 10).ToString("N1"));
                    }

                    string TopKonsantrasyonOrtStr = "0";
                    try
                    {
                        if (TopKonsantrasyonOrt != 0)
                        {
                            if (TopKonsantrasyonOrt > Convert.ToDecimal(row.Cells["HLT"].Value.ToString()))
                            {
                                aciklama = Localization.FumiSuccess;
                                row.Cells["Aciklama2"].Value = aciklama;
                                row.Cells["HatOrtKonsantrasyon"].Value = TopKonsantrasyonOrt;

                                txtSonucIzlemeVeriGirdi.Text = "";
                                groupBox5.Visible = false;
                            }
                            else
                            {
                                decimal takviye = Math.Round(((Convert.ToDecimal(cmbDozaj.Text) - TopKonsantrasyonOrt) * AlanHacmi2), 1);
                                if (Properties.Settings.Default.Dil == "Tr")
                                    aciklama = "Takviye gerekli. (" + Math.Round(takviye, 0) + ")gr";
                                else
                                    aciklama = "Reinforcements required. (" + Math.Round(takviye, 0) + ")g";

                                var minHLT = dtgrdIzlemeVeriGrdi.Rows.Cast<DataGridViewRow>()
                               .Where(x => !x.IsNewRow)
                               .Select(x => x.Cells["HLT"].Value)
                               .Min();

                                if (HLT==Convert.ToDecimal(minHLT))
                                {
                                    if (Properties.Settings.Default.Dil == "Tr")
                                        aciklama = "Fumigasyon Başarısız.";
                                    else
                                        aciklama = "The fumigation is Failed.";

                                    //KapanisTarih = tarih;
                                    txtSonucIzlemeVeriGirdi.Text = aciklama;
                                    groupBox5.Visible = true;
                                    //KapanisSaat = saat;
                                }
                                row.Cells["Aciklama2"].Value = aciklama;
                                row.Cells["HatOrtKonsantrasyon"].Value = TopKonsantrasyonOrt;
                            }
                        }

                        if (TopKonsantrasyonOrt != 0)
                            TopKonsantrasyonOrtStr = TopKonsantrasyonOrt.ToString();

                        if(IzlemeVeriGirdiIDs.Rows[row.Index][listBoxVeriFiltre.GetItemText(listBoxVeriFiltre.SelectedItem)].ToString() != "")
                        {

                            Classes.DatabaseCommands.UpdateIzlemeVeriGirdi(Convert.ToInt32(IzlemeVeriGirdiIDs.Rows[row.Index][listBoxVeriFiltre.GetItemText(listBoxVeriFiltre.SelectedItem)].ToString())
                                , DozajPlanID, Convert.ToInt32(listBoxVeriFiltre.SelectedValue.ToString()),
                            tarih, saat, Sicaklik2, IzlemeNoktasiID, OrtKonsantrasyon, TopKonsantrasyonOrtStr, HLT, aciklama);
                        }
                        else
                        {
                            IzlemeVeriGirdiIDs.Rows[row.Index][listBoxVeriFiltre.GetItemText(listBoxVeriFiltre.SelectedItem)] = Classes.DatabaseCommands.InsertIzlemeVeriGirdi(DozajPlanID, Convert.ToInt32(listBoxVeriFiltre.SelectedValue.ToString()),
                                 tarih, saat, Sicaklik2, IzlemeNoktasiID, OrtKonsantrasyon, TopKonsantrasyonOrtStr, HLT, aciklama);
                        }
                    }
                    catch
                    {
                        DataRow IDRow = IzlemeVeriGirdiIDs.NewRow();
                        IDRow[listBoxVeriFiltre.GetItemText(listBoxVeriFiltre.SelectedItem)] = Classes.DatabaseCommands.InsertIzlemeVeriGirdi(DozajPlanID, Convert.ToInt32(listBoxVeriFiltre.SelectedValue.ToString()),
                             tarih, saat, Sicaklik2, IzlemeNoktasiID, OrtKonsantrasyon, TopKonsantrasyonOrtStr, HLT, aciklama);
                        IzlemeVeriGirdiIDs.Rows.Add(IDRow);
                    }


                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnVeriHesapla_Click: " + hata.Message);
            }
        }

        decimal AlanHacmi2 = 0;
        int listBoxVeriFiltreIndex = 0;
        private void listBoxVeriFiltre_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (listBoxVeriFiltre.SelectedValue.ToString() == "System.Data.DataRowView")
                    return;

                if (IzlemeVeriGirdiIDs.Columns.Count < 1 && GecmisUygulamalarAlldata==null)
                {
                    for (int i = 0; i < listBoxVeriFiltre.Items.Count; i++)
                    {
                        IzlemeVeriGirdiIDs.Columns.Add(listBoxVeriFiltre.GetItemText(listBoxVeriFiltre.Items[i]));
                    }
                }

                if (listBoxVeriFiltre.SelectedIndex!=0)
                listBoxVeriFiltreIndex = listBoxVeriFiltre.SelectedIndex;
                //if (IzlemeVeriGirdiIDs.Columns.Count > 0)
                {
                    DataTable dt = Classes.DatabaseCommands.GetIzlemeVeriGirdileri(DozajPlanID, Convert.ToInt32(listBoxVeriFiltre.SelectedValue.ToString()));
                    if (dt.Rows.Count > 0)
                    {
                        dtgrdIzlemeVeriGrdi.Rows.Clear();
                        foreach (DataRow row in dt.Rows)
                        {
                            dtgrdIzlemeVeriGrdi.Rows.Add(row["AlanAdi"].ToString(), row["Tarih"].ToString(),
                                row["Saat"].ToString(), row["Sicaklik"].ToString(), row["IzlemeNoktasiAdi"].ToString(), row["IzlemeNokBilgID"].ToString()
                                , row["OrtKonsantrasyon"].ToString(), row["HatOrtKonsantrasyon"].ToString(), row["HLT"].ToString(), row["Aciklama"].ToString());
                        }

                        DataTable MaruzKalmaSureandAlanHacmi = Classes.DatabaseCommands.GetMaruzKalmaSuresiandAlanHacmi(Convert.ToInt32(listBoxVeriFiltre.SelectedValue.ToString()));
                        
                        if (MaruzKalmaSureandAlanHacmi.Rows.Count > 0)
                        {
                            AlanHacmi2 = Convert.ToDecimal(MaruzKalmaSureandAlanHacmi.Rows[0]["AlanHacmi"].ToString());
                        }
                        return;
                    }
                }


                try
                {
                    DataTable FumiExcel = Classes.DatabaseCommands.FumiExcelImport();
                    dtgrdIzlemeVeriGrdi.Rows.Clear();
                    DataTable IzlemeNoktasiAdiandID = Classes.DatabaseCommands.GetIzlemePlanbyAlanID(Convert.ToInt32(listBoxVeriFiltre.SelectedValue.ToString()));
                    DataTable MaruzKalmaSureandAlanHacmi = Classes.DatabaseCommands.GetMaruzKalmaSuresiandAlanHacmi(Convert.ToInt32(listBoxVeriFiltre.SelectedValue.ToString()));
                    

                    if (MaruzKalmaSureandAlanHacmi.Rows.Count > 0)
                    {
                        MaruzKalmaSure = Convert.ToInt16(MaruzKalmaSureandAlanHacmi.Rows[0]["MaruzKalmaSuresi"].ToString());
                        AlanHacmi2 = Convert.ToDecimal(MaruzKalmaSureandAlanHacmi.Rows[0]["AlanHacmi"].ToString());
                    }

                    if (MaruzKalmaSure == 12)
                    {
                        KapanisSaat=  Convert.ToDateTime(dtBasSaat.Value.ToShortTimeString()).AddHours(
                            MaruzKalmaSure).ToString("HH:mm");
                        string baslangicTarih = dtBasTarih.Text + " " + dtBasSaat.Value.ToShortTimeString();
                        KapanisTarih = Convert.ToDateTime(baslangicTarih).AddHours(
                            MaruzKalmaSure).ToString("yyyy-MM-dd");
                        MaruzKalmaSure = 4;
                    }

                    if (MaruzKalmaSure == 24)
                    {
                        KapanisSaat = Convert.ToDateTime(dtBasSaat.Value.ToShortTimeString()).AddHours(
                            MaruzKalmaSure).ToString("HH:mm");
                        string baslangicTarih = dtBasTarih.Text + " " + dtBasSaat.Value.ToShortTimeString();
                        KapanisTarih = Convert.ToDateTime(baslangicTarih).AddHours(
                            MaruzKalmaSure).ToString("yyyy-MM-dd");
                        MaruzKalmaSure = 6;
                    }

                    if (IzlemeNoktasiAdiandID != null)
                    {
                        int xlsRow = 0;
                        int xlsColumn = 3;
                        if (MaruzKalmaSure == 6)
                            xlsRow = 4;

                        switch (cmbDozaj.Text)
                        {
                            case "15":
                                xlsColumn = 2;
                                break;
                            case "16":
                                xlsColumn = 5;
                                break;
                            case "20":
                                xlsColumn = 8;
                                break;
                            case "24":
                                xlsColumn = 11;
                                break;
                            case "50":
                                xlsColumn = 14;
                                break;
                            case "100":
                                xlsColumn = 17;
                                break;
                        }

                        for (int i = 0; i < MaruzKalmaSure; i++)
                        {
                            string baslangicTarih = dtBasTarih.Text+ " "+dtBasSaat.Value.ToShortTimeString();
                            string tarih= Convert.ToDateTime(baslangicTarih).AddHours(
                                Convert.ToInt16(FumiExcel.Rows[xlsRow][0].ToString())).ToString("yyyy-MM-dd");

                            foreach (DataRow row in IzlemeNoktasiAdiandID.Rows)
                            {
                                dtgrdIzlemeVeriGrdi.Rows.Add(listBoxVeriFiltre.GetItemText(listBoxVeriFiltre.SelectedItem), tarih,
                                    "", "", row["IzlemeNoktasiAdi"].ToString(), row["ID"].ToString()
                                    , "", "", Math.Round(Convert.ToDecimal(FumiExcel.Rows[xlsRow][xlsColumn].ToString()), 1));
                            }
                            xlsRow++;
                        }
                    }
                }
                catch (Exception hata)
                {
                    loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "listBoxVeriFiltre_SelectedIndexChanged_2: " + hata.Message);
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "listBoxVeriFiltre_SelectedIndexChanged: " + hata.Message);
            }
        }

        #endregion

        #region TAB GRAFİK

        private void btnZamanGrafikYazdr_Click(object sender, EventArgs e)
        {
            try
            {
                //this.chartZamanGrafk.Printing.PrintPreview();

                if (!string.IsNullOrEmpty(txtSonucIzlemeVeriGirdi.Text))
                {
                    lblGrfkIlaveMesaj.Text = Localization.AddFumiHours;
                    //lblGrfkIlaveMesaj.Visible = true;
                }
                lblGrfkBaslik.Text = listBoxGrafkFiltre.GetItemText(listBoxGrafkFiltre.SelectedItem) + " " + tabPageGraph.Text;
                //lblGrfkBaslik.Visible = true;
                printDocument1.DefaultPageSettings.Landscape = true;
                btnZamanGrafikYazdr.Visible = false;
                groupBox14.Visible = false;
                bitmap = Classes.DatabaseCommands.Print(tabPageGraph, printPreviewDialog1, printDocument1);
                printPreviewDialog1.ShowDialog();
                lblGrfkBaslik.Visible = false;
                //lblGrfkIlaveMesaj.Visible = false;
                printDocument1.DefaultPageSettings.Landscape = false;
                btnZamanGrafikYazdr.Visible = true;
                groupBox14.Visible = true;
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnZamanGrafikYazdr_Click: " + hata.Message);
            }
        }

        void GrafikClear()
        {
            try
            {
                foreach (System.Windows.Forms.DataVisualization.Charting.DataPoint item in chartZamanGrafk.Series["Alt Sınır"].Points)
                {
                    item.YValues[0] = 0;
                }
                foreach (System.Windows.Forms.DataVisualization.Charting.DataPoint item in chartZamanGrafk.Series["Gerçekleşen"].Points)
                {
                    item.YValues[0] = 0;
                }
                chartZamanGrafk.ResetAutoValues();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "GrafikClear: " + hata.Message);
            }
        }

        int listBoxGrafkFiltreIndex = 0;
        private void listBoxGrafkFiltre_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (listBoxGrafkFiltre.SelectedValue.ToString() == "System.Data.DataRowView")
                    return;

                if (listBoxGrafkFiltre.SelectedIndex != 0)
                    listBoxGrafkFiltreIndex = listBoxGrafkFiltre.SelectedIndex;

                lblGrfkIlaveMesaj.Text = "";
                lblGrfkBaslik.Text = listBoxGrafkFiltre.GetItemText(listBoxGrafkFiltre.SelectedItem) + " " + tabPageGraph.Text;

                DataTable IzlemeVeriGirdi = Classes.DatabaseCommands.GetIzlemeVeriGirdiHLTandHatOrtKonsantr(DozajPlanID, Convert.ToInt32(listBoxGrafkFiltre.SelectedValue));
                if (IzlemeVeriGirdi.Rows.Count < 1)
                { GrafikClear(); return; }
                
                    List<string> HLTs = new List<string>();
                    HLTs = IzlemeVeriGirdi.Rows.Cast<DataRow>()
                               .Where(x => x["HLT"] != null)
                               .Select(x => x["HLT"].ToString())
                               .Distinct()
                               .ToList();
                int HatSayisiCount = 0;
                HatSayisiCount = Classes.DatabaseCommands.GetIzlemePlanbyAlanID(Convert.ToInt32(listBoxGrafkFiltre.SelectedValue)).Rows.Count;

                List<string> HatOrtKons = new List<string>();
                List<string> HatOrtKonsDistinct = new List<string>();
                HatOrtKons = IzlemeVeriGirdi.Rows.Cast<DataRow>()
                           .Where(x => x["HatOrtKonsantrasyon"] != null)
                           .Select(x => x["HatOrtKonsantrasyon"].ToString())
                           .ToList();
                for (int i = 0; i < HatOrtKons.Count; i+= HatSayisiCount)
                {
                    HatOrtKonsDistinct.Add(HatOrtKons[i]);
                }


                int MaruzKalmaSure = Classes.DatabaseCommands.GetAlanBilgileriID(Convert.ToInt32(listBoxGrafkFiltre.SelectedValue));
                chartZamanGrafk.ChartAreas[0].AxisY.Maximum = Convert.ToDouble(cmbDozaj.Text);
                chartZamanGrafk.ChartAreas[0].AxisY.Interval = 1;
                if (chartZamanGrafk.ChartAreas[0].AxisY.Maximum == 100)
                    chartZamanGrafk.ChartAreas[0].AxisY.Interval = 2;

                if (MaruzKalmaSure == 12)
                {
                    chartZamanGrafk.ChartAreas[0].AxisX.Maximum = 12;
                    chartZamanGrafk.Series["Alt Sınır"].Points[0].YValues[0] = 0;
                    //-------------------------------ALT SINIR-----------------------------------
                    int indexHLT = 0;
                    for (int i = 0; i < 5; i++)
                    {
                        try
                        {
                            if (i == 0)
                                chartZamanGrafk.Series["Alt Sınır"].Points[0].YValues[0] = Convert.ToDouble(HLTs[0]);
                            else
                            {
                                chartZamanGrafk.Series["Alt Sınır"].Points[i].YValues[0] = Convert.ToDouble(HLTs[indexHLT]);
                                indexHLT++;
                            }
                        }
                        catch
                        {
                            indexHLT--;
                            i--;
                        }
                    }
                    
                    chartZamanGrafk.Series["Alt Sınır"].Points[5].YValues[0] = 0;
                    chartZamanGrafk.Series["Alt Sınır"].Points[6].YValues[0] = 0;
                    //---------------------------------------------------------------------------

                    //---------------------------GERÇEKLEŞEN----------------------------------
                    int indexOrtKonst = 0;
                    for (int i = 1; i < 5; i++)
                    {
                        try
                        {
                            chartZamanGrafk.Series["Gerçekleşen"].Points[i].YValues[0] = Convert.ToDouble(HatOrtKonsDistinct[indexOrtKonst]);
                            indexOrtKonst++;
                        }
                        catch
                        {
                            indexOrtKonst--;
                            i--;
                        }
                    }
                    chartZamanGrafk.Series["Gerçekleşen"].Points[0].YValues[0] = Convert.ToInt32(cmbDozaj.Text);
                    chartZamanGrafk.Series["Gerçekleşen"].Points[5].YValues[0] = 0;
                    chartZamanGrafk.Series["Gerçekleşen"].Points[6].YValues[0] = 0;
                    //---------------------------------------------------------------------------
                }

                if (MaruzKalmaSure == 24)
                {
                    chartZamanGrafk.ChartAreas[0].AxisX.Maximum = 24;
                    chartZamanGrafk.Series["Alt Sınır"].Points[0].YValues[0] = 0;
                    //---------------------ALT SINIR---------------------------------------
                    int indexHLT = 0;
                    for (int i = 0; i < 7; i++)
                    {
                        try
                        {
                            if (i == 0)
                                chartZamanGrafk.Series["Alt Sınır"].Points[0].YValues[0] = Convert.ToDouble(HLTs[0]);
                            else
                            {
                                chartZamanGrafk.Series["Alt Sınır"].Points[i].YValues[0] = Convert.ToDouble(HLTs[indexHLT]);
                                indexHLT++;
                            }
                        }
                        catch
                        {
                            indexHLT--;
                            i--;
                        }
                    }
                    //---------------------------------------------------------------------------


                    //-------------------------GERÇEKLEŞEN-----------------------------------
                    int indexOrtKonst = 0;
                    for (int i = 1; i < 7; i++)
                    {
                        try
                        {
                            chartZamanGrafk.Series["Gerçekleşen"].Points[i].YValues[0] = Convert.ToDouble(HatOrtKonsDistinct[indexOrtKonst]);
                            indexOrtKonst++;
                        }
                        catch
                        {
                            indexOrtKonst--;
                            i--;
                        }
                    }

                    chartZamanGrafk.Series["Gerçekleşen"].Points[0].YValues[0] = Convert.ToInt32(cmbDozaj.Text);
                    //---------------------------------------------------------------------------
                }

                
                if (!string.IsNullOrEmpty(txtSonucIzlemeVeriGirdi.Text))
                {
                    lblGrfkIlaveMesaj.Text = Localization.AddFumiHours;
                }
                
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "listBoxGrafkFiltre_SelectedIndexChanged: " + hata.Message);

            }
        }

        private void btnIzlemeVeriGrafk_Click(object sender, EventArgs e)
        {
            try
            {
                int index = listBoxVeriFiltre.SelectedIndex;
                tabControl1.SelectedTab = tabPageGraph;
                listBoxGrafkFiltre.SelectedIndex = index;
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnIzlemeVeriGrafk_Click: " + hata.Message);

            }
        }

        #endregion

        
        

        

        #region WORD RAPORLAR
        private void turkceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Classes.DatabaseCommands.OpenDocReport("BMSB-Sulfuryl-Fluoride-Treatment-Certifikate-Turkce.docx");
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "turkceToolStripMenuItem_Click: " + hata.Message);
            }
        }

        private void ingilizceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Classes.DatabaseCommands.OpenDocReport("BMSB-Sulfuryl-Fluoride-Treatment-Certificate.doc");
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "ingilizceToolStripMenuItem_Click: " + hata.Message);
            }
        }

        private void turkce2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Classes.DatabaseCommands.OpenDocReport("BMSB-Sulfuryl-Metric-Turkce.doc");
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "turkce2ToolStripMenuItem_Click: " + hata.Message);
            }
        }

        private void ingilizce2ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                Classes.DatabaseCommands.OpenDocReport("BMSB-Sulfuryl-Metric.doc");
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "ingilizce2ToolStripMenuItem1_Click: " + hata.Message);
            }
        }

        private void turkce3ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                Classes.DatabaseCommands.OpenDocReport("BMSB-Sulfuryl-Imperial-Turkce.doc");
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "turkce3ToolStripMenuItem1_Click: " + hata.Message);
            }
        }

        private void ingilizce3ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                Classes.DatabaseCommands.OpenDocReport("BMSB-Sulfuryl-Imperial.doc");
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "ingilizce3ToolStripMenuItem2_Click: " + hata.Message);
            }
        }

        private void turkce4ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                Classes.DatabaseCommands.OpenDocReport("BMSB-Sulfuryl-3rd-Party-Turkce.docx");
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "turkce4ToolStripMenuItem2_Click: " + hata.Message);
            }
        }

        private void ingilizce4ToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            try
            {
                Classes.DatabaseCommands.OpenDocReport("BMSB-Sulfuryl-3rd-Party.doc");
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "ingilizce4ToolStripMenuItem3_Click: " + hata.Message);
            }
        }
#endregion






        #region TAB GEÇMİŞ UYGULAMALAR
        string dtgrdGecmisUyglmSatirDPID = "0";
        private void dtgrdGecmisUyglm_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)//farenin sağ tuşuna basılmışsa
                {
                    int satir = dtgrdGecmisUyglm.HitTest(e.X, e.Y).RowIndex;
                    if (satir > -1)
                    {
                        dtgrdGecmisUyglm.ClearSelection();
                        dtgrdGecmisUyglm.Rows[satir].Selected = true;
                        dtgrdGecmisUyglmSatirDPID = dtgrdGecmisUyglm.Rows[satir].Cells["DozajPlanIDGecmis"].Value.ToString();
                    }
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "- dtgrdGecmisUyglm_MouseDown: " + hata.Message);
            }
        }

        private void toolStripMenuItemRapor_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable GecmisUygulamalarAlldata= Classes.DatabaseCommands.GetGecmisUygulamalarAlldata(int.Parse(dtgrdGecmisUyglmSatirDPID));
                Report.Report report = new Report.Report();
                try
                {
                    List<string> TmpAlanHacmi = new List<string>();
                    TmpAlanHacmi = GecmisUygulamalarAlldata.Rows.Cast<DataRow>()
                               .Where(x => x["AlanHacmi"] != null)
                               .Select(x => x["AlanHacmi"].ToString())
                               .Distinct()
                               .ToList();
                    List<decimal> AlanHacmi = TmpAlanHacmi.ConvertAll(s => decimal.Parse(s));

                    List<string> TmpFumiMiktar = new List<string>();
                    TmpFumiMiktar = GecmisUygulamalarAlldata.Rows.Cast<DataRow>()
                               .Where(x => x["FumiMiktar"] != null)
                               .Select(x => x["FumiMiktar"].ToString())
                               .Distinct()
                               .ToList();
                    List<decimal> FumiMiktar = TmpFumiMiktar.ConvertAll(s => decimal.Parse(s));

                    DataRow row = GecmisUygulamalarAlldata.Rows[0];

                    report.lblTarih.Text = row["IlaclamaTarih"].ToString();
                    report.lblKayitNo.Text = row["KayitNo"].ToString();
                    report.lblYptrnFirma.Text = row["FirmaAdi"].ToString();
                    report.lblYpldYer.Text = row["YapildigiYer"].ToString();
                    report.lblFumiSekli.Text = row["FumiSekli"].ToString();
                    report.lblKonteynerNo.Text = row["KonteynerNo"].ToString();
                    report.lblUrunCinsi.Text = row["UrunCinsi"].ToString();
                    report.lblKontrolNo.Text = row["KontrolNo"].ToString();
                    report.lblAmbljAdet.Text = row["AmbalajAdedi"].ToString();
                    report.lblUrunMiktar.Text = row["UrunMiktari"].ToString();
                    report.lblAmbljSekli.Text = row["AmbalajSekli"].ToString();
                    report.lblIlaclananHcm.Text = Math.Round(AlanHacmi.Sum(),1).ToString();
                    report.lblFumiScklk.Text = row["Sicaklik"].ToString();
                    report.lblFumiDoz.Text = row["Dozaj"].ToString() + " Gr";
                    report.lblFumiMktr.Text = Math.Round(FumiMiktar.Sum(),1).ToString() + " Gr";
                    report.lblKapanGunSaat.Text = row["KapanisTarih"].ToString() + " - " + row["KapanisSaat"].ToString();
                    report.lblAclsGunSaat.Text = row["AcilisTarih"].ToString() + " - " + row["AcilisSaat"].ToString();
                    report.lblNeMaksatlaY.Text = row["NeMaksatlaY"].ToString();
                    report.lblZararliAd.Text = row["HedefZararli"].ToString();
                    report.lblFumiYpnAdSoyad1.Text = row["FumiAdSoyad1"].ToString() + "-" + row["FumiLisans1"].ToString();
                    report.lblFumiYpnAdSoyad2.Text = row["FumiAdSoyad2"].ToString() + "-" + row["FumiLisans2"].ToString();
                    report.lblEmtia.Text = row["Emtia"].ToString();
                    report.lblBasincTipi.Text = row["BasincTip"].ToString();

                }
                catch (Exception hata)
                {
                    loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "normalRaporToolStripMenuItem_Click: " + hata.Message);
                }
                finally
                {
                    report.Show();
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "- dtgrdGecmisUyglm_MouseDown: " + hata.Message);
            }
        }
        DataTable GecmisUygulamalarAlldata = null;
        private void toolStripMenuItemVeriGor_Click(object sender, EventArgs e)
        {
            try
            {
                GecmisUygulamalarAlldata = Classes.DatabaseCommands.GetGecmisUygulamalarAlldata(int.Parse(dtgrdGecmisUyglmSatirDPID));

                DataRow row = GecmisUygulamalarAlldata.Rows[0];

                DozajPlanID = int.Parse(row["DozajPlanID"].ToString());
                

                //Firma---------------------------
                cmbFirmaAdi.Text = row["FirmaAdi"].ToString();
                txtTelefon.Text = row["Telefon"].ToString();
                txtEposta.Text = row["Eposta"].ToString();
                txtSehir.Text = row["Sehir"].ToString();
                txtAdres.Text = row["Adres"].ToString();
                txtAciklama.Text = row["Aciklama"].ToString();
                ((Control)this.tabPageCustomerInfo).Enabled = false;
                //--------------------------------------



                //Dozaj Plan----------------------------
                dtIlaclamaTrh.Value= DateTime.Parse(row["IlaclamaTarih"].ToString());
                txtFumLisans1.Text = row["FumiLisans1"].ToString();
                txtFumAdSoyad1.Text = row["FumiAdSoyad1"].ToString();
                txtFumLisans2.Text = row["FumiLisans2"].ToString();
                txtFumAdSoyad2.Text = row["FumiAdSoyad2"].ToString();
                txtYapildigiYer.Text = row["YapildigiYer"].ToString();
                txtKayitNo.Text = row["KayitNo"].ToString();
                txtUrunCinsi.Text = row["UrunCinsi"].ToString();
                txtUrunMiktari.Text = row["UrunMiktari"].ToString();
                txtPartiNo.Text = row["KontrolNo"].ToString();
                txtAmbalajAdedi.Text = row["AmbalajAdedi"].ToString();
                txtAmbalajSekli.Text = row["AmbalajSekli"].ToString();
                txtNeMaksatlaY.Text = row["NeMaksatlaY"].ToString();
                txtKonteynerNo.Text = row["KonteynerNo"].ToString();
                btnHedefZararli.Text = row["HedefZararli"].ToString();
                cmbDozaj.Text = row["Dozaj"].ToString();
                cmbEmtia.Text = row["Emtia"].ToString();
                cmbFumiSekli.Text = row["FumiSekli"].ToString();
                cmbBsncTip.Text = row["BasincTip"].ToString();


                List<string> AlanAdi = new List<string>();
                AlanAdi = GecmisUygulamalarAlldata.Rows.Cast<DataRow>()
                           .Where(x => x["AlanAdi"] != null)
                           .Select(x => x["AlanAdi"].ToString())
                           .Distinct()
                           .ToList();
                dtgrdYapiAlanBlg.Rows.Clear();
                for (int i = 0; i < AlanAdi.Count; i++)
                {
                    var AlanAdiRow = GecmisUygulamalarAlldata.Rows.Cast<DataRow>()
                               .Where(x => x["AlanAdi"].ToString() == AlanAdi[i])
                               .ToList();
                    dtgrdYapiAlanBlg.Rows.Add(AlanAdi[i], AlanAdiRow[0]["Sicaklik"].ToString(), AlanAdiRow[0]["YarilamaSuresi"].ToString(),
                        AlanAdiRow[0]["MaruzKalmaSuresi"].ToString(), AlanAdiRow[0]["AlanHacmi"].ToString(), AlanAdiRow[0]["FumiMiktar"].ToString(),
                        AlanAdiRow[0]["SilindirSayisi"].ToString(), AlanAdiRow[0]["HedefCT"].ToString());
                }
                SetDtgrdYapiAlanBlg_Column();
                groupBox2.Enabled = false;
                groupBox3.Enabled = false;
                groupBox8.Enabled = false;
                panel13.Enabled = false;
                panel23.Enabled = false;
                //--------------------------------------





                //Uygulama Plan-----------------------
                List<string> AlanBilgIDs = new List<string>();
                AlanBilgIDs = GecmisUygulamalarAlldata.Rows.Cast<DataRow>()
                           .Where(x => x["AlanBilgileriID"] != null)
                           .Select(x => x["AlanBilgileriID"].ToString())
                           .Distinct()
                           .ToList();
                AlanBilgileriIDs.Clear();
                AlanBilgileriIDs = AlanBilgIDs.ConvertAll(s => int.Parse(s));
                dtgrdUyglmPlan.Rows.Clear();
                for (int i = 0; i < AlanBilgileriIDs.Count; i++)
                {
                   DataTable UygulamaPlan= Classes.DatabaseCommands.GetUygulamaPlanbyAlanID(AlanBilgileriIDs[i]);
                    for (int j = 0; j < UygulamaPlan.Rows.Count; j++)
                    {
                        DataRow r = UygulamaPlan.Rows[j];
                        dtgrdUyglmPlan.Rows.Add(r["AlanAdi"].ToString(), r["GirisHatti"].ToString(), r["MalzemeCinsi"].ToString(),
                            r["SisCapi"].ToString(), r["Uzunluk"].ToString(), r["FanKapasitesi"].ToString(),
                            r["FanSayisi"].ToString(), r["SilindirSayisi"].ToString()); 
                    }
                }
                groupBox6.Enabled = false;
                panel21.Enabled = false;
                //--------------------------------------



                //İzleme Plan-----------------------
                dtgrdIzlemePlan.Rows.Clear();
                for (int i = 0; i < AlanBilgileriIDs.Count; i++)
                {
                    DataTable IzlemePlan = Classes.DatabaseCommands.GetIzlemePlanbyID(AlanBilgileriIDs[i]);
                    for (int j = 0; j < IzlemePlan.Rows.Count; j++)
                    {
                        DataRow r = IzlemePlan.Rows[j];
                        dtgrdIzlemePlan.Rows.Add(r["AlanAdi"].ToString(), r["IzlemeNoktasiAdi"].ToString(), r["OlcumNoktasi"].ToString(),
                            r["HortumMesafesi"].ToString()); 
                    }
                }
                groupBox9.Enabled = false;
                panel22.Enabled = false;
                label29.Visible = false;
                //--------------------------------------


                //İzleme Veri Girdi-----------------------
                IzlemeVeriGirdiIDs.Columns.Clear();
                dtBasTarih.Value = DateTime.Parse(row["AcilisTarih"].ToString());
                dtBasSaat.Value= DateTime.Parse(row["AcilisSaat"].ToString());
                DataRow rw = GecmisUygulamalarAlldata.Rows[GecmisUygulamalarAlldata.Rows.Count - 1];
                string OrtKonst = rw["OrtKonsantrasyon"].ToString();
                if (OrtKonst != "0,0")
                {
                    panel10.Enabled = false;
                    panel11.Enabled = false;
                    panel17.Enabled = false;
                }
                else
                {
                    panel10.Enabled = true;
                    panel11.Enabled = true;
                    panel17.Enabled = true;
                }
                //--------------------------------------

                tabControl1.SelectedTab = tabPageMonitoringDataInput;
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "- toolStripMenuItemVeriGor_Click: " + hata.Message);
            }
        }
        #endregion


        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                AboutForm About = new AboutForm();
                About.ShowDialog();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "- aboutToolStripMenuItem_Click: " + hata.Message);
            }
        }

        private void FumitrackMainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                AboutForm AF = new AboutForm();
                AF.ShowDialog();
            }
        }

        private void toolStripMenuItemSil_Click(object sender, EventArgs e)
        {
            try
            {
                string message = Localization.QDelete;
                if (DialogResult.Yes == MessageBox.Show(this, message, this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    int check = Classes.DatabaseCommands.DeleteGecmisUyglByID(dtgrdGecmisUyglmSatirDPID);
                    if (check != 0)
                        FillGecmisUygulamalar();
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "- toolStripMenuItemSil_Click: " + hata.Message);
                string message = Localization.ErrorSave;
                MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
