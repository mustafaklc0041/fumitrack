﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fumitrack.Language;
using MetroFramework.Forms;

namespace Fumitrack.GUI
{
    public partial class AdminPanelForm : MetroForm
    {
        public AdminPanelForm()
        {
            InitializeComponent();
            Classes.DatabaseCommands.DataGridColor(dtgrdFirmaListesi);
            Classes.DatabaseCommands.SetLanguage(this);
        }

        public string LoginUser = "";
        private void btnKaydet_YP_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtFirmaAd_YP.Text) || string.IsNullOrEmpty(txtKullAdi_YP.Text) || string.IsNullOrEmpty(txtSifre_YP.Text))
                {
                    string message = Localization.Null;
                    MessageBox.Show(this, message, label1.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                string MD5Sifre = "";
                if (txtSifre_YP.Text.Trim()!=MD5liSifre)
                {
                    MD5Sifre = Classes.DatabaseCommands.MD5Sifrele(txtSifre_YP.Text.Trim());
                }
                else
                {
                    MD5Sifre = MD5liSifre;
                }
                

                bool check = false;
                if (btnKaydet_YP.Text=="KAYDET" || btnKaydet_YP.Text == "SAVE")
                {
                    check = Classes.DatabaseCommands.InsertNewLogin(txtFirmaAd_YP.Text, txtTelefon_YP.Text,
                    txtEposta_YP.Text, txtSehir_YP.Text, txtAdres_YP.Text, txtAciklama_YP.Text, txtKullAdi_YP.Text, MD5Sifre,LoginUser);
                }
                if (btnKaydet_YP.Text == "GÜNCELLE" || btnKaydet_YP.Text == "UPDATE")
                {
                    check = Classes.DatabaseCommands.UpdateLoginUser(dtgrdFirmaListesiSatirID,txtFirmaAd_YP.Text, txtTelefon_YP.Text,
                    txtEposta_YP.Text, txtSehir_YP.Text, txtAdres_YP.Text, txtAciklama_YP.Text, txtKullAdi_YP.Text, MD5Sifre);
                    tabControl1.SelectedTab = tabPageUsers;
                }

                if (check==true)
                {
                    string message = Localization.Success;
                    MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Clear();
                    loadUsers();
                }
                else
                {
                    string message = Localization.NoInternet;
                    MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnKaydet_YP_Click: " + hata.Message);
            }
        }

        void Clear()
        {
            txtFirmaAd_YP.Text = string.Empty;
            txtTelefon_YP.Text= string.Empty;
            txtEposta_YP.Text= string.Empty;
            txtSehir_YP.Text= string.Empty;
            txtAdres_YP.Text= string.Empty;
            txtAciklama_YP.Text = string.Empty;
            txtKullAdi_YP.Text = string.Empty;
            txtSifre_YP.Text = string.Empty;
            btnKaydet_YP.Text = Localization.BtnSave;
            MD5liSifre = "";
        }

        void loadUsers()
        {
            try
            {
                DataTable users = Classes.DatabaseCommands.GetAllLoginUsers();
                dtgrdFirmaListesi.DataSource = users;
                dtgrdFirmaListesi.Columns["logtime"].Visible = false;
                dtgrdFirmaListesi.Columns["Yetki"].Visible = false;
                dtgrdFirmaListesi.Columns["MacID"].Visible = false;
                dtgrdFirmaListesi.Columns["ID"].Visible = false;
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "loadUsers: " + hata.Message);
            }
        }

        private void AdminPanelForm_Load(object sender, EventArgs e)
        {
            try
            {
                loadUsers();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "AdminPanelForm_Load: " + hata.Message);
            }
        }

        private void guncelleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtgrdFirmaListesiSatirID!="0")
                {
                   DataTable dt= Classes.DatabaseCommands.GetAllLoginUsersFromID(dtgrdFirmaListesiSatirID);

                    if (dt != null && dt.Rows.Count>0)
                    {
                        DataRow user = dt.Rows[0];
                        txtFirmaAd_YP.Text = user["FirmaAdi"].ToString();
                        txtTelefon_YP.Text = user["Telefon"].ToString();
                        txtEposta_YP.Text = user["Eposta"].ToString();
                        txtSehir_YP.Text = user["Sehir"].ToString();
                        txtAdres_YP.Text = user["Adres"].ToString();
                        txtAciklama_YP.Text = user["Aciklama"].ToString();
                        txtKullAdi_YP.Text = user["KullaniciAdi"].ToString();
                        MD5liSifre = user["Sifre"].ToString();
                        txtSifre_YP.Text = user["Sifre"].ToString();
                        btnKaydet_YP.Text = Localization.BtnUpdate;
                        tabControl1.SelectedTab = tabPageUserCreate;
                    }
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "guncelleToolStripMenuItem_Click: " + hata.Message);
            }
        }

        string dtgrdFirmaListesiSatirID = "0";
        string MD5liSifre = "";
        private void dtgrdFirmaListesi_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)//farenin sağ tuşuna basılmışsa
                {
                    int satir = dtgrdFirmaListesi.HitTest(e.X, e.Y).RowIndex;
                    if (satir > -1)
                    {
                        dtgrdFirmaListesi.ClearSelection();
                        dtgrdFirmaListesi.Rows[satir].Selected = true;
                        dtgrdFirmaListesiSatirID = dtgrdFirmaListesi.Rows[satir].Cells["ID"].Value.ToString();
                    }
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "- dtgrdFirmaListesi_MouseDown: " + hata.Message);
            }
        }

        private void btnTemizle_YP_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void AdminPanelForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        private void silToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtgrdFirmaListesiSatirID!="0")
                {
                    string message = Localization.QDelete;
                    if (MessageBox.Show(this, message, this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        bool check = Classes.DatabaseCommands.DeleteLoginUser(dtgrdFirmaListesiSatirID);

                        if (check == true)
                        {
                            message = Localization.Success;
                            MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            loadUsers();
                        }
                        else
                        {
                            message = Localization.NoInternet;
                            MessageBox.Show(this, message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, " silToolStripMenuItem_Click: " + hata.Message);
            }
        }

        private void dtgrdFirmaListesi_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            try
            {
                using (SolidBrush b = new SolidBrush(dtgrdFirmaListesi.RowHeadersDefaultCellStyle.ForeColor))
                {
                    e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, b, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 4);
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "dtgrdFirmaListesi_RowPostPaint: " + hata.Message);
            }
        }
    }
}
