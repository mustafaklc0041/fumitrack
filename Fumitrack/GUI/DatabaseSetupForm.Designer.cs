﻿namespace Fumitrack.GUI
{
    partial class DatabaseSetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DatabaseSetupForm));
            this.tabControlDBsetup = new System.Windows.Forms.TabControl();
            this.tabPageDBTest = new System.Windows.Forms.TabPage();
            this.btntestConnection = new System.Windows.Forms.Button();
            this.rdsqlsrvauthentication = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.CmbServerName = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.rdwindowsauthentication = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPageStartSetup = new System.Windows.Forms.TabPage();
            this.btnSetupstart = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtdatabasename = new System.Windows.Forms.TextBox();
            this.spinnerPictureBox = new System.Windows.Forms.PictureBox();
            this.tabControlDBsetup.SuspendLayout();
            this.tabPageDBTest.SuspendLayout();
            this.tabPageStartSetup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinnerPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlDBsetup
            // 
            this.tabControlDBsetup.Controls.Add(this.tabPageDBTest);
            this.tabControlDBsetup.Controls.Add(this.tabPageStartSetup);
            this.tabControlDBsetup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlDBsetup.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tabControlDBsetup.Location = new System.Drawing.Point(20, 60);
            this.tabControlDBsetup.Name = "tabControlDBsetup";
            this.tabControlDBsetup.SelectedIndex = 0;
            this.tabControlDBsetup.Size = new System.Drawing.Size(760, 370);
            this.tabControlDBsetup.TabIndex = 19;
            // 
            // tabPageDBTest
            // 
            this.tabPageDBTest.Controls.Add(this.btntestConnection);
            this.tabPageDBTest.Controls.Add(this.rdsqlsrvauthentication);
            this.tabPageDBTest.Controls.Add(this.label1);
            this.tabPageDBTest.Controls.Add(this.CmbServerName);
            this.tabPageDBTest.Controls.Add(this.label3);
            this.tabPageDBTest.Controls.Add(this.txtUserID);
            this.tabPageDBTest.Controls.Add(this.rdwindowsauthentication);
            this.tabPageDBTest.Controls.Add(this.label4);
            this.tabPageDBTest.Controls.Add(this.txtpassword);
            this.tabPageDBTest.Controls.Add(this.label5);
            this.tabPageDBTest.Location = new System.Drawing.Point(4, 25);
            this.tabPageDBTest.Name = "tabPageDBTest";
            this.tabPageDBTest.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDBTest.Size = new System.Drawing.Size(752, 341);
            this.tabPageDBTest.TabIndex = 0;
            this.tabPageDBTest.Text = "Veritabanı Bilgisi";
            this.tabPageDBTest.UseVisualStyleBackColor = true;
            // 
            // btntestConnection
            // 
            this.btntestConnection.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btntestConnection.Location = new System.Drawing.Point(333, 157);
            this.btntestConnection.Name = "btntestConnection";
            this.btntestConnection.Size = new System.Drawing.Size(175, 31);
            this.btntestConnection.TabIndex = 16;
            this.btntestConnection.Text = "BAĞLANTIYI TEST ET";
            this.btntestConnection.UseVisualStyleBackColor = true;
            this.btntestConnection.Click += new System.EventHandler(this.btntestConnection_Click);
            // 
            // rdsqlsrvauthentication
            // 
            this.rdsqlsrvauthentication.AutoSize = true;
            this.rdsqlsrvauthentication.Checked = true;
            this.rdsqlsrvauthentication.Location = new System.Drawing.Point(38, 16);
            this.rdsqlsrvauthentication.Name = "rdsqlsrvauthentication";
            this.rdsqlsrvauthentication.Size = new System.Drawing.Size(142, 21);
            this.rdsqlsrvauthentication.TabIndex = 0;
            this.rdsqlsrvauthentication.TabStop = true;
            this.rdsqlsrvauthentication.Text = "SQL Sunucu Kimliği";
            this.rdsqlsrvauthentication.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Veri Kaynağı:";
            // 
            // CmbServerName
            // 
            this.CmbServerName.FormattingEnabled = true;
            this.CmbServerName.Items.AddRange(new object[] {
            "127.0.0.1",
            ".\\SQLEXPRESS ",
            ".\\\\SQLEXPRESS ",
            "\\\\SQLEXPRESS",
            ".SQLEXPRESS",
            "(local)",
            "(.)",
            "."});
            this.CmbServerName.Location = new System.Drawing.Point(38, 81);
            this.CmbServerName.Name = "CmbServerName";
            this.CmbServerName.Size = new System.Drawing.Size(182, 24);
            this.CmbServerName.TabIndex = 2;
            this.CmbServerName.Text = "(local)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(330, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Kullanıcı ID:";
            // 
            // txtUserID
            // 
            this.txtUserID.Location = new System.Drawing.Point(333, 78);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(175, 24);
            this.txtUserID.TabIndex = 3;
            this.txtUserID.Text = "sa";
            // 
            // rdwindowsauthentication
            // 
            this.rdwindowsauthentication.AutoSize = true;
            this.rdwindowsauthentication.Location = new System.Drawing.Point(214, 16);
            this.rdwindowsauthentication.Name = "rdwindowsauthentication";
            this.rdwindowsauthentication.Size = new System.Drawing.Size(122, 21);
            this.rdwindowsauthentication.TabIndex = 1;
            this.rdwindowsauthentication.Text = "Windows Kimliği";
            this.rdwindowsauthentication.UseVisualStyleBackColor = true;
            this.rdwindowsauthentication.CheckedChanged += new System.EventHandler(this.rdwindowsauthentication_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(330, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Şifre:";
            // 
            // txtpassword
            // 
            this.txtpassword.Location = new System.Drawing.Point(333, 121);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.Size = new System.Drawing.Size(175, 24);
            this.txtpassword.TabIndex = 4;
            this.txtpassword.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(329, 170);
            this.label5.TabIndex = 8;
            this.label5.Text = resources.GetString("label5.Text");
            // 
            // tabPageStartSetup
            // 
            this.tabPageStartSetup.Controls.Add(this.btnSetupstart);
            this.tabPageStartSetup.Controls.Add(this.label2);
            this.tabPageStartSetup.Controls.Add(this.txtdatabasename);
            this.tabPageStartSetup.Controls.Add(this.spinnerPictureBox);
            this.tabPageStartSetup.Location = new System.Drawing.Point(4, 25);
            this.tabPageStartSetup.Name = "tabPageStartSetup";
            this.tabPageStartSetup.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStartSetup.Size = new System.Drawing.Size(752, 341);
            this.tabPageStartSetup.TabIndex = 2;
            this.tabPageStartSetup.Text = "Kuruluma Başla";
            this.tabPageStartSetup.UseVisualStyleBackColor = true;
            // 
            // btnSetupstart
            // 
            this.btnSetupstart.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btnSetupstart.Location = new System.Drawing.Point(74, 112);
            this.btnSetupstart.Name = "btnSetupstart";
            this.btnSetupstart.Size = new System.Drawing.Size(175, 35);
            this.btnSetupstart.TabIndex = 109;
            this.btnSetupstart.Text = "KURULUMU BAŞLAT";
            this.btnSetupstart.UseVisualStyleBackColor = true;
            this.btnSetupstart.Click += new System.EventHandler(this.btnSetupstart_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(71, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 17);
            this.label2.TabIndex = 107;
            this.label2.Text = "Veritabanı Adı:";
            // 
            // txtdatabasename
            // 
            this.txtdatabasename.Location = new System.Drawing.Point(74, 42);
            this.txtdatabasename.Name = "txtdatabasename";
            this.txtdatabasename.Size = new System.Drawing.Size(175, 24);
            this.txtdatabasename.TabIndex = 108;
            this.txtdatabasename.Text = "Fumitrack";
            // 
            // spinnerPictureBox
            // 
            this.spinnerPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.spinnerPictureBox.Image = global::Fumitrack.Properties.Resources.loading_apple;
            this.spinnerPictureBox.Location = new System.Drawing.Point(286, 100);
            this.spinnerPictureBox.Margin = new System.Windows.Forms.Padding(2);
            this.spinnerPictureBox.Name = "spinnerPictureBox";
            this.spinnerPictureBox.Size = new System.Drawing.Size(62, 56);
            this.spinnerPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.spinnerPictureBox.TabIndex = 106;
            this.spinnerPictureBox.TabStop = false;
            this.spinnerPictureBox.Visible = false;
            // 
            // DatabaseSetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControlDBsetup);
            this.Name = "DatabaseSetupForm";
            this.Text = "Veritabanı Ayarları";
            this.tabControlDBsetup.ResumeLayout(false);
            this.tabPageDBTest.ResumeLayout(false);
            this.tabPageDBTest.PerformLayout();
            this.tabPageStartSetup.ResumeLayout(false);
            this.tabPageStartSetup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinnerPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlDBsetup;
        private System.Windows.Forms.TabPage tabPageDBTest;
        private System.Windows.Forms.RadioButton rdsqlsrvauthentication;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUserID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPageStartSetup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox spinnerPictureBox;
        public System.Windows.Forms.RadioButton rdwindowsauthentication;
        public System.Windows.Forms.Button btntestConnection;
        public System.Windows.Forms.Button btnSetupstart;
        public System.Windows.Forms.TextBox txtdatabasename;
        public System.Windows.Forms.ComboBox CmbServerName;
    }
}