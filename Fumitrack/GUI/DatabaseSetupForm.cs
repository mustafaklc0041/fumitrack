﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fumitrack.Language;
using MetroFramework.Forms;

namespace Fumitrack.GUI
{
    public partial class DatabaseSetupForm : MetroForm
    {
        public DatabaseSetupForm()
        {
            InitializeComponent();
            this.tabPageStartSetup.Parent = null;
            Classes.DatabaseCommands.SetLanguage(this);
        }
        public bool auto = false;
        private void btntestConnection_Click(object sender, EventArgs e)
        {
            try
            {
                TestConnection();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btntestConnection_Click: " + hata.Message);

            }
        }

        public bool TestConnection()
        {
            bool durum = false;
            try
            {
                string connString1 = "";
                string connString2 = "";
                if (rdsqlsrvauthentication.Checked == true)
                {
                    connString1 = @"Data Source='" + CmbServerName.Text + "'; User ID='" + txtUserID.Text + "';Password='" + txtpassword.Text + "'";
                    connString2 = @"Data Source='" + CmbServerName.Text + "' " +
                                                   "; Initial Catalog='" + txtdatabasename.Text + "' " +
                                                   "; User ID='" + txtUserID.Text + "'; Password='" + txtpassword.Text + "' ";
                }
                else
                {
                    connString1 = @"Data Source='" + CmbServerName.Text + "'; Integrated Security=true";
                    connString2 = @"Data Source='" + CmbServerName.Text + "' " +
                            "; Initial Catalog='" + txtdatabasename.Text + "'; Integrated Security=true";

                }
                durum=SetLocalConnectionString(connString1, connString2);
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "TestConnection: " + hata.Message);

            }
            return durum;
        }

        bool SetLocalConnectionString(string connString1, string connString2)
        {
            bool durum = false;
            try
            {
                SqlConnection sqlConn = Classes.DataAccess.Connection(connString1);
                if (sqlConn.State == ConnectionState.Open)
                {
                    try
                    {
                        sqlConn = Classes.DataAccess.Connection(connString2);
                        if (sqlConn.State == ConnectionState.Open)
                        {
                            Properties.Settings.Default.LocalConnectionString = connString2;

                            Properties.Settings.Default.Save();
                            Properties.Settings.Default.Upgrade();
                            Properties.Settings.Default.Reload();
                            durum = true;
                            if (!auto)
                            {
                                string message = Localization.VeritabaniVar;
                                MessageBox.Show(message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            this.tabPageStartSetup.Parent = this.tabControlDBsetup;
                            tabControlDBsetup.SelectedTab = tabPageStartSetup;
                            
                        }
                        else
                        {
                            if (!auto)
                            {
                                string message = Localization.SuccessSQL;
                                MessageBox.Show(message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            Properties.Settings.Default.LocalConnectionString = connString1;

                            Properties.Settings.Default.Save();
                            Properties.Settings.Default.Upgrade();
                            Properties.Settings.Default.Reload();
                            durum = true;
                            this.tabPageStartSetup.Parent = this.tabControlDBsetup;
                            tabControlDBsetup.SelectedTab = tabPageStartSetup;
                        }
                    }
                    catch
                    {
                        if (!auto)
                        {
                            string message = Localization.SuccessSQL;
                            MessageBox.Show(message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        Properties.Settings.Default.LocalConnectionString = connString1;

                        Properties.Settings.Default.Save();
                        Properties.Settings.Default.Upgrade();
                        Properties.Settings.Default.Reload();
                        durum = true;
                        this.tabPageStartSetup.Parent = this.tabControlDBsetup;
                        tabControlDBsetup.SelectedTab = tabPageStartSetup;
                    }
                }
                else
                {
                    if (!auto)
                    {
                        string message = Localization.ErrorSQL;
                        MessageBox.Show(message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (Exception hata)
            {
                if (!auto)
                {
                    string message = Localization.ErrorSQL;
                    MessageBox.Show(message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "SetLocalConnectionString: " + hata.Message);
            }
            return durum;
        }

        private void btnSetupstart_Click(object sender, EventArgs e)
        {
            try
            {
                spinnerPictureBox.Visible = true;
                btnSetupstart.Enabled = false;
                string LocalsqlConnectionString = Properties.Settings.Default.LocalConnectionString;
                int check= Classes.DatabaseCommands.CreateDatabase(txtdatabasename.Text);
                if (check != 0)
                {
                    string message = Localization.DatabaseCreate;
                    MessageBox.Show(message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    string message = Localization.VeritabaniHata;
                    MessageBox.Show(message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    spinnerPictureBox.Visible = false;
                    btnSetupstart.Enabled = true;
                }
            }
            catch (Exception hata)
            {
                string message = Localization.VeritabaniHata;
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnSetupstart_Click: " + hata.Message);
                MessageBox.Show(message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                spinnerPictureBox.Visible = false;
                btnSetupstart.Enabled = true;

            }
        }

        private void rdwindowsauthentication_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdwindowsauthentication.Checked == true)
                {
                    CmbServerName.Text = ".\\SQLEXPRESS";
                    txtUserID.Enabled = false;
                    txtpassword.Enabled = false;
                }
                else
                {
                    txtUserID.Enabled = true;
                    txtpassword.Enabled = true;
                }
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "rdwindowsauthentication_CheckedChanged: " + hata.Message);
            }
        }
    }
}
