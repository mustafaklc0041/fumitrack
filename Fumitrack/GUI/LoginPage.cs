﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fumitrack.Language;
using MetroFramework.Forms;
using Microsoft.Win32;

namespace Fumitrack.GUI
{
    public partial class LoginPage : MetroForm
    {
        public LoginPage()
        {
            InitializeComponent();

            Classes.DatabaseCommands.SetLanguage(this);
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                lblLoginHata.Text = "";

                if (string.IsNullOrEmpty(txtUserName.Text))
                {
                    string message = Localization.KullaniciAdiNull;
                    MessageBox.Show(this, message, label1.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (string.IsNullOrEmpty(txtPasswd.Text))
                {
                    string message = Localization.ParolaNull;
                    MessageBox.Show(this, message, label2.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                bool Internet = Classes.DatabaseCommands.CheckInternetConnection();
                if(Internet==false)
                {
                    lblLoginHata.Text = Localization.CheckInternet;
                    return;
                }

                string MD5Sifre = Classes.DatabaseCommands.MD5Sifrele(txtPasswd.Text.Trim());

                string mac = "";
                try
                {
                    mac = MAC.Value();
                }
                catch { }

                bool check= Classes.DatabaseCommands.CheckLogin(txtUserName.Text.Trim(), MD5Sifre, mac);
                
                if (check==true)//Başarılı giriş
                {
                    if (checkBoxRememberMe.Checked)
                    {
                        Properties.Settings.Default["username"] = txtUserName.Text;
                        Properties.Settings.Default["password"] = txtPasswd.Text;
                    }
                    else
                    {
                        Properties.Settings.Default["username"] = "";
                        Properties.Settings.Default["password"] = "";
                    }
                    Properties.Settings.Default.Save();
                    Properties.Settings.Default.Upgrade();

                    //Classes.DatabaseCommands.CopyFumiExcelServertoLocal();

                    int yetki = Classes.DatabaseCommands.GetLoginUserYetki(txtUserName.Text.Trim());
                    if (yetki==1)
                    {
                        AdminPanelForm APF = new AdminPanelForm();
                        APF.LoginUser = txtUserName.Text.Trim();
                        this.Hide();
                        APF.Show();
                    }
                    else
                    {
                        FumitrackMainForm FMF = new FumitrackMainForm();
                        this.Hide();
                        FMF.Show();
                    }
                }
                else
                {
                    lblLoginHata.Text = Localization.LoginError;
                }
            }
            catch (Exception hata)
            {
                Fumitrack.loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "btnLogin_Click: " + hata.Message);
            }
        }

        void SetDatabase()
        {
            try
            {
                try
                {
                    RegistryKey kontrol = null;// Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server");
                    if (kontrol == null)
                    {
                        Process p = new Process();
                        p.StartInfo.FileName = Application.StartupPath + @"\Resources\setup.exe";
                        p.Start();
                        //İşlem bitene kadar bekle
                        p.WaitForExit();
                    }
                }
                catch(Exception aa)
                {
                }

                string DBConnection = Properties.Settings.Default.LocalConnectionString;

                if (string.IsNullOrEmpty(DBConnection))
                {
                    DatabaseSetupForm DSF = new DatabaseSetupForm();
                    DSF.rdwindowsauthentication.Checked = true;
                    DSF.auto = true;
                    bool durum = false;
                    int i = 0;
                    while (!durum)
                    {
                        DSF.CmbServerName.Text = DSF.CmbServerName.Items[i].ToString();
                        durum = DSF.TestConnection();
                        i++;
                    }

                    int check = Classes.DatabaseCommands.CreateDatabase(DSF.txtdatabasename.Text);
                    if (check != 0)
                    {
                    }
                    else
                    {
                        string message = Localization.VeritabaniHata;
                        MessageBox.Show(message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (Exception hata)
            {
                Fumitrack.loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "SetDatabase: " + hata.Message);
            }
        }

        private void LoginPage_Load(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    SetDatabase();//İlk açılışta database yok ise otomatik oluşturulur.
                }
                catch {}


                try
                {
                    Classes.DatabaseCommands.DatabaseSync();
                }
                catch { }

                txtUserName.Text = Properties.Settings.Default["username"].ToString();
                txtPasswd.Text = Properties.Settings.Default["password"].ToString();
                if (!string.IsNullOrEmpty(txtUserName.Text))
                {
                    checkBoxRememberMe.Checked = true;
                }
                this.BringToFront();
            }
            catch (Exception hata)
            {
                loglama.error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "LoginPage_Load: " + hata.Message);

            }
        }

        private void LoginPage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.D && e.Control == true)
            {
                DatabaseSetupForm DSF = new DatabaseSetupForm();
                DSF.ShowDialog();
            }
        }
    }
}
