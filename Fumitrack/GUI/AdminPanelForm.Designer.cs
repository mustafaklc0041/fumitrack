﻿namespace Fumitrack.GUI
{
    partial class AdminPanelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageUserCreate = new System.Windows.Forms.TabPage();
            this.btnTemizle_YP = new System.Windows.Forms.Button();
            this.btnKaydet_YP = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtTelefon_YP = new System.Windows.Forms.MaskedTextBox();
            this.txtAdres_YP = new System.Windows.Forms.TextBox();
            this.txtAciklama_YP = new System.Windows.Forms.TextBox();
            this.txtEposta_YP = new System.Windows.Forms.TextBox();
            this.txtSehir_YP = new System.Windows.Forms.TextBox();
            this.txtSifre_YP = new System.Windows.Forms.TextBox();
            this.txtKullAdi_YP = new System.Windows.Forms.TextBox();
            this.txtFirmaAd_YP = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPageUsers = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtgrdFirmaListesi = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.guncelleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.silToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label9 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPageUserCreate.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPageUsers.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdFirmaListesi)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageUserCreate);
            this.tabControl1.Controls.Add(this.tabPageUsers);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Tahoma", 11.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tabControl1.Location = new System.Drawing.Point(23, 74);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(960, 455);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPageUserCreate
            // 
            this.tabPageUserCreate.Controls.Add(this.btnTemizle_YP);
            this.tabPageUserCreate.Controls.Add(this.btnKaydet_YP);
            this.tabPageUserCreate.Controls.Add(this.groupBox1);
            this.tabPageUserCreate.Location = new System.Drawing.Point(4, 27);
            this.tabPageUserCreate.Name = "tabPageUserCreate";
            this.tabPageUserCreate.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUserCreate.Size = new System.Drawing.Size(952, 424);
            this.tabPageUserCreate.TabIndex = 0;
            this.tabPageUserCreate.Text = "Uygulayıcı Firma Ekleme";
            this.tabPageUserCreate.UseVisualStyleBackColor = true;
            // 
            // btnTemizle_YP
            // 
            this.btnTemizle_YP.Font = new System.Drawing.Font("Tahoma", 11.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnTemizle_YP.Location = new System.Drawing.Point(662, 376);
            this.btnTemizle_YP.Name = "btnTemizle_YP";
            this.btnTemizle_YP.Size = new System.Drawing.Size(134, 42);
            this.btnTemizle_YP.TabIndex = 10;
            this.btnTemizle_YP.Text = "TEMİZLE";
            this.btnTemizle_YP.UseVisualStyleBackColor = true;
            this.btnTemizle_YP.Click += new System.EventHandler(this.btnTemizle_YP_Click);
            // 
            // btnKaydet_YP
            // 
            this.btnKaydet_YP.Font = new System.Drawing.Font("Tahoma", 11.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKaydet_YP.Location = new System.Drawing.Point(815, 376);
            this.btnKaydet_YP.Name = "btnKaydet_YP";
            this.btnKaydet_YP.Size = new System.Drawing.Size(134, 42);
            this.btnKaydet_YP.TabIndex = 9;
            this.btnKaydet_YP.Text = "KAYDET";
            this.btnKaydet_YP.UseVisualStyleBackColor = true;
            this.btnKaydet_YP.Click += new System.EventHandler(this.btnKaydet_YP_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.txtTelefon_YP);
            this.groupBox1.Controls.Add(this.txtAdres_YP);
            this.groupBox1.Controls.Add(this.txtAciklama_YP);
            this.groupBox1.Controls.Add(this.txtEposta_YP);
            this.groupBox1.Controls.Add(this.txtSehir_YP);
            this.groupBox1.Controls.Add(this.txtSifre_YP);
            this.groupBox1.Controls.Add(this.txtKullAdi_YP);
            this.groupBox1.Controls.Add(this.txtFirmaAd_YP);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox1.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(946, 367);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Uygulayıcı Firma Bilgileri";
            // 
            // txtTelefon_YP
            // 
            this.txtTelefon_YP.Location = new System.Drawing.Point(174, 94);
            this.txtTelefon_YP.Mask = "(999) 000-0000";
            this.txtTelefon_YP.Name = "txtTelefon_YP";
            this.txtTelefon_YP.Size = new System.Drawing.Size(288, 25);
            this.txtTelefon_YP.TabIndex = 3;
            // 
            // txtAdres_YP
            // 
            this.txtAdres_YP.Location = new System.Drawing.Point(580, 94);
            this.txtAdres_YP.Multiline = true;
            this.txtAdres_YP.Name = "txtAdres_YP";
            this.txtAdres_YP.Size = new System.Drawing.Size(336, 77);
            this.txtAdres_YP.TabIndex = 4;
            // 
            // txtAciklama_YP
            // 
            this.txtAciklama_YP.Location = new System.Drawing.Point(174, 198);
            this.txtAciklama_YP.Multiline = true;
            this.txtAciklama_YP.Name = "txtAciklama_YP";
            this.txtAciklama_YP.Size = new System.Drawing.Size(742, 73);
            this.txtAciklama_YP.TabIndex = 6;
            // 
            // txtEposta_YP
            // 
            this.txtEposta_YP.Location = new System.Drawing.Point(174, 146);
            this.txtEposta_YP.Name = "txtEposta_YP";
            this.txtEposta_YP.Size = new System.Drawing.Size(288, 25);
            this.txtEposta_YP.TabIndex = 5;
            // 
            // txtSehir_YP
            // 
            this.txtSehir_YP.Location = new System.Drawing.Point(580, 45);
            this.txtSehir_YP.Name = "txtSehir_YP";
            this.txtSehir_YP.Size = new System.Drawing.Size(336, 25);
            this.txtSehir_YP.TabIndex = 2;
            // 
            // txtSifre_YP
            // 
            this.txtSifre_YP.Location = new System.Drawing.Point(174, 330);
            this.txtSifre_YP.Name = "txtSifre_YP";
            this.txtSifre_YP.Size = new System.Drawing.Size(288, 25);
            this.txtSifre_YP.TabIndex = 8;
            this.txtSifre_YP.UseSystemPasswordChar = true;
            // 
            // txtKullAdi_YP
            // 
            this.txtKullAdi_YP.Location = new System.Drawing.Point(174, 290);
            this.txtKullAdi_YP.Name = "txtKullAdi_YP";
            this.txtKullAdi_YP.Size = new System.Drawing.Size(288, 25);
            this.txtKullAdi_YP.TabIndex = 7;
            // 
            // txtFirmaAd_YP
            // 
            this.txtFirmaAd_YP.Location = new System.Drawing.Point(174, 45);
            this.txtFirmaAd_YP.Name = "txtFirmaAd_YP";
            this.txtFirmaAd_YP.Size = new System.Drawing.Size(288, 25);
            this.txtFirmaAd_YP.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(48, 333);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 18);
            this.label8.TabIndex = 0;
            this.label8.Text = "*Şifre";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(48, 293);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 18);
            this.label7.TabIndex = 0;
            this.label7.Text = "*Kullanıcı Adı";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(48, 201);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 18);
            this.label5.TabIndex = 0;
            this.label5.Text = "Açıklama";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(48, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 18);
            this.label4.TabIndex = 0;
            this.label4.Text = "E-Posta Adresi";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(48, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "Telefon";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(517, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 18);
            this.label6.TabIndex = 0;
            this.label6.Text = "Şehir";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(517, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Adres";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(48, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "*Firma İsmi";
            // 
            // tabPageUsers
            // 
            this.tabPageUsers.Controls.Add(this.groupBox4);
            this.tabPageUsers.Location = new System.Drawing.Point(4, 27);
            this.tabPageUsers.Name = "tabPageUsers";
            this.tabPageUsers.Size = new System.Drawing.Size(952, 424);
            this.tabPageUsers.TabIndex = 2;
            this.tabPageUsers.Text = "Uygulayıcı Firma Listesi";
            this.tabPageUsers.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.panel1);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.ForeColor = System.Drawing.Color.Firebrick;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(952, 399);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Uygulayıcı Firma Listesi";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dtgrdFirmaListesi);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 22);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(946, 374);
            this.panel1.TabIndex = 0;
            // 
            // dtgrdFirmaListesi
            // 
            this.dtgrdFirmaListesi.AllowUserToAddRows = false;
            this.dtgrdFirmaListesi.AllowUserToDeleteRows = false;
            this.dtgrdFirmaListesi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgrdFirmaListesi.BackgroundColor = System.Drawing.Color.White;
            this.dtgrdFirmaListesi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgrdFirmaListesi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgrdFirmaListesi.Location = new System.Drawing.Point(0, 18);
            this.dtgrdFirmaListesi.MultiSelect = false;
            this.dtgrdFirmaListesi.Name = "dtgrdFirmaListesi";
            this.dtgrdFirmaListesi.ReadOnly = true;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dtgrdFirmaListesi.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgrdFirmaListesi.RowTemplate.ContextMenuStrip = this.contextMenuStrip1;
            this.dtgrdFirmaListesi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgrdFirmaListesi.Size = new System.Drawing.Size(946, 356);
            this.dtgrdFirmaListesi.TabIndex = 0;
            this.dtgrdFirmaListesi.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dtgrdFirmaListesi_RowPostPaint);
            this.dtgrdFirmaListesi.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dtgrdFirmaListesi_MouseDown);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Font = new System.Drawing.Font("Tahoma", 11F);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.guncelleToolStripMenuItem,
            this.silToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(194, 48);
            // 
            // guncelleToolStripMenuItem
            // 
            this.guncelleToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 11F);
            this.guncelleToolStripMenuItem.Name = "guncelleToolStripMenuItem";
            this.guncelleToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.guncelleToolStripMenuItem.Text = "Güncelle (Update)";
            this.guncelleToolStripMenuItem.Click += new System.EventHandler(this.guncelleToolStripMenuItem_Click);
            // 
            // silToolStripMenuItem
            // 
            this.silToolStripMenuItem.Name = "silToolStripMenuItem";
            this.silToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.silToolStripMenuItem.Text = "Sil (Delete)";
            this.silToolStripMenuItem.Click += new System.EventHandler(this.silToolStripMenuItem_Click);
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(946, 18);
            this.label9.TabIndex = 0;
            this.label9.Text = "İşlem yapmak için satıra sağ tıklayınız...";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // AdminPanelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1006, 554);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AdminPanelForm";
            this.Padding = new System.Windows.Forms.Padding(23, 74, 23, 25);
            this.Resizable = false;
            this.Text = "Yönetici Paneli";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminPanelForm_FormClosing);
            this.Load += new System.EventHandler(this.AdminPanelForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPageUserCreate.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPageUsers.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgrdFirmaListesi)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageUserCreate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MaskedTextBox txtTelefon_YP;
        private System.Windows.Forms.TextBox txtAdres_YP;
        private System.Windows.Forms.TextBox txtAciklama_YP;
        private System.Windows.Forms.TextBox txtEposta_YP;
        private System.Windows.Forms.TextBox txtSehir_YP;
        private System.Windows.Forms.TextBox txtFirmaAd_YP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnKaydet_YP;
        private System.Windows.Forms.TextBox txtSifre_YP;
        private System.Windows.Forms.TextBox txtKullAdi_YP;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tabPageUsers;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dtgrdFirmaListesi;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem guncelleToolStripMenuItem;
        private System.Windows.Forms.Button btnTemizle_YP;
        private System.Windows.Forms.ToolStripMenuItem silToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label9;
    }
}